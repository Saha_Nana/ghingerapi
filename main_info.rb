
APP_NAME = "Ghinger"

ActiveRecord::Base.establish_connection(
  :adapter  => "postgresql",
  :host     => "localhost",
  :username => "saha",
  :password => "amdbestofall",
  :database => "ginger_db",
  :pool => 10
)

NW_CRD="CRD"
NW_MTN="MTN"
NW_TIG="TIG"
NW_VOD="VOD"
NW_AIR="AIR"

REF_MA = "GHINGER-MEDICAL"
REF_MED = "GHINGER-MEDICATION"
REF_LAB = "GHINGER-LAB"
REF_PRSC = "GHINGER-PRESCRIP"
REF_HC = "GHINGER-HOMECARE"
REF_PD = "GHINGER-PERSONAL_DOCTOR"
REF_PC = "GHINGER-PHONE_CONSULT"
REF_VC = "GHINGER-VIDEO_CONSULT"
REF_PDHC = "GHINGER-PD_HOMECARE"
REF_PDDP = "GHINGER-PD_PRESCRIP"
REF_PDVC = "GHINGER-PD_VIDEO_CONSULT"

STR_DR="DR"
STR_CR="CR"

STR_SUCCESS="000"
STR_FAILURE="001"

Str_Ghinger="Ghinger Health Care" # title of APP for email and other official purposes



CALLBACK_URL = "https://api.ghingerhealth.com/primary_callback"  
PAYMENT_METHOD = "momo"
PAYMENT_DESCRIPTION = "Order Payment for Ghinger"

ERR_NAME ={ resp_code: '101', resp_desc: "Please provide Customer Name" }
ERR_EMAIL ={ resp_code: '101', resp_desc: "Please provide Customer Email" }
ERR_MOBILE_NUMBER = { resp_code: '101', resp_desc: 'Please provide Customer Phone' }
ERR_MOMO_NUM = { resp_code: '101', resp_desc: 'Please provide Customer MOMO number' }
ERR_MOMO_NET = { resp_code: '101', resp_desc: 'Please provide Customer MOMO Network' }
ERR_AMOUNT = { resp_code: '101', resp_desc: 'Please provide amount' }
ERR_APPTITLE = { resp_code: '101', resp_desc: 'Please provide appointment_title' }
ERR_BILL = { resp_code: '101', resp_desc: 'Please provide billing_info_id' }

############################################### RESPONSE MESSAGES AND CODES ##################################################################

ERR_PAYMENT_REQ = {:resp_code=>"000", :resp_desc=>"Kindly Complete payment by entering your PIN on the prompt"}
ERR_PAYMENT_MTN = {:resp_code=>"000", :resp_desc=>"Request for payment recieved successfully. Kindly dial *170# to proceed with payments. Select 6 (My Wallet), then choose 3 (My Approvals) to complete the transaction."}
ERR_USERNAME ={ resp_code: '111', resp_desc: "Enter Username" }
ERR_SURNAME ={ resp_code: '111', resp_desc: "Enter Surname" }
ERR_OTHR_NAME ={ resp_code: '112', resp_desc: "Enter Other names." }
ERR_MOBILE ={ resp_code: '113', resp_desc: "Enter Mobile number" }
ERR_NETWORK ={ resp_code: '114', resp_desc: "Select Mobile network" }
ERR_USER_TYPE ={ resp_code: '114', resp_desc: "Select User Type" }
ERR_DOB ={ resp_code: '114', resp_desc: "Please enter your date of birth" }
ERR_PASS ={ resp_code: '115', resp_desc: "Enter password" }
ERR_RESETTOKEN = {resp_code: '120', resp_desc: "Password Reset Code cannot be empty!"}
ERR_APPTID = {resp_code: '120', resp_desc: "Confirmed Appointment ID not found!"}
ERR_RECIEPIENTNUMBER = {resp_code: '120', resp_desc: "Reciepient Number cannot be empty."}
ERR_RECIEPIENTEMAIL = {resp_code: '120', resp_desc: "Reciepient Email Address cannot be empty."}
ERR_MESSAGEBODY = {resp_code: '120', resp_desc: "Message body cannot be empty."}
ERR_MESSAGESUBJECT = {resp_code: '120', resp_desc: "Message Subject cannot be empty."}
FAILED_REGIS = {resp_code: '116', resp_desc: "You have already registered on Ghinger"}
USERNAME_EXIST = {resp_code: '120', resp_desc: "Username has already been taken!"}
EMAIL_EXIST = {resp_code: '120', resp_desc: "Email has already been taken!"}
PHONENUMBER_EXIST = {resp_code: '120', resp_desc: "Mobile Number has already been taken!"}
PHONENUMBER_NOT_EXIST = {resp_code: '120', resp_desc: "Mobile Number does not exist!"}
PHONENUMBER_RESETTOKEN_NOT_EXIST = {resp_code: '120', resp_desc: "Mobile Number or Reset token records not found!"}
LOGIN_SUC = {resp_code: '117', resp_desc: "Login successfully"}
LOGIN_FAIL = {resp_code: '118', resp_desc: "Login failed. Wrong mobile number or Username. Please try again. "}
SUC_REGIS ={ resp_code: '202', resp_desc: "You have already subscribed to ALEXpay!" }
NO_DATA = {resp_code: '119', resp_desc: "No data can be found"}
ERR_LOCATION = {resp_code: '122', resp_desc: "Please enter location"}
ERR_HOSPITAL_ID = {resp_code: '123', resp_desc: "Hospital ID is required!"}
ERR_USER_ID = {resp_code: '124', resp_desc: "User ID is required!"}
ERR_CAT = {resp_code: '125', resp_desc: "Requester Category is required!"}
ERR_DATE = {resp_code: '126', resp_desc: "Proposed Date is required!"}
ERR_DESC = {resp_code: '127', resp_desc: "Test list is required!"}
ERR_PASS1 =  {resp_code: '120', resp_desc: "Password should have a minimum of 8 characters"}
NO_REQID = {resp_code: '121', resp_desc: "No Requester ID"}
NO_APPOINTMENTID = {resp_code: '121', resp_desc: "No Appointment ID"}
NO_PROF_INFO = {resp_code: '220', resp_desc: "Professional Info does not exists"}
SUCCESS_PROF_INFO = {resp_code: '221', resp_desc: "Professional Info exists"}
NO_PAT_DOC_PRAMS = {resp_code: '222', resp_desc: "No patient ID and Doctor ID"}
INVALID_PRAMS = {resp_code: '128', resp_desc: "Invalid Param(s) provided."}
INVALID_APPTID_PRAMS = {resp_code: '128', resp_desc: "Invalid Appointment ID Param(s) provided."}
INVALID_ACTION_PRAMS = {resp_code: '128', resp_desc: "Invalid ACTION param provided."}
INVALID_PERSONTYPE_PRAMS = {resp_code: '128', resp_desc: "Invalid Person Type provided."}
INVALID_BILLINFOID_PRAMS = {resp_code: '128', resp_desc: "Invalid Patient Billing Info ID provided."}
INVALID_AVAILDATE_PRAMS = {resp_code: '128', resp_desc: "Invalid AVAIL DATE Param(s) provided."}
INVALID_PROFID_PRAMS = {resp_code: '128', resp_desc: "Invalid PROF ID Param(s) provided."}
ERR_MISSING_IMAGE_PARAM = {resp_code: '129', resp_desc: "Invalid Image Param(s) provided."}
ERR_EMAIL_SUCCESS = {resp_code: '130', resp_desc: "Email has been sent successfully."}

ERR_MISSING_BILLING_INFO_ID = {resp_code: '128', resp_desc: "Missing Patient Billing Info ID."}
ERR_MISSING_TRANS_AMT = {resp_code: '128', resp_desc: "Missing transaction amount"}
ERR_MISSING_TRANS_NW = {resp_code: '128', resp_desc: "Missing transaction Network"}
ERR_MISSING_TRANS_TYPE = {resp_code: '128', resp_desc: "Missing transaction Type"}
ERR_MISSING_PHONE_NUMBER =  {resp_code: '128', resp_desc: "Missing Phone Number"}
ERR_INVALID_TRANS_AMT = {:resp_code=>"040", :resp_desc=>"Invalid transaction amount"}
ERR_INVALID_TRANS_NW = {:resp_code=>"041", :resp_desc=>"Invalid transaction network"}
ERR_INVALID_TRANS_TYPE = {:resp_code=>"042", :resp_desc=>"Invalid transaction type"}
ERR_INVALID_CUST_NO = {:resp_code=>"043", :resp_desc=>"Invalid customer number"}

ERR_MISSING_APPT_TITLE = {resp_code: '128', resp_desc: "Missing Appointment Title"}
ERR_INVALID_APPT_TITLE= {resp_code: '128', resp_desc: "Invalid Appointment Title"}

############################################### RESPONSE MESSAGES AND CODES ##################################################################






############################################### CONFIGS FOR ANMGW - ORCHARD ##################################################################
#This is where the SMS & PAYMENT API Will hit.

STROPENTIMEOUT = "180"
# HEADERS = {'Content-Type'=> 'application/json','timeout'=>STROPENTIMEOUT, 'open_timeout'=>STROPENTIMEOUT}
#STR_CLIENT_KEY="" #your client key / public key
#STR_SECRET_KEY="" #your secret key
# API_KEY = "" #SMS key
# CLIENT_ID="" #your client's id
# REQHDR = {'Content-Type'=>'Application/json','timeout'=>'180'}
# SMS_URL= ""
# SMS_END_POINT = "/SendMySMS"
# STRERR = '99'.freeze
STR_PASS = "3001"
STR_FAIL = "3002"
ERR_STR_SUCC = "Success"
ERR_STR_FAIL = "Failure"





REQHDR = {'Content-Type'=>'Application/json','timeout'=>'180'}
TRANS_TYPES = 'CTM'
CLIENT_ID = "30"
STR_CLIENT_KEY = "ig6onp6eJY1dmESscCpH4PlZLUN3C0k5GWz1RmnzBhvczcmK2csF9/A54Mg2F7No3anReNJwMY/kWZ9H4W1K9g=="
STR_SEC_KEY = "QNFpot9mMHojSR4QQAQThq8bPo1oruVPXhW4PzDS1NlfmCxTi2Uywvy1imqhmfXW0GRqPTPU4MGss4XFxR1wHA=="
GHINGER_MOMO_CALLBACK = "https://api.ghingerhealth.com/primary_callback"  
GhINGER_REF = "GHINGERHEALTHCARE"
GHINGER_VISA_URL_CALLBACK = "https://api.ghingerhealth.com/visa_callback"  
VISA_LANDING_PAGE = "https://ghingerhealth.com/"
VISA_PAYMENT_MODE = "CRD"
VISA_CURRENCY_CODE = "GHS"

#Endpoints for MOMO PAYMENT
URL=""  # Main URL
HEADER={'Content-Type'=>'application/json','timeout'=>'180'} 
ORCH_ENDPOINT="" # ENdpoint
ORCH_RET_URL="" # Callback URL & Endpoint





Email_from1="noreply@ghingerhealthcare.com.gh"
Email_from = ""   #"ghinger@ghingerhealthcare.com.gh"
Email_sub = "Ghinger Health Care"
EMAIL_SENDER = "Ghinger Health Care"
REPLY_TO = "info@ghingerhealthcare.com.gh"
SIGN_UP_EMAIL_BODY = "You have successfully signed up on Ghinger Health Care"  

#DEFAULT_IMG_PATH = '/opt/ghinger_api/public/pictures/' 
DEFAULT_IMG_PATH = '/opt/devdir/saha/GhingerAPI/public/pictures/'  
#DEFAULT_IMG_PATH = '/opt/devdir/GhingerApps/ghinger_portal_test/public/images/'

LAB_IMG_PATH = DEFAULT_IMG_PATH 
USER_PROFILE_IMG_PATH = DEFAULT_IMG_PATH + 'user_profiles/'
