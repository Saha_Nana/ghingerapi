# Use this file to easily define all of your cron jobs.

set :output, "/opt/padmore/logs/error.log"

# ====================================================== Below are the correct configs =====================================================================

every 1.day, at: '12:30am' do
#every 1.minutes do
#every :thursday, at: '10:16am' do
  command "ruby -r '/opt/osafo/osafo/ghinger_api/crons/sendnotification_test.rb' -e 'CheckCrons.check_appointments_due'"
end

# ====================================================== correct configs =====================================================================