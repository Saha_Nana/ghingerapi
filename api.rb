require 'sinatra'
require 'json'
require 'nokogiri'
require 'sinatra/activerecord'
require 'yaml'
require 'faraday'
require 'net/http'
require 'uri'
require 'savon'
require 'digest/md5'
require 'openssl'
require 'twilio-ruby'
require './model'
require './functions'
require 'bcrypt'
require 'date'
require 'openssl'
require './main_info'
require './crons/check_appointment_date_cron'
require './primary_callback'
require './visa_callback'




#set :bind, "144.217.165.180" #API IP address
#set :port, "6020" # API Port.


#This is the endpoint that the cron job will call. This endpoint will now make call to the cron file where the actual processing is.
get '/check_appointments_due' do
  response['Access-Control-Allow-Origin'] = '*'
  
  puts check_appointments_due()
end



post '/send_sms' do
  
  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  puts req
  recipient_number = req['recipient_number']
  message_body = req['message_body']
  
  halt ERR_RECIEPIENTNUMBER.to_json unless req.key?('recipient_number')
  halt ERR_MESSAGEBODY.to_json unless req.key?('message_body')
  
  puts genUniqueID
  sendmsg(APP_NAME, recipient_number, message_body)
  
end

post '/send_email' do
  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'
  
  puts req
  email = req['recipient_email']
  message_body = req['message_body']
  email_sub = req['email_subject']
  
  halt ERR_RECIEPIENTEMAIL.to_json unless req.key?('recipient_email')
  halt ERR_MESSAGESUBJECT.to_json unless req.key?('email_subject')
  halt ERR_MESSAGEBODY.to_json unless req.key?('message_body')
  
  # str_email=""
  # str_email="Dear Mady,\n\n"
  # str_email << "#{SIGN_UP_EMAIL_BODY}. Find below your signup details: \n\n\n"
  # str_email << "Regards,\nGhinger Health Care\n"
      
  ############## Send email to applicant #################
  send_email(email, message_body, email_sub)
end



post '/mail_me' do


  my_test_mail()
end


post '/sign_up' do

  puts"RUNNING GINGER SIGN UP"

  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  puts req
  surname = req['surname']
  other_names = req['other_names']
  mobile_number = req['mobile_number']
  email = req['email'].strip.downcase
  password = req['password']
  user_type = req['user_type']
  pds = req['pds']
  dob = req['dob']


  if password.length < 6
     halt ERR_PASS1.to_json
   
  end
  
   if mobile_number.length > 15
    error_num  =  { resp_code: '666',resp_desc: 'Mobile number is not valid. Please provide the correct number'}
     halt error_num.to_json
   
  end
  
  
  #########VALIDATING THE PARAMS

  halt ERR_SURNAME.to_json unless req['surname']
  halt ERR_OTHR_NAME.to_json unless req['other_names']
  halt ERR_MOBILE.to_json unless req.key?('mobile_number')
  halt ERR_EMAIL.to_json unless req.key?('email')
  halt ERR_PASS.to_json unless req.key?('password')
  halt ERR_USER_TYPE.to_json unless req.key?('user_type')
#  halt ERR_DOB.to_json unless req.key?('dob')


 if Registration.exists?(:mobile_number => mobile_number)

      halt PHONENUMBER_EXIST.to_json

      
 elsif Registration.exists?(:email.downcase => email.downcase)
      halt EMAIL_EXIST.to_json

    else

      if user_type == 'C' # insert_details_patient
        
        #halt ERR_LOCATION.to_json unless req.key?('suburb_id')
        
        insert_details_patient(surname,other_names,mobile_number,email,password,user_type,"APP",password,dob)
        regis_success  =  { resp_code: '000',resp_desc: 'You have successfully registered on Ghinger ! Kindly wait for a confirmation to proceed!', user_type1: user_type}

      else

        regid = insert_details(surname,other_names,mobile_number,email,password,user_type,"APP",password,dob,pds)
        regis_success  =  { resp_code: '000',resp_desc: 'You have successfully registered on Ghinger! Kindly Login!', user_type1: user_type, regid: regid}

        puts "*************************************************"
        puts "regis_success = #{regis_success}"
        puts "*************************************************"

      end
    end
    regis_success.to_json
end


post '/update_user_profile' do
  puts "------------------UPDATING PROFILE API ------------------"
  
  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  id = req['id']
  person_id = req['person_id']
  surname = req['surname']
  other_names = req['other_names']
  mobile_number = req['mobile_number']
  email = req['email']
  
   
    update_user_profile(id,person_id,surname,other_names,mobile_number,email)

    regis_success = { resp_code: '000',resp_desc: 'You have successfully updated your profile!'}

    regis_success.to_json
    
 
end



post '/update_registration' do

  puts"RUNNING GINGER SIGN UP"

  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

   id = req['id']
   suburb_name = req['surburb_name']
   hospital_name = req['hospital_name']
   has_specialty = req['has_specialty']
   specialty_name = req['specialty_name']
   specialty_duration = req['specialty_duration']
   license_number = req['license_number']
   medical_regulator = req['medical_regulator']
   foreign_training = req['foreign_training']
   foreign_medical_regulator = req['foreign_medical_regulator']
   foreign_license_number = req['foreign_license_number']

  puts req
  
  # {"id"=>3, "surburb_name"=>"Madina", "hospital_name"=>"Fsafda", "has_specialty"=>"t", "specialty_name"=>4, "specialty_duration"=>"12", "license_number"=>"345v23", "medical_regulator"=>2, "foreign_training"=>"t", "foreign_medical_regulator"=>"432423", "foreign_license_number"=>"43243"}
   
  halt INVALID_PRAMS.to_json unless req['id']
  halt INVALID_PRAMS.to_json unless req['surburb_name']
  halt INVALID_PRAMS.to_json unless req['hospital_name']
  halt INVALID_PRAMS.to_json unless req['has_specialty']
  halt INVALID_PRAMS.to_json unless req['specialty_name']
  halt INVALID_PRAMS.to_json unless req['specialty_duration']
  # halt INVALID_PRAMS.to_json unless req['license_number']
  # halt INVALID_PRAMS.to_json unless req['medical_regulator']
  # halt INVALID_PRAMS.to_json unless req['foreign_training']
 
  if SuburbMaster.get_suburb_id(suburb_name).exists?
    
    p surburb_id = SuburbMaster.get_suburb_id(suburb_name)[0]["id"].to_json
    
    update_details(id,surburb_id,hospital_name,has_specialty,specialty_name,specialty_duration,license_number,medical_regulator,foreign_training,foreign_medical_regulator,foreign_license_number)

    regis_success = { resp_code: '000',resp_desc: 'You have successfully registered on Ghinger! Kindly wait for a confirmation to proceed!'}

    regis_success.to_json
    
  else
     halt NO_DATA.to_json
  end
    

    
end

post '/prepare_login' do
  
  puts "prepare_login request start time : #{Time.now} OR  #{DateTime::now.to_s} "
  
  request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'
  
  email = req['email'].strip.downcase
  password = req['password']
  
  halt ERR_EMAIL.to_json unless req['email']
  halt ERR_PASS.to_json unless req.key?('password')

    puts "EMAIL IS :  #{email} " 
    puts "PASSWORD IS: #{password}"


  if PasswordTable.exists?(:email => email ,:password => password_hash(password), :status => true)
    
    db_password = PasswordTable.where(' password=? and status=?',password_hash(password),true).order('id desc')[0].password

    db_email= PasswordTable.where('email =? and status=?',email,true).order('id desc')[0].email

    puts "login request end time :  #{Time.now} OR #{DateTime::now.to_s} "

    if  ((password_hash(password) == db_password) && (db_email.strip == email))

      puts "Password CORRECT" 
      puts "THE EMAILFOR SING IN IS #{email}"
      
      puts "################## START STEP 1 - retrieve_regis1 #################### "
        
        if Registration.retrieve_regis1(email).exists?
            retrieve_regis1 = Registration.retrieve_regis1(email)
            p "-----------------------------"
            p retrieve_regis1
            p retrieve_regis1.to_json
          
         else
           retrieve_regis1 = ""
           
        end
      
      puts "################## END STEP 1 - retrieve_regis1 ####################"
        
        
      puts "################## START STEP 2 - retrieve_regis ####################"  
        
        if Registration.retrieve_regis(email).exists?
            retrieve_regis = Registration.retrieve_regis(email)
        p "-----------------------------"
        p retrieve_regis
        p retrieve_regis.to_json
    
          else
            retrieve_regis = ""
       
        end
        
      puts "################## END STEP 2 - retrieve_regis ####################"
        
      
      puts "################## START STEP 3 - retrieve_confirm ####################"
      
        if Registration.retrieve_confirm(email).exists?
            retrieve_confirm = Registration.retrieve_confirm(email)
            p "-----------------------------"
            p retrieve_confirm
            p retrieve_confirm.to_json
            
           
            new_id = ""
            puts " new_id = #{new_id}"
        
    
          else
          
            retrieve_confirm = ""
        end
        
      puts "################## END STEP 3 - retrieve_confirm ####################"
        
       
      puts "################## START STEP 4 - retrieve_doctor_regis ####################" 
      
            

                retrieve_doctor_regis = ""

       puts "################## END STEP 4 - retrieve_doctor_regis ####################" 
       
       
       puts "################## START STEP 5 - retrieve_confirm ####################"
      
        if User.retrieve_user_status(email).exists?
            retrieve_user_status = User.retrieve_user_status(email)
            p "-----------------------------"
            p retrieve_user_status
            p retrieve_user_status.to_json  
          
          else
           
            retrieve_user_status = ""
        end
        
      puts "################## END STEP 5 - retrieve_confirm ####################"
            
        
      puts "prepare_login request stop time : #{Time.now} OR #{DateTime::now.to_s} "
    
      return { 'LOGIN_SUC' => LOGIN_SUC, 'result1' => retrieve_regis1 , 'result2' => retrieve_regis , 'result3' => retrieve_confirm , 'result4' => retrieve_doctor_regis, 'retrieve_user_status' => retrieve_user_status }.to_json
  
  


     else
       puts  puts "PIN INCORRECT"
       halt LOGIN_FAIL.to_json
      


     end

      halt SUC_REGIS.to_json

    else
      regis_success  =  { resp_code: "222",resp_desc: "Wrong Email/Password. Please try again or sign up if you don't have an account"}
      puts regis_success
         end
    regis_success.to_json
  
end


post '/reset_password_get_phonenumber' do
  
  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  mobile_number = req['mobile_number']

  halt ERR_MOBILE.to_json unless req['mobile_number']
  
  if PasswordTable.exists?(:mobile_number => mobile_number)

      db_records = PasswordTable.where('mobile_number =? and status=?',mobile_number,true).order('id desc')[0]
      
       if (db_records.mobile_number)
          puts db_records
          puts "db_mobile_number = #{db_records.mobile_number} and reset code = #{genUniqueID} and id = #{db_records.id}"
          message_body = "Reset Code : #{genUniqueID}"
          
          update_reset_token = update_reset_token(genUniqueID,db_records.mobile_number,db_records.id)
          recipient_number =  db_records.mobile_number  
  
          sendmsg(APP_NAME, recipient_number, message_body)
          
          regis_success  =  { resp_code: '000',resp_desc: 'Reset Code has been sent Successfully. Enter the code on the next page!', returned_phonenumber: db_records.mobile_number}
          regis_success.to_json
       else
         regis_failure  =  { resp_code: '000',resp_desc: 'Phone Number is incorrect. Please check and try again!'}
         regis_failure.to_json
       end
      
  else
      halt PHONENUMBER_NOT_EXIST.to_json
  end
   
end


post '/reset_password_get_reset_token' do
  
  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  phonenumber = req['phonenumber']
  reset_token = req['reset_token']
  
  puts req
  
  halt ERR_MOBILE.to_json unless req['phonenumber']
  halt ERR_RESETTOKEN.to_json unless req['reset_token']
  
  if PasswordTable.exists?(:mobile_number => phonenumber, :reset_token => password_hash(reset_token))
    db_reset_token = PasswordTable.where(' mobile_number =? and reset_token =? and status=?',phonenumber,password_hash(reset_token),true).order('id desc')[0].reset_token
    
    if(db_reset_token)
      
       regis_success  =  { resp_code: '000',resp_desc: 'Phone Number and Reset Token records found.',db_reset_token: db_reset_token}
       return regis_success.to_json
       
     else
       regis_failure  =  { resp_code: '100',resp_desc: 'Reset Token not found.'}
       return regis_failure.to_json
    end
      
  else
      halt PHONENUMBER_RESETTOKEN_NOT_EXIST.to_json
  end
   
end


post '/reset_password_new_password' do
  
  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  reset_token = req['reset_token']
  phonenumber = req['phonenumber'] # or use Id of the record.
  confirmed_password = req['confirmed_password']
  
  puts req
  
  halt ERR_MOBILE.to_json unless req['phonenumber']
  halt ERR_PASS.to_json unless req['confirmed_password']
  
  password_reset(confirmed_password,phonenumber,reset_token)
  
  #update the password provided the record match the ID or phonenumber
   
end


post '/retrieve_regis' do

    request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'

  email = req['email']

  puts "THE email IS #{email}"


    if Registration.retrieve_regis(email).exists?
        retrieve = Registration.retrieve_regis(email)
    p "-----------------------------"
    p retrieve
    p retrieve.to_json

     return retrieve.to_json  

      else
        halt NO_DATA.to_json
    end

 # else
   # check  =  { resp_code: '222',resp_desc: 'This User has not registered'}
 # end

return check.to_json

end





post '/retrieve_edit' do

    request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'

  reg_id = req['reg_id']

      retrieve = Registration.retrieve_edit(reg_id)
        
        
    p "-----------------------------"  
    p retrieve
    p retrieve.to_json

     return retrieve.to_json  


end

post '/verify_professional_info' do

  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  doc_id = req['doc_id']

  puts "THE DOC ID IS #{doc_id}"

   reg = Registration.find(doc_id)

   if reg.specialty_id.nil? or reg.specialty_duration.nil? or reg.professional_group_id.nil? or reg.licence_number.nil?
     puts "#############################"
     puts "the specialty field is empty"
     puts "#############################"
     # prevent the person
     halt NO_PROF_INFO.to_json

   else
     puts "#############################"
     puts "the specialty field is not empty"
     puts "#############################"

     halt SUCCESS_PROF_INFO.to_json
     # allow the person
   end

end

post '/find_patient_details_pds' do


  #######################


  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  patient_id = req['patient_id']
  doctor_id = req['doctor_id']

  if doctor_id != nil and patient_id != nil

   _array = ConfirmedPersonalDoctorService.joins("LEFT JOIN person_infos ON confirmed_personal_doctor_services.patient_id =  person_infos.id LEFT JOIN registration ON person_infos.reg_id = registration.id LEFT JOIN suburb_masters ON suburb_masters.id = confirmed_personal_doctor_services.suburb_id LEFT JOIN pre_appointments ON pre_appointments.id = confirmed_personal_doctor_services.pre_appointment_id ")
      .select("pre_appointment_id,patient_id,confirmed_personal_doctor_services.appointment_type_id,person_infos.surname,person_infos.other_names,suburb_masters.suburb_name,person_infos.dob,pre_appointments.allergies,pre_appointments.prev_medical_history,pre_appointments.medication,pre_appointments.prev_medical_history")
      .where( "confirmed_personal_doctor_services.doctor_id = ? and  confirmed_personal_doctor_services.patient_id = ? and confirmed_personal_doctor_services.appointment_type_id = 'PD'","#{doctor_id}","#{patient_id}").
      order('person_infos.created_at desc')


      return _array.to_json



  else
    regis_success  =  { resp_code: '555',resp_desc: 'Please your account has not been verified. Kindly be patient. An agent would speak to you soon to get your account active!'}

    regis_success.to_json

  end


  ############

end

post '/pre_appointments/statistics' do

    request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'
  

  requester_id = req['requester_id']

  if requester_id.blank?
    halt NO_REQID.to_json
  else
    statistics1 = PreAppointment.preappointment_statistics(requester_id)
    return statistics1.to_json
  end
return check.to_json

end




post '/retrieve_regis1' do
  
  puts "login request start time :  #{Time.now} OR #{DateTime::now.to_s} "

    request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'

  email = req['email']

  puts "THE email IS #{email}"



    if Registration.retrieve_regis1(email).exists?
        retrieve = Registration.retrieve_regis1(email)
    p "-----------------------------"
    p retrieve
    p retrieve.to_json

     return retrieve.to_json

      else
        halt NO_DATA.to_json
    end

 # else
   # check  =  { resp_code: '222',resp_desc: 'This User has not registered'}
 # end
#
return check.to_json

end



post '/retrieve_alt' do

  request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'

  id = req['id']
  
  halt INVALID_PRAMS.to_json unless params['id']

 # if Registration.exists?(:id => id)

    if Registration.retrieve_alt(id).exists?
        retrieve = Registration.retrieve_alt(id)
    p "-----------------------------"
    p retrieve
    p retrieve.to_json

     return retrieve.to_json

      else
        halt NO_DATA.to_json
    end

 # else
   # check  =  { resp_code: '222',resp_desc: 'This User has not registered'}
 # end

return check.to_json

end


get '/location_search' do

  #request.body.rewind
  #req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'

  location = params['location']
  
  halt INVALID_PRAMS.to_json unless params['location']

  puts "THE location IS #{location}"

   # halt ERR_LOCATION.to_json unless params['location']


if ProviderMaster.retrieve_providers(location).exists?
   _array = ProviderMaster.retrieve_providers(location)

    p "-----------------------------"
    p _array
    p _array.to_json

    return _array.to_json

  else
        halt NO_DATA.to_json
  end


end

post '/hospital' do


  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'


  location = req['location']
  
  halt INVALID_PRAMS.to_json unless req['location']
# halt ERR_LOCATION.to_json unless req['location']
if ProviderMaster.retrieve_providers(location).exists?
  _array = ProviderMaster.retrieve_providers(location)

    p _array
    p _array.to_json

    return _array.to_json
  else
        halt NO_DATA.to_json
  end


end

post '/services' do


  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'


  provider_name = req['provider_name']
  
  halt INVALID_PRAMS.to_json unless req['provider_name']

  puts "Provider name is #{provider_name}"

  _array = ProviderService.search_services(provider_name)

    p _array
    p _array.to_json

    return _array.to_json

end





get '/get_service_providers' do
  
  response['Access-Control-Allow-Origin'] = '*'
  
  if ProviderMaster.get_service_providers().exists?
   _array = ProviderMaster.get_service_providers()

    return _array.to_json

  else
        halt NO_DATA.to_json
  end
end


get '/get_countries' do
  
  response['Access-Control-Allow-Origin'] = '*'
  
  if CountryMaster.get_countries().exists?
   _array = CountryMaster.get_countries()

    return _array.to_json

  else
        halt NO_DATA.to_json
  end
end


get '/get_regions_by_country' do
  
  response['Access-Control-Allow-Origin'] = '*'
  
  country_id = params['country_id']
  
  halt INVALID_PRAMS.to_json unless params['country_id']
  
  if RegionMaster.retrieve_region(country_id).exists?
   _array = RegionMaster.retrieve_region(country_id)

    return _array.to_json

  else
        halt NO_DATA.to_json
  end
  
end


get '/get_cities_by_region' do
  
  response['Access-Control-Allow-Origin'] = '*'
  
  region_id = params['region_id']
  
  halt INVALID_PRAMS.to_json unless params['region_id']
  
  if CityTownMaster.retrieve_cities(region_id).exists?
   _array = CityTownMaster.retrieve_cities(region_id)

    return _array.to_json

  else
        halt NO_DATA.to_json
  end
  
end

######
get '/get_all_cities' do
  
  response['Access-Control-Allow-Origin'] = '*'

   _array = CityTownMaster.retrieve_cities()

    return _array.to_json

end


get '/get_all_suburbs' do
  
  response['Access-Control-Allow-Origin'] = '*'

   _array = SuburbMaster.retrieve_suburbs()

    return _array.to_json

end


get '/get_suburbs_by_city' do
  
  response['Access-Control-Allow-Origin'] = '*'
  
  city_id = params['city_id']
  
  halt INVALID_PRAMS.to_json unless params['city_id']
  
  if SuburbMaster.retrieve_suburbs(city_id).exists?
   _array = SuburbMaster.retrieve_suburbs(city_id)

    return _array.to_json

  else
        halt NO_DATA.to_json
  end
  
end


get '/get_suburbs_by_city_type_along' do
  
  response['Access-Control-Allow-Origin'] = '*'
  
  city_id = params['city_id']
  location = params['location']
  
  halt INVALID_PRAMS.to_json unless params['city_id']
  halt INVALID_PRAMS.to_json unless params['location']
  
  puts "city_id = #{city_id} AND location = #{location}" 
  
  if city_id == 'null'
    return INVALID_PRAMS.to_json
  else
    
    if SuburbMaster.retrieve_suburbs_type_along(city_id,location).exists?
      _array = SuburbMaster.retrieve_suburbs_type_along(city_id,location)

      puts _array.to_json
    
      return _array.to_json

    else
        halt NO_DATA.to_json
    end
  
  end
    
end



post '/retrieve_service_prices' do
   request.body.rewind
   req = JSON.parse request.body.read

   response['Access-Control-Allow-Origin'] = '*'
   appointment_type_id = req['appt_id']
   req_urgency = req['req_urgency']
   
   earnings = []
   puts "----------------------"
   puts appointment_type_id
   puts  req_urgency
   puts "----------------------"
   
   fees_obj = ServiceFeeMaster.where(appt_type_id: appointment_type_id,req_urgency: req_urgency,active_status: true,del_status: false)
   puts earning_rates = EarningRate.where(apt_type_id: appointment_type_id,req_urgency: req_urgency).select('apt_type_id,req_urgency,earn_rate')
   puts earning_rates[0].earn_rate
   puts orignal_fee = fees_obj[0].fee
   puts earn_rates = earning_rates[0].earn_rate
    puts "ORIGINAL FEE IS #{orignal_fee}"
    puts "EARN RATE IS #{earn_rates}"
    
   potential_earn_for_doctor = orignal_fee - (orignal_fee * earn_rates)
   earn_for_ghinger = orignal_fee * earn_rates
   
   puts "potential_earn_for_doctor IS #{potential_earn_for_doctor}"
    puts "earn_for_ghinger IS #{earn_for_ghinger}"
    
    earnings << {"DoctorEarnings": potential_earn_for_doctor, "GhingerEarnings": earn_for_ghinger}

   
   return earnings.to_json
  
   
   result_success = {resp_code: '000',resp_desc: 'Services Fee retrieved Successfully.', fee: fees, urgency: urgency}

   result_success.to_json
  
  
end


post '/give_feedback' do
  
  request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'

  
  requester_id = req['requester_id']
  requester_name = req['requester_name']
  appointment_type= req['appointment_type']
  message = req['message']


   puts insert_feedback(requester_id, requester_name,appointment_type,message)
   regis_success  =  { resp_code: '000',resp_desc: 'Your feedback has been submitted successfully. The Ghinger team appreciates it. Thank you!'}

    regis_success.to_json


end

  


post '/make_appointment' do
  
  request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'

  hospital_id = req['provider_id']
  user_id = req['requester_id']
  user_idd = req['user_id']
  user_type = req['requester_cat']
  beneficiary_name= req['beneficiary_name']
  # beneficiary_gender= req['beneficiary_gender']
  beneficiary_phone_number= req['beneficiary_phone_number']
  beneficiary_age= req['beneficiary_age']
  appointment_type_id = req['appointment_type_id']
  proposed_date = req['proposed_date']
  complaint_desc = req['complaint_desc']
  prev_medical_history = req['prev_medical_history']
  req_urgency = req['req_urgency']
  allergies = req['allergies']
  suburb_id = req['suburb_id']
  medication = req['medication']
  remarks = req['remark']
  test_list = req['test_list']
  
  if test_list.nil?
    test_list = ""
  end
  
  #########VALIDATING THE PARAMS
  
  halt ERR_USER_ID.to_json unless req['requester_id']
  halt ERR_CAT.to_json unless req['requester_cat']
  halt ERR_DESC.to_json unless req['complaint_desc']

  puts "***********requests:  #{req} ***********"

   puts insert_appointment(hospital_id, user_id,user_type,beneficiary_name,beneficiary_phone_number,beneficiary_age,appointment_type_id,proposed_date,complaint_desc,prev_medical_history,req_urgency,allergies,"APP",suburb_id,medication,remarks,user_idd,test_list)
   regis_success  =  { resp_code: '000',resp_desc: 'Appointment made Successfully. You would be contacted shortly. Thank you!'}

  regis_success.to_json


end

post '/homecare' do
  request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'


  suburb_id = req['suburb_id']
  hospital_id = req['provider_id']
  user_id = req['requester_id']
  user_type = req['requester_cat']
  beneficiary_name= req['beneficiary_name']
  beneficiary_age= req['beneficiary_age']
  beneficiary_phone_number= req['beneficiary_phone_number']
  req_urgency = req['req_urgency']
  appointment_type_id = req['appointment_type_id']  
  proposed_date = req['proposed_date']
  proposed_time = req['proposed_time']
  complaint_desc = req['complaint_desc']
  prev_medical_history = req['prev_medical_history']
  allergies = req['allergies']
  medication = req['medication']
  appt_amnt = req['appt_amnt']
 

  puts "-----------HOME CARE - PARAMETERS BEGIN--------------------"
  puts appointment_type_id
  puts "######### request = #{req}"
  puts "-----------HOME CARE - PARAMETERS END--------------------"


  #########VALIDATING THE PARAMS
  
  halt ERR_USER_ID.to_json unless req['requester_id']
  halt ERR_CAT.to_json unless req['requester_cat']
  #halt ERR_DATE.to_json unless req.key?('proposed_date')
  halt ERR_DESC.to_json unless req['complaint_desc']
  halt NO_APPOINTMENTID.to_json unless req['appointment_type_id']

 
  puts insert_home_appointment(hospital_id,user_id,user_type,beneficiary_name,beneficiary_phone_number,beneficiary_age,appointment_type_id,proposed_date,proposed_time,complaint_desc,prev_medical_history,req_urgency,allergies,"APP",suburb_id,medication,appt_amnt)
  regis_success  =  { resp_code: '000',resp_desc: 'Appointment made Successfully. You would be contacted shortly. Thank you!'}

  regis_success.to_json


   # puts insert_appointment(hospital_id, user_id,user_type,beneficiary_name,beneficiary_phone_number,appointment_type_id,proposed_date,complaint_desc,prev_medical_history,req_urgency,allergies,"APP",suburb_id,medication,remarks,user_idd)
   
  
end

post '/doc_request' do
  request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'

  user_id = req['requester_id']
  appointment_type_id = req['appointment_type_id']
  pd_name = req['pd_name']
  has_pd = req['has_pd']
  # suburb_id = req['suburb_id']
  suburb_name = req['suburb_name']
  prev_medical_history = req['prev_medical_history']
  
  #########VALIDATING THE PARAMS

  if PreAppointment.exists?(:requester_id => user_id,:appointment_type_id => "PD")
    regis_success  =  { resp_code: '555',resp_desc: 'You have already requested for a personal doctor. We would verify your request shortly.'}
    regis_success.to_json
  
  else
    
    if user_id != nil
       
      #get the suburb_id first from the suburb_masters and then use that id to insert before
      if SuburbMaster.get_suburb_id(suburb_name).exists?
        p suburb_id = SuburbMaster.get_suburb_id(suburb_name)[0]["id"].to_json
        
        #puts insert_doc_request("S",user_id,appointment_type_id,suburb_id,has_pd,pd_name,"APP",prev_medical_history)
        puts insert_doc_request(user_id,appointment_type_id,suburb_id,has_pd,pd_name,"APP",prev_medical_history)
     
        regis_success  =  { resp_code: '000',resp_desc: 'Personal Doctor Request made Successfully. You would be contacted shortly. Thank you!'}
    
        regis_success.to_json
      else
            halt NO_DATA.to_json   
      end
    
    else
      regis_success  =  { resp_code: '555',resp_desc: 'Please your account has not been verified. Kindly be patient. An agent would speak to you soon to get your account active!'}
  
      regis_success.to_json
  
    end
  end

end


post '/check_patient_pds_status' do
  request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'

  user_id = req['requester_id']
  #########VALIDATING THE PARAMS

  # get_patient_doctor(patient_id)
  
  if ConfirmedPersonalDoctorService.get_patient_doctor(user_id).exists?
    
    #fetch doctor's details
    doc_record = ConfirmedPersonalDoctorService.get_patient_doctor(user_id)[0]
    # puts doc_record.to_json
    if PersonInfo.retrieve_doctor_regis(doc_record.doctor_id).exists?
      
      doc_records = PersonInfo.retrieve_doctor_regis(doc_record.doctor_id)
      
      puts doc_records.to_json
    
      regis_success  =  { resp_code: '000',resp_desc: 'Personal doctor records found.',doc_records: doc_records}
      return regis_success.to_json
      
    end
    
        

  else

    if PreAppointment.exists?(:requester_id => user_id,:appointment_type_id => "PD", :confirm_status => 'f')
      regis_success  =  { resp_code: '555',resp_desc: 'You have already requested for a personal doctor. We would verify your request shortly.'}
      regis_success.to_json

    else
    
      if user_id != nil
        
        regis_success  =  { resp_code: '100',resp_desc: 'No request for Personal Doctor Service has been made.!'}
        regis_success.to_json
      
      else
         regis_success  =  { resp_code: '555',resp_desc: 'Please your account has not been verified. Kindly be patient. An agent would speak to you soon to get your account active!'}
    
         regis_success.to_json
  
     end
  end
 
 end
  

end

post '/phone_consult' do
  request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'

  # {"requester_id":137,"requester_cat":"S","beneficiary_name":"","req_urgency":"S","appointment_type_id":"VC","proposed_date":"2018-04-26
  #           12:36","complaint_desc":"None","prev_medical_history":"None","allergies":"None","medication":"None"}

  user_id = req['requester_id']
  appointment_type_id = req['appointment_type_id']
  app_date = req['proposed_date']
  requester_cat = req['requester_cat']
  beneficiary_name = req['beneficiary_name']
 
  beneficiary_age = req['beneficiary_age']
  beneficiary_phone_number= req['beneficiary_phone_number']
  req_urgency = req['req_urgency']
  proposed_date = req['proposed_date']
  complaint_desc = req['complaint_desc']
  prev_medical_history = req['prev_medical_history']
  allergies = req['allergies']
  medication = req['medication']
  appt_amnt = req['appt_amnt']

  puts req




  #########VALIDATING THE PARAMS

  if user_id != nil
   puts insert_phone_consult(user_id,appointment_type_id,app_date,"APP",requester_cat,beneficiary_name,beneficiary_age,beneficiary_phone_number,req_urgency,proposed_date,complaint_desc,prev_medical_history,allergies,medication,appt_amnt)
   regis_success  =  { resp_code: '000',resp_desc: 'Appointment made Successfully. You would be contacted shortly. Thank you!'}

  regis_success.to_json
  else
     regis_success  =  { resp_code: '555',resp_desc: 'Please your account has not been verified. Kindly be patient. An agent would speak to you soon to get your account active!'}

  regis_success.to_json

   end


end

post '/prescription' do
  request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'

  user_id = req['requester_id']
  requester_cat = req['requester_cat']
  beneficiary_name = req['beneficiary_name']
  beneficiary_phone_number= req['beneficiary_phone_number']
  req_urgency = req['req_urgency']
  appointment_type_id = req['appointment_type_id']
  medication = req['medication']
  duration = req['duration']

  beneficiary_age = ""
  
  allergies = ""
  prev_medical_history = ""
  
  beneficiary_age = req['beneficiary_age']
 
  allergies = req['allergies']
  prev_medical_history = req['prev_medical_history']
  


  if user_id != nil
    
    
   puts "############################################ prescription ##########################################" 
   puts "prescription requests: #{req}"
   puts "############################################ prescription ##########################################" 
   
   
   puts insert_prescription(user_id,appointment_type_id,medication,duration,"APP",requester_cat,beneficiary_name,beneficiary_phone_number,beneficiary_age,req_urgency,allergies,prev_medical_history)
   regis_success  =  { resp_code: '000',resp_desc: 'Prescription request made Successfully. You would be contacted shortly. Thank you!'}

  regis_success.to_json
  else
     regis_success  =  { resp_code: '555',resp_desc: 'Please your account has not been verified. Kindly be patient. An agent would speak to you soon to get your account active!'}

  regis_success.to_json

   end


end

post '/appointment_history' do


  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  requester_id = req['requester_id']

  if requester_id != nil


if PreAppointment.appointment_history(requester_id).exists?
  _array = PreAppointment.appointment_history(requester_id)

    p _array
    p _array.to_json

    return _array.to_json
  else
        halt NO_DATA.to_json
  end

  else
     regis_success  =  { resp_code: '555',resp_desc: 'Please your account has not been verified. Kindly be patient. An agent would speak to you soon to get your account active!'}

  regis_success.to_json

   end
end

post '/medical_appointment_detail' do


  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  appointment_id = req['appointment_id']

  # puts appointment_id

  if appointment_id != nil


    if PreAppointment.med_appointment_detail(appointment_id).exists?
      _array = PreAppointment.med_appointment_detail(appointment_id)

      p _array
      p _array.to_json

      return _array.to_json
    else
          halt NO_DATA.to_json
    end

  else
     regis_success  =  { resp_code: '555',resp_desc: 'Please your account has not been verified. Kindly be patient. An agent would speak to you soon to get your account active!'}

  regis_success.to_json 

   end
end



post '/medical_appointment_history' do

  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  requester_id = req['requester_id']

  if requester_id != nil


if PreAppointment.medical_appointment_history(requester_id).exists?
  _array = PreAppointment.medical_appointment_history(requester_id)

    p _array
    p _array.to_json

    return _array.to_json
  else
        halt NO_DATA.to_json
  end

  else
     regis_success  =  { resp_code: '555',resp_desc: 'Please your account has not been verified. Kindly be patient. An agent would speak to you soon to get your account active!'}

  regis_success.to_json

   end
end


post '/medication_appointment_history' do

  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  requester_id = req['requester_id']

  if requester_id != nil

    if PreAppointment.medication_appointment_history(requester_id).exists?
      _array = PreAppointment.medication_appointment_history(requester_id)

      # p _array
      # p _array.to_json
      
      return _array.to_json
    else
      halt NO_DATA.to_json
    end

  else
    regis_success  =  { resp_code: '555',resp_desc: 'Please your account has not been verified. Kindly be patient. An agent would speak to you soon to get your account active!'}

    regis_success.to_json

  end
end

post '/medication_appointment_history_details' do


  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  appointment_id = req['appointment_id']

  if appointment_id != nil


    if PreAppointment.medication_appointment_history_details(appointment_id).exists?
      _array = PreAppointment.medication_appointment_history_details(appointment_id)

      p _array
      p _array.to_json

      return _array.to_json
    else
      halt NO_DATA.to_json
    end

  else
    regis_success  =  { resp_code: '555',resp_desc: 'Please your account has not been verified. Kindly be patient. An agent would speak to you soon to get your account active!'}

    regis_success.to_json

  end
end




post '/phone_consult_history' do

  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  requester_id = req['requester_id']
  appointment_type_id = req['appointment_type_id']

  # puts requester_id

  if requester_id != nil

    if PreAppointment.phone_consult_appointment_history(requester_id,appointment_type_id).exists?
      _array = PreAppointment.phone_consult_appointment_history(requester_id,appointment_type_id)

      p _array
      p _array.to_json

      return _array.to_json
    else
      halt NO_DATA.to_json
    end

  else
    regis_success  =  { resp_code: '555',resp_desc: 'Please your account has not been verified. Kindly be patient. An agent would speak to you soon to get your account active!'}

    regis_success.to_json

  end
end

post '/phone_consult_history_details' do


  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  appointment_id = req['appointment_id']
  appointment_type_id = req['appointment_type_id']
  
  halt INVALID_PRAMS.to_json unless req['appointment_id']
  halt INVALID_PRAMS.to_json unless req['appointment_type_id']

  if appointment_id != nil
    
    _phone_consult_data = ""
    _patient_med_records = ""
  
    if PreAppointment.phone_consult_appointment_history_detail(appointment_id,appointment_type_id).exists?
      _phone_consult_data = PreAppointment.phone_consult_appointment_history_detail(appointment_id,appointment_type_id)
      
      p _phone_consult_data
      puts "------------PHOEN CNSULT DATA----------"
      puts  _phone_consult_data.to_json
       puts "--------------------------------"
       puts "--------------------------------"
        puts "--------------------------------"
         puts "--------------------------------"
      
      confirmed_appointment_id = _phone_consult_data[0].confirmed_appointments_id
      puts "-----------------------CONFIRMED APPT ID" 
      puts confirmed_appointment_id
      puts "--------------------------------"
       puts "--------------------------------"
        puts "--------------------------------"
         puts "--------------------------------"
   
     return {resp_code: '000',resp_desc: "Records found", phone_consult_data: _phone_consult_data}.to_json
     
     end
    
    
    
    

  else
    regis_success  =  { resp_code: '555',resp_desc: 'Please your account has not been verified. Kindly be patient. An agent would speak to you soon to get your account active!'}

    regis_success.to_json

  end
end


post '/personal_doctor_prescription_history' do

  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  requester_id = req['requester_id']
  appointment_type_id = req['appointment_type_id']

  if requester_id != nil


    if PreAppointment.personal_doctor_prescription_history(requester_id,appointment_type_id).exists?
      _array = PreAppointment.personal_doctor_prescription_history(requester_id,appointment_type_id)

      p _array
      p _array.to_json

      return _array.to_json
    else
      halt NO_DATA.to_json
    end

  else
    regis_success  =  { resp_code: '555',resp_desc: 'Please your account has not been verified. Kindly be patient. An agent would speak to you soon to get your account active!'}

    regis_success.to_json

  end
end

post '/personal_doctor_prescription_history_details' do


  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  appointment_id = req['appointment_id']
  appointment_type_id = req['appointment_type_id']
  
  pdsprescription_history_data = ""
   _patient_med_records = ""


  if appointment_id != nil


    if PreAppointment.personal_doctor_prescription_details(appointment_id,appointment_type_id).exists?
      _pdsprescription_history_data = PreAppointment.personal_doctor_prescription_details(appointment_id,appointment_type_id)

      p _pdsprescription_history_data
      p _pdsprescription_history_data.to_json
      
        confirmed_appointment_id = _pdsprescription_history_data[0].confirmed_appointments_id
      p confirmed_appointment_id
      

     return {resp_code: '000',resp_desc: "Records found", pdsprescription_history_data: _pdsprescription_history_data}.to_json
 end

  else
    regis_success  =  { resp_code: '555',resp_desc: 'Please your account has not been verified. Kindly be patient. An agent would speak to you soon to get your account active!'}

    regis_success.to_json

  
  end 
end

post '/video_consult_appointment_history' do


  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  requester_id = req['requester_id']
  appointment_type_id = req['appointment_type_id']

  if requester_id != nil

    if PreAppointment.video_consult_appointment_history(requester_id,appointment_type_id).exists?
      _array = PreAppointment.video_consult_appointment_history(requester_id,appointment_type_id)

      p _array
      p _array.to_json

      return _array.to_json
    else
      halt NO_DATA.to_json
    end

  else
    regis_success  =  { resp_code: '555',resp_desc: 'Please your account has not been verified. Kindly be patient. An agent would speak to you soon to get your account active!'}

    regis_success.to_json

  end
end

post '/video_consult_appointment_detail' do

  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  appointment_id = req['appointment_id']
  appointment_type_id = req['appointment_type_id']
  
  halt INVALID_PRAMS.to_json unless req['appointment_id']
  halt INVALID_PRAMS.to_json unless req['appointment_type_id']

  if appointment_id != nil
    
    _video_consult_data = ""
    _patient_med_records = ""
  
    if PreAppointment.video_consult_appointment_history_detail(appointment_id,appointment_type_id).exists?
      _video_consult_data = PreAppointment.video_consult_appointment_history_detail(appointment_id,appointment_type_id)
      
      p _video_consult_data
      p _video_consult_data.to_json
      
      confirmed_appointment_id = _video_consult_data[0].confirmed_appointments_id
      p confirmed_appointment_id
      
  
      return {resp_code: '000',resp_desc: "Records found", video_consult_data: _video_consult_data}.to_json
       end

  else
    regis_success  =  { resp_code: '555',resp_desc: 'Please your account has not been verified. Kindly be patient. An agent would speak to you soon to get your account active!'}

    regis_success.to_json

  end
  
  
  
  
  
end

post '/homecare_appointment_history' do


  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  requester_id = req['requester_id']
  appointment_type_id = req['appointment_type_id']
  puts appointment_type_id
  
  if requester_id != nil


    if PreAppointment.homecare_appointment_history(requester_id,appointment_type_id).exists?
      _array = PreAppointment.homecare_appointment_history(requester_id,appointment_type_id)

      p _array
      p _array.to_json

      return _array.to_json
    else
      halt NO_DATA.to_json
    end

  else
    regis_success  =  { resp_code: '555',resp_desc: 'Please your account has not been verified. Kindly be patient. An agent would speak to you soon to get your account active!'}

    regis_success.to_json

  end
  
  
  #NEW
  
  
  
    
  
end

post '/homecare_appointment_detail' do


  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  appointment_id = req['appointment_id']
  appointment_type_id = req['appointment_type_id']

  if appointment_id != nil


    if PreAppointment.homecare_appointment_history_detail(appointment_id,appointment_type_id).exists?
      _array = PreAppointment.homecare_appointment_history_detail(appointment_id,appointment_type_id)

      p _array
      p _array.to_json

    
      
      confirmed_appointment_id = _array[0].confirmed_appointments_id
      p confirmed_appointment_id
      
   
    return {resp_code: '000',resp_desc: "Records found", home_care_data: _array}.to_json
   end

  else
    regis_success  =  { resp_code: '555',resp_desc: 'Please your account has not been verified. Kindly be patient. An agent would speak to you soon to get your account active!'}

    regis_success.to_json

  end
end


post '/lab_appointment_history' do

  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  requester_id = req['requester_id']

  if requester_id != nil


    if PreAppointment.lab_appointment_history(requester_id).exists?
      _array = PreAppointment.lab_appointment_history(requester_id)

     

      return _array.to_json
    else
      halt NO_DATA.to_json
    end

  else
    regis_success  =  { resp_code: '555',resp_desc: 'Please your account has not been verified. Kindly be patient. An agent would speak to you soon to get your account active!'}

    regis_success.to_json

  end
end



get '/specialty_masters' do

  response['Access-Control-Allow-Origin'] = '*'



    if SpecialtyMaster.get_specialties().exists?
      _array = SpecialtyMaster.get_specialties()

      p _array
      p _array.to_json

      return _array.to_json
    else
      halt NO_DATA.to_json
    end


end

get '/professional_groups' do

  response['Access-Control-Allow-Origin'] = '*'

    if ProfesionalGroups.get_profesional_groups().exists?
      _array = ProfesionalGroups.get_profesional_groups()

      p _array
      p _array.to_json

      return _array.to_json
    else
      halt NO_DATA.to_json
    end
end


get '/appointment_types' do

  response['Access-Control-Allow-Origin'] = '*'

    if AppointmentType.get_appointment_types().exists?
      _array = AppointmentType.get_appointment_types()

      p _array
      p _array.to_json

      return _array.to_json
    else
      halt NO_DATA.to_json
    end
end

get '/appointment_types1' do

  response['Access-Control-Allow-Origin'] = '*'

    if AppointmentType.get_appointment_types1().exists?
      _array = AppointmentType.get_appointment_types1()

      p _array
      p _array.to_json

      return _array.to_json
    else
      halt NO_DATA.to_json
    end
end



post '/lab_appointment_detail' do


  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  appointment_id = req['appointment_id']
  # puts appointment_id
  if appointment_id != nil


    if PreAppointment.lab_appointment_history_detail(appointment_id).exists?
      _array = PreAppointment.lab_appointment_history_detail(appointment_id)

      p _array
      p _array.to_json

      return _array.to_json
    else
      halt NO_DATA.to_json
    end

  else
    regis_success  =  { resp_code: '555',resp_desc: 'Please your account has not been verified. Kindly be patient. An agent would speak to you soon to get your account active!'}

    regis_success.to_json

  end
end


post '/read_appointment' do


  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  appointment_id = req['appointment_id']
  # puts appointment_id
  if appointment_id != nil

    puts read_appointment(appointment_id)
    
    regis_success  =  { resp_code: '000',resp_desc: 'Appointment has been read.'}
    regis_success.to_json

  else
    regis_success  =  { resp_code: '555',resp_desc: 'This appointment cannot be found!'}

    regis_success.to_json

  end
end


get '/retrieve_order_details1' do
  
  
  
  response['Access-Control-Allow-Origin'] = '*'

  confirmed_appointment_id = params['confirmed_appointment_id']

  puts "THE confirmed_appointment_id IS #{confirmed_appointment_id}"

   # halt ERR_LOCATION.to_json unless params['location']


  if PatientMedRecord.retrieve_order_details1(confirmed_appointment_id).exists?
     _array = PatientMedRecord.retrieve_order_details1(confirmed_appointment_id)
  
      p "-----------------------------"
      p _array
      p _array.to_json
  
      return _array.to_json
  
    else
          halt NO_DATA.to_json
    end
  
end

post '/personal_doc_appointment_history' do


  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'
  
  puts "req = #{req}"

  requester_id = req['requester_id']
  patient_id = req['patient_id']
  doctor_id = req['doctor_id']
  action = req['action']
  
  if action != nil
    if requester_id != nil
      
      halt INVALID_PRAMS.to_json unless req['action']
      
      if action == "fetch_personal_doctor_and_retrieve_investigation"
        
        halt INVALID_PRAMS.to_json unless req['requester_id']
        halt INVALID_PRAMS.to_json unless req['patient_id']
        halt INVALID_PRAMS.to_json unless req['doctor_id']
        
        if PreAppointment.personal_doc_appointment_history(requester_id).exists?

          retrieve = PreAppointment.personal_doc_appointment_history(requester_id)
            retrieved_list = retrieve.as_json
            puts "--------------RETRIEVED LIST----------------"
            puts retrieved_list.inspect
            puts "----------------------------------------------"
            
        else
            retrieved_list = ""
            halt NO_DATA.to_json
        end
        
        if PatientMedRecord.retrieve_order_details(patient_id,doctor_id).exists?
              retrieve1 = PatientMedRecord.retrieve_order_details(patient_id,doctor_id)
              p "-----------------------------"
              p retrieve1
              p retrieve1.to_json
          
            # return retrieve.to_json
          
        else
          retrieve1 = ""
          halt NO_DATA.to_json
        end
        
        
        return { 'retrieved_list' => retrieved_list, 'retrieve1' => retrieve1 }.to_json

        
      end
      
    end
    
  end
end



post '/saveImage' do
  

  response['Access-Control-Allow-Origin'] = '*'
 
  puts "saveImage params = #{params}"
  
  picture_name = ""
  appointment_type_id = ""
  appointment_id = ""
  requester_id = ""
  # halt ERR_MISSING_IMAGE_PARAM.to_json if !params.key?("requester_id")
  halt ERR_MISSING_IMAGE_PARAM.to_json if !params.key?("appointment_type_id")
  halt ERR_MISSING_IMAGE_PARAM.to_json if !params.key?("appointment_id")
  halt ERR_MISSING_IMAGE_PARAM.to_json if !params.key?("fileName")
  
  picture_name = params['appointment_type_id'] + params['appointment_id'] + params['fileName']['filename']
  requester_id = params['requester_id']
  appointment_id = params['appointment_id']
  appointment_type_id = params['appointment_type_id']
 
  saveImage(requester_id,picture_name,appointment_id,appointment_type_id)
 
end



get '/getImage' do
  
  response['Access-Control-Allow-Origin'] = '*'
  
  appointment_id=params["appointment_id"]
  
  getImage(appointment_id)
 
end



post '/lab_appointment' do
  request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'



  # hospital_id = req['provider_id']
  user_id = req['requester_id']
  user_type = req['requester_cat']
  beneficiary_name= req['beneficiary_name']
  beneficiary_age= req['beneficiary_age']
  beneficiary_phone_number= req['beneficiary_phone_number']
  appointment_type_id = req['appointment_type_id']
  proposed_date = req['proposed_date']
  test_list = req['test_list']
  prev_medical_history = req['prev_medical_history']
  req_urgency = req['req_urgency']
  allergies = req['allergies']
  suburb_id = req['suburb_id']

  #########VALIDATING THE PARAMS

  halt ERR_USER_ID.to_json unless req['requester_id']
  halt ERR_CAT.to_json unless req['requester_cat']
  #halt ERR_DATE.to_json unless req.key?('proposed_date')
  halt ERR_DESC.to_json unless req['test_list']


   puts lab = lab_appointment(user_id,user_type,beneficiary_name,beneficiary_age,beneficiary_phone_number,appointment_type_id,proposed_date,test_list,prev_medical_history,req_urgency,allergies,"APP",suburb_id)
   puts "lab id returned = #{lab.to_s}"
   regis_success  =  { resp_code: '000',resp_desc: 'Your Lab Appointment has been made successfully. You would be contacted shortly. Thank you!', appointment_id: lab.to_s}


  regis_success.to_json


end


post '/medication' do
  request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'


  user_id = req['requester_id']
  user_type = req['requester_cat']
  beneficiary_name= req['beneficiary_name']
  beneficiary_age= req['beneficiary_age']
  beneficiary_phone_number= req['beneficiary_phone_number']

  appointment_type_id = req['appointment_type_id']
  proposed_date = req['proposed_date']
  medication = req['medication']
  prev_medical_history = req['prev_medical_history']
  req_urgency = req['req_urgency']
  allergies = req['allergies']
  suburb_id = req['suburb_id']

  #########VALIDATING THE PARAMS

  halt ERR_USER_ID.to_json unless req['requester_id']
  halt ERR_CAT.to_json unless req['requester_cat']
  #halt ERR_DATE.to_json unless req.key?('proposed_date')
  halt ERR_DESC.to_json unless req['medication']

    puts "############## MEDICATION ###################"
    puts req
   puts medication_id = medication1(user_id,user_type,beneficiary_name,beneficiary_age,beneficiary_phone_number,appointment_type_id,proposed_date,medication,prev_medical_history,req_urgency,allergies,"APP",suburb_id)
   regis_success  =  { resp_code: '000',resp_desc: 'Your request for medication has been made successfully. You would be contacted shortly. Thank you!', appointment_id: medication_id.to_s}
    puts "############## MEDICATION ###################"
  regis_success.to_json


end

get "/lab_services" do

 response['Access-Control-Allow-Origin'] = '*'
 id = params['id']



if LabServices.get_services(id).exists?
   _array = LabServices.get_services(id)

    p "-----------------------------"
    p _array
    p _array.to_json

    return _array.to_json

  else
        halt NO_DATA.to_json
  end

end



get "/drugs_services" do

 response['Access-Control-Allow-Origin'] = '*'
 id = params['id']


   _array = DrugMaster.get_drugs(id)

    p "-----------------------------"
    p _array
    p _array.to_json

    return _array.to_json



end



get '/get_confirmed_appointments_by_patient_count' do

  response['Access-Control-Allow-Origin'] = '*'

  patient_id = params['patient_id']
  
  halt INVALID_PRAMS.to_json unless params['patient_id']
  puts "THE patient_id IS #{patient_id}"

  if ConfirmedAppointment.confirmed_appointments_by_patient(patient_id).exists?
    _array = ConfirmedAppointment.confirmed_appointments_by_patient_count(patient_id)[0]["counter"]
    p "-----------------------------"
    p _array
    p _array.to_json
    
    
    results1  =  { resp_code: '000',resp_desc: 'Records found!', record_count: _array }.to_json

    return results1
    
  else
     halt NO_DATA.to_json
  end

end

get '/get_confirmed_appointments_by_patient' do

  response['Access-Control-Allow-Origin'] = '*'

  patient_id = params['patient_id']
  
  halt INVALID_PRAMS.to_json unless params['patient_id']
  puts "THE patient_id IS #{patient_id}"

  if ConfirmedAppointment.confirmed_appointments_by_patient(patient_id).exists?
    _array = ConfirmedAppointment.confirmed_appointments_by_patient(patient_id)
    #_md_doc_gen_appointment_count = PreConfirmedAppointment.general_doc_md_appointments_count(doc_id)
    p "-----------------------------"
    p _array
    p _array.to_json
    
    
    results1  =  { resp_code: '000',resp_desc: 'Records found!', records: _array }.to_json

    return results1
    
  else
     halt NO_DATA.to_json
  end

end



get '/get_confirmed_appointments_by_doctor' do

  response['Access-Control-Allow-Origin'] = '*'

  doc_id = params['doc_id']
  
  halt INVALID_PRAMS.to_json unless params['doc_id']
  puts "THE doc_id IS #{doc_id}"

  if ConfirmedAppointment.confirmed_appointments_by_doctor(doc_id).exists?
    _array = ConfirmedAppointment.confirmed_appointments_by_doctor(doc_id)
      p "-----------------------------"
    p _array
    p _array.to_json
    
   
    return _array.to_json
    
  else
     halt NO_DATA.to_json
  end

end



post '/personal_doc' do

  request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'




  user_id = req['requester_id']
  user_type = req['requester_cat']
  beneficiary_name= req['beneficiary_name']
  appointment_type_id = req['appointment_type_id']
  proposed_date = req['proposed_date']
  complaint_desc = req['complaint_desc']
  prev_medical_history = req['prev_medical_history']
  req_urgency = req['req_urgency']
  allergies = req['allergies']
  suburb_id = req['suburb_id']
  #########VALIDATING THE PARAMS
  # halt ERR_HOSPITAL_ID.to_json unless req['provider_id']
  halt ERR_USER_ID.to_json unless req['requester_id']
  halt ERR_CAT.to_json unless req['requester_cat']
  #halt ERR_DATE.to_json unless req.key?('proposed_date')
  halt ERR_DESC.to_json unless req['complaint_desc']


   puts insert_doc_app( user_id,user_type,beneficiary_name,appointment_type_id,proposed_date,complaint_desc,prev_medical_history,req_urgency,allergies,"APP",suburb_id)
   regis_success  =  { resp_code: '000',resp_desc: 'Appointment made Successfully. You would be contacted shortly. Thank you!'}

  regis_success.to_json

end

post '/retrieve_doc' do

    request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'

  email = req['email']

  puts "THE email IS #{email}"



   puts "THE email IS #{email}"

    if Registration.retrieve_confirm(email).exists?
        retrieve = Registration.retrieve_confirm(email)
    p "-----------------------------"
    p retrieve
    p retrieve.to_json

     return retrieve.to_json

      else
        halt NO_DATA.to_json
    end


return check.to_json

end

post '/retrieve_doc2' do

  request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'

  id = req['id']

  puts "---------------------------------------------------"
  puts "THE ID IN retrieve_doc2 IS #{id}"
  puts "-----------------------------------------------------"

  if id.nil?

     puts "DO nothing"

  else

  if PersonInfo.retrieve_doctor_regis(id).exists?
    retrieve = PersonInfo.retrieve_doctor_regis(id)
    p "-----------------------------"
    p retrieve
    p retrieve.to_json

  return retrieve.to_json

  else
    halt NO_DATA.to_json
  end
end
  #return check.to_json

end

post '/doctor_patient' do

    request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'

  id = req['id']

     puts "---------------------------------------------------"
    puts "THE ID IS #{id}"
   puts "-----------------------------------------------------"

    if ConfirmedPersonalDoctorService.doctor_patients(id).exists?
        retrieve = ConfirmedPersonalDoctorService.doctor_patients(id)
        retrieved_list = retrieve.as_json
        puts "--------------RETRIEVED LIST----------------"
        puts retrieved_list.int
        puts "----------------------------------------------"

        results = []

        retrieved_list.each do |retrieved|
          patient=PersonInfo.find(retrieved['patient_id'])
          registration_obj = Registration.find(patient.reg_id)
          patient = patient.as_json
          puts "LETS SEE PATIENT"
          puts patient
          puts "--------------------------------------"
          patient["mobile_number"] = registration_obj.mobile_number
          puts "LETS SEE PATIENT- mobile and reg"
          puts retrieved.merge(patient)
          puts
          puts "--------------------------------------"
          # # patient["requests"] = PreAppointment.select("appointment_type_id").where(requester_id: retrieved['patient_id'] ).as_json
          results << retrieved.merge(patient)
          puts "-----------------------------------------------"
          puts patient.as_json
          puts "-----------------------------------------------"
        end
    p "-----------------------------"
    # p retrieve
    p results.to_json

     return results.to_json

      else
        halt NO_DATA.to_json
    end



return check.to_json

end

post '/doctor_patient3' do

    request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'

  id = req['id']

     puts "---------------------------------------------------"
    puts "THE ID IS #{id}"
   puts "-----------------------------------------------------"

    if ConfirmedPersonalDoctorService.doctor_patients3(id).exists?
        retrieve = ConfirmedPersonalDoctorService.doctor_patients3(id)
        retrieved_list = retrieve.as_json
        puts "--------------RETRIEVED LIST----------------"
        puts retrieved_list.inspect
        puts "----------------------------------------------"

        results = []

        retrieved_list.each do |retrieved|
          patient=PersonInfo.find(retrieved['patient_id'])
          registration_obj = Registration.find(patient.reg_id)
          patient = patient.as_json
          patient["mobile_number"] = registration_obj.mobile_number
         
          results << retrieved.merge(patient)
          puts "-----------------------------------------------"
          puts patient.as_json
          puts "-----------------------------------------------"
        end
    p "-----------------------------"
    # p retrieve
    p results.to_json

     return results.to_json

      else
        halt NO_DATA.to_json
    end



return check.to_json

end


post '/doctor_patient4' do

    request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'

  id = req['id']

     puts "---------------------------------------------------"
    puts "THE ID IS #{id}"
   puts "-----------------------------------------------------"

    if ConfirmedPersonalDoctorService.doctor_patients4(id).exists?
        retrieve = ConfirmedPersonalDoctorService.doctor_patients4(id)
        retrieved_list = retrieve.as_json
        puts "--------------RETRIEVED LIST----------------"
        puts retrieved_list.inspect
        puts "----------------------------------------------"

        results = []

        retrieved_list.each do |retrieved|
          patient=PersonInfo.find(retrieved['patient_id'])
          registration_obj = Registration.find(patient.reg_id)
          patient = patient.as_json
          patient["mobile_number"] = registration_obj.mobile_number
          # patient["requests"] = PreAppointment.select("appointment_type_id").where(requester_id: retrieved['patient_id'] ).as_json
          results << retrieved.merge(patient)
          puts "--------------------- patient --------------------------"
          puts patient.as_json
          puts "----------------------patient -------------------------"
        end
    p "-----------------------------"
    # p retrieve
    p results.to_json

     return results.to_json

      else
        halt NO_DATA.to_json
    end



return check.to_json

end

post '/doctor_patient2' do

    request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'

  id = req['id']

     puts "---------------------------------------------------"
    puts "THE ID IS #{id}"
   puts "-----------------------------------------------------"

    if ConfirmedPersonalDoctorService.doctor_patients2(id).exists?
        retrieve = ConfirmedPersonalDoctorService.doctor_patients2(id)
        retrieved_list = retrieve.as_json
        puts "--------------RETRIEVED LIST----------------"
        puts retrieved_list.inspect
        puts "----------------------------------------------"

        results = []

        retrieved_list.each do |retrieved|
          patient=PersonInfo.find(retrieved['patient_id'])
          registration_obj = Registration.find(patient.reg_id)
          patient = patient.as_json
          patient["mobile_number"] = registration_obj.mobile_number
          # patient["requests"] = PreAppointment.select("appointment_type_id").where(requester_id: retrieved['patient_id'] ).as_json
          results << retrieved.merge(patient)
          puts "-----------------------------------------------"
          puts patient.as_json
          puts "-----------------------------------------------"
        end
    p "-----------------------------"
    # p retrieve
    p results.to_json

     return results.to_json

      else
        halt NO_DATA.to_json
    end



return check.to_json

end



post '/prescription_list' do

    request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'

    id = req['id']
    appointment_type_id = req['appointment_type_id']

     puts "---------------------------------------------------"
    puts "THE ID IS #{id}"
   puts "-----------------------------------------------------"

    if ConfirmedPersonalDoctorService.prescription_list(id,appointment_type_id).exists?
        retrieve = ConfirmedPersonalDoctorService.prescription_list(id,appointment_type_id)
        retrieved_list = retrieve.as_json
        puts "--------------RETRIEVED LIST----------------"
        puts retrieved_list.inspect
        puts "----------------------------------------------"

        results = []

        retrieved_list.each do |retrieved|
          patient=PersonInfo.find(retrieved['patient_id'])
          registration_obj = Registration.find(patient.reg_id)
          patient = patient.as_json
          patient["mobile_number"] = registration_obj.mobile_number
          # patient["requests"] = PreAppointment.select("appointment_type_id").where(requester_id: retrieved['patient_id'] ).as_json
          results << retrieved.merge(patient)
          puts "-----------------------------------------------"
          puts patient.as_json
          puts "-----------------------------------------------"
        end
    p "-----------------------------"
    # p retrieve
    p results.to_json

     return results.to_json

      else
        halt NO_DATA.to_json
    end



return check.to_json

end


post '/order_investigation' do
  
  request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'

  patient_id = req['patient_id']
  doctor_id = req['doctor_id']
  clinical_complaints = req['clinical_complaints']
  clinical_examinations= req['clinical_examinations']
  working_diagnosis = req['working_diagnosis']
  investigation_rquired = req['investigation_rquired']
  treatments = req['treatments']
  follow_up_plan = req['follow_up_plan']
  confirmed_appointment_id = req['confirmed_appointment_id']
  
  halt INVALID_PRAMS.to_json unless req['confirmed_appointment_id']
  halt INVALID_PRAMS.to_json unless req['doctor_id']

   puts order_appointment(patient_id, doctor_id,clinical_complaints,clinical_examinations,working_diagnosis,investigation_rquired,treatments,follow_up_plan,confirmed_appointment_id)
   regis_success  =  { resp_code: '000',resp_desc: 'Clinical record saved Successfully. Thank you!'}

  regis_success.to_json

end


post "/attend_to_pds_prescription" do

  request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'

  patient_id = req['patient_id']
  appointment_id = req['appointment_id']
  
   puts attend_prescription(patient_id,appointment_id)
   regis_success  =  { resp_code: '000',resp_desc: 'Prescription record saved Successfully. Thank you!'}

  regis_success.to_json

end


post '/retrieve_order_investigation' do

  request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'

  patient_id = req['patient_id']
  doctor_id = req['doctor_id']
  
  halt INVALID_PRAMS.to_json unless req['patient_id']
  halt INVALID_PRAMS.to_json unless req['doctor_id']


  puts "---------------------------------------------------"
  puts "THE patient_id IN retrieve_order_investigation IS #{patient_id} AND #{doctor_id}"
  puts "-----------------------------------------------------"

  if patient_id.nil?

     puts "DO nothing"

  else

    if PatientMedRecord.retrieve_order_details(patient_id,doctor_id).exists?
      retrieve = PatientMedRecord.retrieve_order_details(patient_id,doctor_id)
      p "-----------------------------"
      p retrieve
      p retrieve.to_json
  
    return retrieve.to_json
  
    else
      halt NO_DATA.to_json
    end
end
  #return check.to_json

end

post '/refer_patient' do

  request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'

  patient_surname = req['surname']
  patient_other_names = req['other_names']
  patient_dob = req['dob']
  patient_contact = req['contact']
  patient_refered_by = req['refered_by']

  puts "---------------------------------------------------"
  puts "THE refer patient incoming req IS #{req}"
  puts "-----------------------------------------------------"

  halt ERR_SURNAME.to_json unless req['surname']
  halt ERR_OTHR_NAME.to_json unless req['other_names']
  halt ERR_DOB.to_json unless req.key?('dob')
  halt ERR_MOBILE.to_json unless req['contact']

  puts "############## REFER PATIENT ###################"
  puts referpatient(patient_surname,patient_other_names,patient_dob,patient_contact,patient_refered_by)
  regis_success  =  { resp_code: '000',resp_desc: 'Patient has been referred successfully. Thank you!'}
  puts "############## REFER PATIENT ###################"
  regis_success.to_json
  
end


get '/get_patients_referred' do

  #request.body.rewind
  #req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'

  doc_id = params['doc_id']
  halt INVALID_PRAMS.to_json unless params['doc_id']

  puts "THE doc_id IS #{doc_id}"

   # halt ERR_LOCATION.to_json unless params['location']

  if Referal.retrieve_patients_referred(doc_id).exists?
   _array = Referal.retrieve_patients_referred(doc_id)

    p "-----------------------------"
    p _array
    p _array.to_json

    return _array.to_json

  else
        halt NO_DATA.to_json
  end

end


get '/get_new_general_appointments' do

  response['Access-Control-Allow-Origin'] = '*'

  doc_id = params['doc_id']
  person_type = params['person_type'].tr('"', '')
  halt INVALID_PRAMS.to_json unless params['doc_id']
  
  me = 'P'
  puts "get_new_general_appointments - THE doc_id IS #{doc_id} AND person_type = #{person_type.tr('"', '')} AND me1 = #{me} AND me = #{me.tr('"', '')}"

  # if PreConfirmedAppointment.new_general_appointments(doc_id,person_type).exists?
   _array = PreConfirmedAppointment.new_general_appointments(doc_id,person_type)
   
   # _count = 0
   puts "get_new_general_appointments - person_type = #{person_type}"
   if PreConfirmedAppointment.new_general_appointments_count(doc_id,person_type)
     _count = PreConfirmedAppointment.new_general_appointments_count(doc_id,person_type)#[0]["counter"]
   else
     _count = 0
   end
    
    p "-----------------------------"
    p "get_new_general_appointments _array = #{_array}"
    p "get_new_general_appointments _array.to_json = #{_array.to_json}"
    
    p "get_new_general_appointments _count.to_json = #{_count.to_json}"
    p _count.to_json
    
    results  =  { resp_code: '000',resp_desc: 'Records found!', count: _count[0]["counter"], records: _array }.to_json

    # return _array.to_json
    return results
    
  # else
     # halt NO_DATA.to_json
  # end

end


get '/get_patient_appointments_count' do

  response['Access-Control-Allow-Origin'] = '*'

  patient_id = params['patient_id']
  halt INVALID_PRAMS.to_json unless params['patient_id']
  puts "THE patient_id IS #{patient_id}"

  # if PreConfirmedAppointment.new_general_appointments_count(doc_id).exists?
    _medical_appointment_count = PreConfirmedAppointment.medical_appointments_count(patient_id)[0]["counter"]
    _medication_appointment_count = PreConfirmedAppointment.medication_appointments_count(patient_id)[0]["counter"]
    _labrequest_appointment_count = PreConfirmedAppointment.labrequest_appointments_count(patient_id)[0]["counter"]
    _homecare_appointment_count = PreConfirmedAppointment.homecare_appointments_count(patient_id)[0]["counter"]
    #_phoneconsult_appointment_count = PreConfirmedAppointment.phoneconsult_appointments_count(patient_id)[0]["counter"]
    _videoconsult_appointment_count = PreConfirmedAppointment.videoconsult_appointments_count(patient_id)[0]["counter"]
    # _hc_doc_gen_appointment_count = PreConfirmedAppointment.general_doc_hc_appointments_count(doc_id)
    
    #PDS counts
    _pdhc_patient_appointment_count = PreConfirmedAppointment.pdhc_appointments_count(patient_id)[0]["counter"]
    _pdpc_patient_appointment_count = PreConfirmedAppointment.pdpc_appointments_count(patient_id)[0]["counter"]
    _pdvc_patient_appointment_count = PreConfirmedAppointment.pdvc_appointments_count(patient_id)[0]["counter"]
    _pddp_patient_appointment_count = PreConfirmedAppointment.pddp_appointments_count(patient_id)[0]["counter"]
    
    _total_patient_pds_appointment_count = PreConfirmedAppointment.patient_pds_appointments_count(patient_id)[0]["counter"]


    
    _all_appointments_overall_total = _medical_appointment_count + _medication_appointment_count + _labrequest_appointment_count + _homecare_appointment_count + _videoconsult_appointment_count + _total_patient_pds_appointment_count
    
    
    # _appointments_total_without_pds = _total_count1 + _total_doc_gen_appointment_count1
    
    # _all_appointments_overall_total =  PreConfirmedAppointment.total_all_appointments_count(doc_id)
    
    p "--------------TOTAL APPOINTMENT COUNT---------------"
    # p _general_count.to_json
    # p _pds_count.to_json
    # p _total_count.to_json
    p _all_appointments_overall_total.to_json
    
   
        
    results  =  { resp_code: '000',resp_desc: 'Records found!',medical_appointment_count: _medical_appointment_count,medication_appointment_count: _medication_appointment_count,labrequest_appointment_count: _labrequest_appointment_count,homecare_appointment_count: _homecare_appointment_count, videoconsult_appointment_count: _videoconsult_appointment_count,
    pdhc_patient_appointment_count: _pdhc_patient_appointment_count, pdpc_patient_appointment_count: _pdpc_patient_appointment_count, pdvc_patient_appointment_count: _pdvc_patient_appointment_count,
    pddp_patient_appointment_count: _pddp_patient_appointment_count, total_patient_pds_appointment_count: _total_patient_pds_appointment_count,all_appointments_overall_total: _all_appointments_overall_total}.to_json


    return results
    
  # else
     # halt NO_DATA.to_json
  # end

end

get '/get_new_appointments_count' do

  response['Access-Control-Allow-Origin'] = '*'

  doc_id = params['doc_id']
  person_type = params['person_type'].tr('"', '')
  
  halt INVALID_PRAMS.to_json unless params['doc_id']
  
  puts "get_new_appointments_count params = #{params}"
  puts "get_new_appointments_count - THE doc_id IS #{doc_id} AND person_type = #{person_type}"
  
  new_appointments_count(doc_id,person_type)

end


get '/get_new_general_appointments_details' do

  response['Access-Control-Allow-Origin'] = '*'

  record_id = params['record_id']
  
  halt INVALID_PRAMS.to_json unless params['record_id']
  puts "THE id IS #{record_id}"
  

  if PreConfirmedAppointment.new_general_appointments_details(record_id).exists?
   _array = PreConfirmedAppointment.new_general_appointments_details(record_id)
    p "-----------------------------"
    p _array
    p _array.to_json

    return _array.to_json
    
  else
     halt NO_DATA.to_json
  end

end



get '/get_new_personaldoctorserviceappointments' do

  response['Access-Control-Allow-Origin'] = '*'

  doc_id = params['doc_id']
  halt INVALID_PRAMS.to_json unless params['doc_id']
  puts "THE doc_id IS #{doc_id}"

  if PreConfirmedAppointment.new_personaldoctorserviceappointments(doc_id).exists?
   _array = PreConfirmedAppointment.new_personaldoctorserviceappointments(doc_id)
    p "-----------------------------"
    p _array
    p _array.to_json

    return _array.to_json
    
  else
     halt NO_DATA.to_json
  end

end

get '/get_new_personaldoctorserviceappointments_details' do

  response['Access-Control-Allow-Origin'] = '*'

  record_id = params['record_id']
  halt INVALID_PRAMS.to_json unless params['record_id']
  puts "THE record_id IS #{record_id}"

  if PreConfirmedAppointment.new_personaldoctorserviceappointments_details(record_id).exists?
   _array = PreConfirmedAppointment.new_personaldoctorserviceappointments_details(record_id)
    p "-----------------------------"
    p _array
    p _array.to_json

    return _array.to_json
    
  else
     halt NO_DATA.to_json
  end

end

get '/get_general_appointments_medication' do

  response['Access-Control-Allow-Origin'] = '*'

  doc_id = params['doc_id']
  halt INVALID_PRAMS.to_json unless params['doc_id']
  puts "THE doc_id IS #{doc_id}"

  if PreConfirmedAppointment.general_appointments_medication(doc_id).exists?
   _array = PreConfirmedAppointment.general_appointments_medication(doc_id)
   _md_doc_gen_appointment_count = PreConfirmedAppointment.general_doc_md_appointments_count(doc_id)
    p "-----------------------------"
    p _array
    p _array.to_json
    
    
    results1  =  { resp_code: '000',resp_desc: 'Records found!', count: _md_doc_gen_appointment_count, records: _array }.to_json

    return results1
    
  else
     halt NO_DATA.to_json
  end

end

get '/get_general_appointments_medication_details' do

  response['Access-Control-Allow-Origin'] = '*'

  record_id = params['record_id']
  halt INVALID_PRAMS.to_json unless params['record_id']
  puts "THE record_id IS #{record_id}"

  if PreConfirmedAppointment.general_appointments_medication_details(record_id).exists?
   _array = PreConfirmedAppointment.general_appointments_medication_details(record_id)
    p "-----------------------------"
    p _array
    p _array.to_json

    return _array.to_json
    
  else
     halt NO_DATA.to_json
  end

end

get '/get_general_appointments_videoconsult' do

  response['Access-Control-Allow-Origin'] = '*'

  doc_id = params['doc_id']
  halt INVALID_PRAMS.to_json unless params['doc_id']
  puts "THE doc_id IS #{doc_id}"

  if PreConfirmedAppointment.general_appointments_videoconsult(doc_id).exists?
   _array = PreConfirmedAppointment.general_appointments_videoconsult(doc_id)
   _vc_doc_gen_appointment_count = PreConfirmedAppointment.general_doc_vc_appointments_count(doc_id)
    p "-----------------------------"
    p _array
    p _array.to_json
    
     results1  =  { resp_code: '000',resp_desc: 'Records found!', count: _vc_doc_gen_appointment_count, records: _array }.to_json

    return results1
    
  else
     halt NO_DATA.to_json
  end

end


get '/get_general_appointments_videoconsult_details' do

  response['Access-Control-Allow-Origin'] = '*'

  record_id = params['record_id']
  halt INVALID_PRAMS.to_json unless params['record_id']
  puts "THE record_id IS #{record_id}"

  if PreConfirmedAppointment.general_appointments_videoconsult_details(record_id).exists?
   _array = PreConfirmedAppointment.general_appointments_videoconsult_details(record_id)
    p "-----------------------------"
    p _array
    p _array.to_json

    return _array.to_json
    
  else
     halt NO_DATA.to_json
  end

end


get '/get_general_appointments_homecare' do

  response['Access-Control-Allow-Origin'] = '*'

  doc_id = params['doc_id']
  person_type = params['person_type'].tr('"', '')
  
  halt INVALID_PRAMS.to_json unless params['doc_id']
  halt INVALID_PERSONTYPE_PRAMS.to_json unless params['person_type']
  puts "THE doc_id IS #{doc_id} AND person_type = #{person_type}"

  if PreConfirmedAppointment.general_appointments_homecare(doc_id,person_type).exists?
    _array = PreConfirmedAppointment.general_appointments_homecare(doc_id,person_type)
    _hc_doc_gen_appointment_count = PreConfirmedAppointment.general_doc_hc_appointments_count(doc_id,person_type)
    p "-----------------------------"
    p _array
    p _array.to_json
    
    results1  =  { resp_code: '000',resp_desc: 'Records found!', count: _hc_doc_gen_appointment_count, records: _array }.to_json
     
    return results1
    
  else
     halt NO_DATA.to_json
  end

end


get '/get_general_appointments_homecare_details' do

  response['Access-Control-Allow-Origin'] = '*'

  record_id = params['record_id']
  halt INVALID_PRAMS.to_json unless params['record_id']
  puts "THE record_id IS #{record_id}"

  if PreConfirmedAppointment.general_appointments_homecare_details(record_id).exists?
   _array = PreConfirmedAppointment.general_appointments_homecare_details(record_id)
    p "-----------------------------"
    p _array
    p _array.to_json

    return _array.to_json
    
  else
     halt NO_DATA.to_json
  end

end



get '/get_doc_pds_appointments_prescription' do

  response['Access-Control-Allow-Origin'] = '*'

  doc_id = params['doc_id']
  halt INVALID_PRAMS.to_json unless params['doc_id']
  puts "THE doc_id IS #{doc_id}"

  if PreConfirmedAppointment.doc_pds_appointments_prescription(doc_id).exists?
   _array = PreConfirmedAppointment.doc_pds_appointments_prescription(doc_id)
    p "-----------------------------"
    p _array
    p _array.to_json

    return _array.to_json
    
  else
     halt NO_DATA.to_json
  end

end


get '/get_doc_pds_appointments_prescription_details' do

  response['Access-Control-Allow-Origin'] = '*'

  record_id = params['record_id']
  halt INVALID_PRAMS.to_json unless params['record_id']
  puts "THE record_id IS #{record_id}"

  if PreConfirmedAppointment.doc_pds_appointments_prescription_details(record_id).exists?
   _array = PreConfirmedAppointment.doc_pds_appointments_prescription_details(record_id)
    p "-----------------------------"
    p _array
    p _array.to_json

    return _array.to_json
    
  else
     halt NO_DATA.to_json
  end

end



get '/get_doc_pds_appointments_phoneconsult' do

  response['Access-Control-Allow-Origin'] = '*'

  doc_id = params['doc_id']
  halt INVALID_PRAMS.to_json unless params['doc_id']
  puts "THE doc_id IS #{doc_id}"

  if PreConfirmedAppointment.doc_pds_appointments_phoneconsult(doc_id).exists?
   _array = PreConfirmedAppointment.doc_pds_appointments_phoneconsult(doc_id)
    p "-----------------------------"
    p _array
    p _array.to_json

    return _array.to_json
    
  else
     halt NO_DATA.to_json
  end

end


get '/get_doc_pds_appointments_phoneconsult_details' do

  response['Access-Control-Allow-Origin'] = '*'

  record_id = params['record_id']
  halt INVALID_PRAMS.to_json unless params['record_id']
  puts "THE record_id IS #{record_id}"

  if PreConfirmedAppointment.doc_pds_appointments_phoneconsult_details(record_id).exists?
   _array = PreConfirmedAppointment.doc_pds_appointments_phoneconsult_details(record_id)
    p "-----------------------------"
    p _array
    p _array.to_json

    return _array.to_json
    
  else
     halt NO_DATA.to_json
  end

end



get '/get_doc_pds_appointments_videoconsult' do

  response['Access-Control-Allow-Origin'] = '*'

  doc_id = params['doc_id']
  halt INVALID_PRAMS.to_json unless params['doc_id']
  puts "THE doc_id IS #{doc_id}"

  if PreConfirmedAppointment.doc_pds_appointments_videoconsult(doc_id).exists?
   _array = PreConfirmedAppointment.doc_pds_appointments_videoconsult(doc_id)
    p "-----------------------------"
    p _array
    p _array.to_json

    return _array.to_json
    
  else
     halt NO_DATA.to_json
  end

end


get '/get_doc_pds_appointments_videoconsult_details' do

  response['Access-Control-Allow-Origin'] = '*'

  record_id = params['record_id']
  halt INVALID_PRAMS.to_json unless params['record_id']
  puts "THE record_id IS #{record_id}"

  if PreConfirmedAppointment.doc_pds_appointments_videoconsult_details(record_id).exists?
   _array = PreConfirmedAppointment.doc_pds_appointments_videoconsult_details(record_id)
    p "-----------------------------"
    p _array
    p _array.to_json

    return _array.to_json
    
  else
     halt NO_DATA.to_json
  end

end



get '/get_doc_pds_appointments_homecare' do

  response['Access-Control-Allow-Origin'] = '*'

  doc_id = params['doc_id']
  halt INVALID_PRAMS.to_json unless params['doc_id']
  
  puts "THE doc_id IS #{doc_id}"

  if PreConfirmedAppointment.doc_pds_appointments_homecare(doc_id).exists?
   _array = PreConfirmedAppointment.doc_pds_appointments_homecare(doc_id)
    p "-----------------------------"
    p _array
    p _array.to_json

    return _array.to_json
    
  else
     halt NO_DATA.to_json
  end

end


get '/get_doc_pds_appointments_homecare_details' do

  response['Access-Control-Allow-Origin'] = '*'

  record_id = params['record_id']
  halt INVALID_PRAMS.to_json unless params['record_id']
  
  puts "THE record_id IS #{record_id}"

  if PreConfirmedAppointment.doc_pds_appointments_homecare_details(record_id).exists?
   _array = PreConfirmedAppointment.doc_pds_appointments_homecare_details(record_id)
    p "-----------------------------"
    p _array
    p _array.to_json

    return _array.to_json
    
  else
     halt NO_DATA.to_json
  end

end


post '/new_doc_appointment_accept_decline' do
  request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'
  
  appt_id = req['appt_id']
  action = req['action']
  pre_confirmed_id = req['pre_confirmed_id']
  available_date1 = req['available_date']
  comment1 = req['comment']
  professional_id = req['professional_id']
  my_person_type = req['my_person_type']
  person_type = req['person_type']
  
  puts "new_doc_appointment_accept_decline req = #{req}"
  
  
  halt INVALID_APPTID_PRAMS.to_json unless req['appt_id']
  halt INVALID_ACTION_PRAMS.to_json unless req['action']
  halt INVALID_PERSONTYPE_PRAMS.to_json unless req['person_type']  

  puts "############## NEW DOCTOR APPOINTMENT ACCEPT/DECLINE START ###################"
  
    if action == "A" #accept action
      pre_confirmed_appt_for_all = PreConfirmedAppointment.where(pre_appointment_id: appt_id )
      closed_status = pre_confirmed_appt_for_all.last.closed
      if closed_status
        doc_action_failed = {resp_code: '111', resp_desc: "Sorry the appointment no longer exist"}
      else
        puts new_doc_appointment_accept(appt_id,pre_confirmed_id,person_type)
        
        halt INVALID_ACTION_PRAMS.to_json unless req['professional_id']
        
        if ConfirmedAppointment.accepted_appointments_details_for_sms(appt_id).exists?
          _array = ConfirmedAppointment.accepted_appointments_details_for_sms(appt_id)[0]
          patient_name = _array["surname"] + " " + _array["other_names"]
          puts recipient_number = _array["contact"]
          recipient_email = _array["email"]
          
          if PersonInfo.retrieve_doctor_regis(professional_id).exists?
            _doctor_records = PersonInfo.retrieve_doctor_regis(professional_id)[0]
            doctor_name = _doctor_records["surname"] + " " + _doctor_records["other_names"]
          end
          puts  "viewing more details #{_array.to_json}"
          
          if person_type 
            case person_type
            when "D"
               message_body = "Hello #{patient_name}, your appointment for #{_array["appointment_type"]} has been accepted by Dr. #{doctor_name}. Appointment Date: #{_array["confirmed_date"].strftime("%F %I:%M %p")}. Thank you."

            when "N"
               message_body = "Hello #{patient_name}, your appointment for #{_array["appointment_type"]} has been accepted by Nurse #{doctor_name}. Appointment Date: #{_array["confirmed_date"].strftime("%F %I:%M %p")}. Thank you."

            else
              return
            end
    
          end
      
          
          
          halt INVALID_ACTION_PRAMS.to_json unless recipient_number
          halt INVALID_ACTION_PRAMS.to_json unless message_body
          
          sendmsg(APP_NAME, recipient_number, message_body)
          
          email_sub = "#{_array["appointment_type"]} has been Accepted."
          mess_body = "Your appointment for #{_array["appointment_type"]} has been accepted by Nurse #{doctor_name}. \n\n Appointment Date: #{_array["confirmed_date"].strftime("%F %I:%M %p")}.\n"
          approve_deny_appoint_Sendemail(recipient_email,patient_name,email_sub,mess_body)
          
        end

        doc_action_success  =  { resp_code: '000',resp_desc: 'Appointment has been accepted Successfully. Thank you!'}
      end
    
    else
      #decline action
      halt INVALID_AVAILDATE_PRAMS.to_json unless req['available_date']
      halt INVALID_PROFID_PRAMS.to_json unless req['professional_id']
     puts new_doc_appointment_decline(appt_id,pre_confirmed_id,available_date1,comment1,professional_id)
     doc_action_success  =  { resp_code: '000',resp_desc: 'Appointment Declined successfully. Thank you!'}
    end
  puts "############## NEW DOCTOR APPOINTMENT ACCEPT/DECLINE END ###################"
  
  doc_action_success.to_json

end


post '/bill_details' do
  request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'
  
  confirmed_appointment_id = ""
  appointment_type_id = ""
  req_urgency_ref = ""
  
  bill_info_id = req['bill_info_id']
  confirmed_appointment_id = req['confirmed_appointment_id']
  appointment_type_id = req['appointment_type_id']
  req_urgency_ref = req['req_urgency_ref']
  
  puts "bill_details req = #{req}"
  
  halt INVALID_APPTID_PRAMS.to_json unless req['bill_info_id']
  # halt INVALID_ACTION_PRAMS.to_json unless req['action']
   
  if BillingInfoBillItems.bill_details(bill_info_id,confirmed_appointment_id,appointment_type_id,req_urgency_ref).exists?
   _array = BillingInfoBillItems.bill_details(bill_info_id,confirmed_appointment_id,appointment_type_id,req_urgency_ref)
    p "-----------------------------"
    p _array
    p _array.to_json

    return _array.to_json
    
  else
     halt NO_DATA.to_json
  end
  
end

post '/accept_or_reject_bill' do
  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'
  
  patient_billing_infos_id = req['patient_billing_infos_id']
  action = req['action']
  
  puts "accept_or_reject_bill req = #{req} and action = #{action}"
  
  halt INVALID_BILLINFOID_PRAMS.to_json unless req['patient_billing_infos_id']
  halt INVALID_ACTION_PRAMS.to_json unless req['action']
  
  puts "############## Patient Bill ACCEPT/DECLINE START ###################"
  
  if PatientBillingInfo.where(id: patient_billing_infos_id).exists?
  
    puts patient_bill_info = PatientBillingInfo.where(id: patient_billing_infos_id )[0]
    paid_status = patient_bill_info.payment_status
    
    if paid_status == true
      doc_action_failed = {resp_code: '111', resp_desc: "This bill has already been paid for."}
      return doc_action_failed.to_json
    end
    
    puts approved_status = patient_bill_info.approval_status    
    
    if approved_status == true && action == "A" 
      doc_action_failed = {resp_code: '111', resp_desc: "This bill has already been accepted."}
      return doc_action_failed.to_json
    else
      
      if action == "A" #accept action
        puts accept_decline = patient_accept_decline_bill(patient_billing_infos_id,1)
        bill_action_success  =  { resp_code: '000',resp_desc: 'Your bill has been accepted Successfully. Kindly proceed to make payment.'} 
      
      else
        puts accept_decline = patient_accept_decline_bill(patient_billing_infos_id,0)
        bill_action_success  =  { resp_code: '000',resp_desc: 'Your bill has been rejected Successfully.'} 
      end
      
    end 
      
  end
  
  return bill_action_success.to_json
  
end


post '/nurse_doc_appointment_id' do
  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'
  
  puts
  puts req
  puts
  
  appt_id=req["appt_id"]
  
  
  send_accept_bill_message(appt_id)
end



post '/callback_resp' do
  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'
  
  puts
  puts req
  puts
  
  message=req["message"]
  trans_status=req["trans_status"]
  trans_id=req["trans_id"]
  trans_ref=req["trans_ref"]
  
  payment_callback(message, trans_status, trans_id, trans_ref)
end




# post '/make_payment' do
  
#   request.body.rewind
#   req = JSON.parse request.body.read

#   response['Access-Control-Allow-Origin'] = '*'


#   customer_name = req['customer_name']
#   customer_email = req['customer_email']
#   customer_telephone = req['customer_telephone']
#   mobile_wallet_number = req['mobile_wallet_number']
#   mobile_wallet_network = req['mobile_wallet_network']
#   amount = req['amount']
#   voucher = req['voucher']
#   billing_info_id = req["billing_info_id"]
#   appointment_title=req["appointment_title"]
  
#   #########VALIDATING THE PARAMS 
#   halt ERR_NAME.to_json unless req['customer_name']
#   halt ERR_EMAIL.to_json unless req['customer_email']
#   halt ERR_MOBILE_NUMBER.to_json unless req['customer_telephone']
#   halt ERR_MOMO_NUM.to_json unless req.key?('mobile_wallet_number')
#   halt ERR_MOMO_NET.to_json unless req['mobile_wallet_network']
#   halt ERR_AMOUNT.to_json unless req.key?('amount')
#   halt ERR_BILL.to_json unless req.key?('billing_info_id')
#   halt ERR_APPTITLE.to_json unless req.key?('appointment_title')
 
 

#    pay = payment(customer_name,customer_email,customer_telephone,mobile_wallet_number,mobile_wallet_network,amount,voucher,billing_info_id,appointment_title)
   
#   puts "Value: #{pay}"
  
#   message = ERR_PAYMENT_REQ
  
#   if mobile_wallet_network == "mtn" then
#     message = ERR_PAYMENT_MTN
#   end
  
#   return message.to_json

# end




#MOMO PAYMENT REQUEST
post '/make_payment' do
  
  request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'

  subscription_id = req['subscription_id']
  customer_number = req['customer_number']
  amount = req['amount']
  nw = req['nw']
  voucher_code = req['voucher_code']
  user_id = req['user_id']
  
  
  #########VALIDATING THE PARAMS 


  halt ERR_MOMO_NUM.to_json unless req.key?('customer_number')
  halt ERR_MOMO_NET.to_json unless req['nw']
  halt ERR_AMOUNT.to_json unless req.key?('amount')
  

   pay = mobileMoneyReq(subscription_id,customer_number, amount,nw,voucher_code,user_id)

   
  puts "Value: #{pay}"
  
  message = ERR_PAYMENT_REQ
  
  if nw == "MTN" then
    message = ERR_PAYMENT_MTN
  end
  
  return message.to_json

end





#CARD PAYMENT REQUEST
post '/make_payment_card' do
  
  request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'

  subscription_id = req['subscription_id']
  nickname = req['nickname']
  amount = req['amount']
  user_id = req['user_id']

  pay = visaReq(subscription_id,nickname, amount,user_id)

   
  puts "Value: #{pay.inspect}"

   return pay.to_json
end


 post '/retrieve_subscription_id' do
  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  user_id = req['user_id']
  

  retrieve_id = SubscriptionNewMain.retrieve_s_id(user_id)
  puts retrieve_id.to_json
  return retrieve_id.to_json

end



#  post '/payment_history' do
#   request.body.rewind
#   req = JSON.parse request.body.read
#   response['Access-Control-Allow-Origin'] = '*'

#   mobile_number = req['mobile_number']
  
#   puts "mobile number is #{mobile_number}"

#   payobject = PaymentTransaction.retrieve_payment_history(mobile_number)
#   p payobject.to_json
#   return payobject.to_json

# end

 post '/payment_history' do
  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  user_id = req['user_id']
  
  puts "user_id is #{user_id}"

  payobject = SubsPayment.retrieve_payment_history(user_id)
  p payobject.to_json
  return payobject.to_json

end






post '/book_subscription' do
  
  request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'

  user_id = req['user_id']
  f_name = req['f_name']
  last_name = req['last_name']
  phone_number = req['phone_number']
  email= req['email']
  gender= req['gender']
  marital_status= req['marital_status']
  religion = req['religion']
  address = req['address']
  emergency_name = req['emergency_name']
  emergency_phone = req['emergency_phone']
  emergency_email = req['emergency_email']
  emergency_address = req['emergency_address']
  sub_name = req['sub_name']
  amount = req['amount']
  user_name = req['user_name']
  user_phone = req['user_phone'] 
  dob = req['dob'] 
  f_name_second = req['f_name_second'] 
  l_name_second = req['l_name_second'] 
  phone_number_second = req['phone_number_second'] 
  address_second = req['address_second'] 
  dob_second = req['dob_second'] 
  gender_second = req['gender_second']  

 


   puts insert_subscription(user_id,f_name,last_name,phone_number,email,gender,marital_status,religion,address,emergency_name,emergency_phone,emergency_email,emergency_address,sub_name,amount,user_name,user_phone,dob,f_name_second,l_name_second,phone_number_second,address_second,dob_second,gender_second)

   regis_success  =  { resp_code: '000',resp_desc: 'Subscription is received and pending payments before a Doctor is assigned. Kindly complete payment to complete the process. Thank you.'}

  regis_success.to_json


end





 post '/subscription_history' do
  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  user_id = req['user_id']
  
  retrieve_sub_history = SubscriptionNewMain.retrieve_subscription_history(user_id)

  p retrieve_sub_history.to_json
  return retrieve_sub_history.to_json

end




 post '/subscription_history_details' do
  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  sub_id = req['sub_id']
  
  retrieve_sub_history = SubscriptionNewMain.subscription_history_details(sub_id)

  p retrieve_sub_history.to_json
  return retrieve_sub_history.to_json

end



 post '/subscription_history_for_doc' do
  request.body.rewind
  req = JSON.parse request.body.read
  response['Access-Control-Allow-Origin'] = '*'

  doc_id = req['doc_id']
  
  retrieve_sub_history = SubscriptionNewMain.retrieve_subscription_history_for_doc(doc_id)

  p retrieve_sub_history.to_json
  return retrieve_sub_history.to_json

end



post '/make_remarks' do
  
  request.body.rewind
  req = JSON.parse request.body.read

  response['Access-Control-Allow-Origin'] = '*'

  sub_id = req['sub_id']
  remark = req['remark']
 
   puts make_remarks(sub_id,remark)

   regis_success  =  { resp_code: '000',resp_desc: 'Remark has been made successfully!'}

  regis_success.to_json


end
