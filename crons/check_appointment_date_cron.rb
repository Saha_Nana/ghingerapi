require 'date'
#this will handle appointment date checking... Below are pseudocodes and algorithms....

def get_appt_twentyfourhour_date()
    
  time_now = DateTime.now
  puts time_now      # => 2011-05-06T11:42:26+03:00
    
  appt_calculated_date = time_now + 1 #today plus 1 day (24 hours)
  puts "24 hours = #{appt_calculated_date.to_date}"
  
  return appt_calculated_date.to_date  
end


#appointments due in 24 hours time.
def check_appointments_due()
  
  appt_set_date = ""
  puts appt_set_date = get_appt_twentyfourhour_date()
  
  #get all appointments due in 24 hours time but not attended to.
  
  puts appointment_details = ConfirmedAppointment.joins("INNER JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id INNER JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id INNER JOIN person_infos ON person_infos.id = confirmed_appointments.patient_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = person_infos.id LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat LEFT JOIN pre_confirmed_appointments ON pre_confirmed_appointments.pre_appointment_id = confirmed_appointments.pre_appointment_id")
          .select("confirmed_appointments.id, confirmed_appointments.pre_appointment_id, confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,confirmed_appointments.patient_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,person_contact_infos.email as patient_email,pre_appointments.medication,pre_appointments.duration,confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,true as accepted_status,confirmed_appointments.attended_to,pre_appointments.beneficiary_name,pre_appointments.beneficiary_age,pre_appointments.beneficiary_gender,pre_appointments.beneficiary_phone_number,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,request_categories.category,pre_appointments.allergies,pre_appointments.prev_medical_history,pre_appointments.proposed_date,pre_appointments.proposed_time,confirmed_appointments.paid,pre_confirmed_appointments.doctor_id, pre_confirmed_appointments.nurse_id")
          .where('confirmed_appointments.attended_to = false AND confirmed_appointments.confirmed_date::timestamp::date = ?',appt_set_date)
          .order("confirmed_appointments.id desc")
          
  puts appointment_details.to_json
          
  if !appointment_details.nil? then
    
    appointments = []
    
    !appointment_details.each do |appt|
            
      str_email = ""
      patient_name = ""
      patient_name = appt.surname + " " +appt.other_names
      patient_email = appt.patient_email
      patient_contact_number = appt.contact
      appointment_type = appt.appointment_type
      professional_name = ""
      doctor_id = appt["doctor_id"]
      nurse_id = appt["nurse_id"]
      beneficiary_gender = ""
      
      if !doctor_id.nil? then
        puts "doctor is not nil"
        p = PersonInfo.joins("LEFT JOIN person_contact_infos ON person_contact_infos.person_id = person_infos.id")
        .select("person_infos.surname,person_infos.other_names,person_infos.person_type_id,person_contact_infos.contact_number,person_contact_infos.email")
        .where("person_infos.id = ?","#{doctor_id}")[0]
        puts "p = #{p.to_json}"
        if !p.nil? then
          
          prof_email_address=p.email
          prof_contact_number=p.contact_number
          ########## Generate Date ##############
          time=Time.new
          strtm=time.strftime("%d-%m-%Y")
          #######################################
          puts prof_name= "Dr. " + p.surname.to_s + " " +p.other_names.to_s
          puts "professional_name = #{prof_name}"
          
        end
      end
      
      if !nurse_id.nil? then
        puts "nurse is not nil"
        p = PersonInfo.joins("LEFT JOIN person_contact_infos ON person_contact_infos.person_id = person_infos.id")
        .select("person_infos.surname,person_infos.other_names,person_infos.person_type_id,person_contact_infos.contact_number,person_contact_infos.email")
        .where("person_infos.id = ?","#{nurse_id}")[0]
        puts "p = #{p.to_json}"
        if !p.nil? then
          
          prof_email=p.email
          prof_contact_number=p.contact_number
          ########## Generate Date ##############
          time=Time.new
          strtm=time.strftime("%d-%m-%Y")
          #######################################
          puts prof_name= "Nurse " + p["surname"].to_s + " " +p["other_names"].to_s
          puts "professional_name = #{prof_name} AND professional email = #{prof_email}"
          
        end
        
      end 


      if !appt.beneficiary_gender.nil? then
        if appt.beneficiary_gender == "F" then
          beneficiary_gender = "Female"
        else 
          beneficiary_gender = "Male"
        end
      end
      
      appointment_type = appt.appointment_type
      confirmed_date = appt.confirmed_date.strftime("%B %d, %Y %I:%M%p")
      category = appt.category
      beneficiary_name = appt.beneficiary_name
      beneficiary_age = appt.beneficiary_age
      beneficiary_phone_number = appt.beneficiary_phone_number
      urgency = appt.urgency
      allergies = appt.allergies
      prev_medical_history = appt.prev_medical_history
      
      if !appt.paid.nil? then paid_status = appt.paid else paid_status = "" end
      
        send_notifications(prof_email,prof_contact_number,prof_name,patient_name,patient_email,patient_contact_number,appointment_type,confirmed_date,category,beneficiary_name,beneficiary_age,beneficiary_gender, beneficiary_phone_number,urgency,allergies,prev_medical_history,paid_status)

      end
    
  end
         
  #return appointment_details.to_json  
end



def send_notifications(prof_email,prof_contact_number,prof_name,patient_name,patient_email,patient_contact_number,appointment_type,confirmed_date,category,beneficiary_name,beneficiary_age,beneficiary_gender, beneficiary_phone_number,urgency,allergies,prev_medical_history,paid_status)
  
  applicant_name = ""
  appointment_sms = ""
  
  puts "send_notifications - patient_email = #{patient_email}"
  puts "send_notifications - prof_email = #{prof_email}"
  
  if !patient_email.nil? then
   #to patient.
    notification_to_patient(patient_name,patient_email,patient_contact_number,prof_name,appointment_type,confirmed_date,category,beneficiary_name,beneficiary_age,beneficiary_gender,beneficiary_phone_number,urgency,allergies,prev_medical_history,paid_status)
  end
 
  if !prof_email.nil? then
    #to professional.
    notification_to_prof(prof_email,prof_contact_number,prof_name,patient_name,appointment_type,confirmed_date,category,beneficiary_name,beneficiary_age,beneficiary_gender,beneficiary_phone_number,urgency,allergies,prev_medical_history,paid_status)
  end
      
end



def notification_to_prof(prof_email,prof_contact_number,prof_name,patient_name,appointment_type,confirmed_date,category,beneficiary_name,beneficiary_age,beneficiary_gender,beneficiary_phone_number,urgency,allergies,prev_medical_history,paid_status)
  
  if !prof_email.nil? && paid_status == true then
     
      str_email= "Dear #{prof_name}, \n"
      str_email << "You are reminded of your pending #{appointment_type} appointment with #{patient_name} which is set for tomorrow #{confirmed_date}. Find below details of the appointment: \n\n\n"
      
      if !appointment_type.nil? then str_email << "Appointment Type: #{appointment_type}\n" end
      if !confirmed_date.nil? then str_email << "Confirmed Appointment Date: #{confirmed_date}\n" end
      if !category.nil? then str_email << "Appointment Category: #{category}\n" end
      if !beneficiary_name.nil? then str_email << "Beneficiary's Name: #{beneficiary_name}\n" end
      if !beneficiary_age.nil? then str_email << "Beneficiary's Age: #{beneficiary_age}\n" end
      if !beneficiary_gender.nil? then str_email << "Beneficiary's Gender: #{beneficiary_gender}\n" end
      if !beneficiary_phone_number.nil? then str_email << "Beneficiary's Phone Number: #{beneficiary_phone_number}\n" end
      if !urgency.nil? then str_email << "Appointment's Urgency: #{urgency}\n" end
      if !allergies.nil? then str_email << "Allergies: #{allergies}\n" end
      if !prev_medical_history.nil? then str_email << "Previous Medical History: #{prev_medical_history}\n" end
      
      str_email << "\n Kindly treat this mail as urgent and attend to the appointment before end of day tomorrow (#{confirmed_date}).\n\n"
      str_email << "Regards,\n#{Email_sub}\n"
      
      puts "prof_email = #{prof_email} AND str_email = #{str_email}"
      ############## Send email to patient #################
      puts send_email(prof_email, str_email, "#{appointment_type} A
      ppointment Reminder with patient: #{patient_name}")
 
   end
   
   if !prof_contact_number.nil? && paid_status == true then
     
      appointment_sms= "Dear #{prof_name}, you are reminded of your pending #{appointment_type} appointment with #{patient_name} on #{confirmed_date}.\n"
      appointment_sms << "Regards,\n#{Email_sub}\n"
      
      puts "#{appointment_sms.to_s} AND prof_contact_number = #{prof_contact_number}"
      
      #puts sendmsg(APP_NAME, prof_contact_number, appointment_sms.to_s, msg_type='T',src="API")#send sms to user
   end
   
end


def notification_to_patient(patient_name,patient_email,patient_contact_number,prof_name,appointment_type,confirmed_date,category,beneficiary_name,beneficiary_age,beneficiary_gender,beneficiary_phone_number,urgency,allergies,prev_medical_history,paid_status)
  puts "notification_to_patient - patient_email = #{patient_email}"
  
  if !patient_email.nil? && paid_status != true then
    
    str_email= "Dear #{patient_name}, \n"
    str_email << "You are reminded to make payment for your incoming #{appointment_type} appointment bill before your appointment date on #{confirmed_date}. Find below details of the appointment: \n\n\n"
    
    if !appointment_type.nil? then str_email << "Appointment Type: #{appointment_type}\n" end
    if !confirmed_date.nil? then str_email << "Confirmed Appointment Date: #{confirmed_date}\n" end
    if !category.nil? then str_email << "Appointment Category: #{category}\n" end
    if !beneficiary_name.nil? then str_email << "Beneficiary's Name: #{beneficiary_name}\n" end
    if !beneficiary_age.nil? then str_email << "Beneficiary's Age: #{beneficiary_age}\n" end
    if !beneficiary_gender.nil? then str_email << "Beneficiary's Gender: #{beneficiary_gender}\n" end
    if !beneficiary_phone_number.nil? then str_email << "Beneficiary's Phone Number: #{beneficiary_phone_number}\n" end
    if !urgency.nil? then str_email << "Appointment's Urgency: #{urgency}\n" end
    if !allergies.nil? then str_email << "Allergies: #{allergies}\n" end
    if !prev_medical_history.nil? then str_email << "Previous Medical History: #{prev_medical_history}\n\n" end
      
    str_email << "Kindly contact customer care in case you have issues with your appointment.\n\n"
    str_email << "Regards,\n#{Email_sub}\n"
    
    puts send_email(patient_email, str_email, "#{appointment_type} Appointment Bill Payment Reminder")
    
  end
  
  #remind patient via SMS if he has not made payment 
  if !patient_contact_number.nil? && paid_status != true then
     
    appointment_sms= "You are reminded to make payment for your incoming #{appointment_type} appointment bill before your appointment date on #{confirmed_date}.\nKindly contact customer care in case you have issues with your appointment.\n"
    appointment_sms << "Regards,\n#{Email_sub}\n"
    
    puts "#{appointment_sms.to_s} AND patient_contact_number = #{patient_contact_number}"
    
    puts sendmsg(APP_NAME, patient_contact_number, appointment_sms.to_s, msg_type='T',src="API")#send sms to user
  end
   
  
  if !patient_email.nil? && !prof_name.nil? && paid_status == true then
     
      str_email= "Dear #{patient_name}, \n"
      str_email << "You are reminded of your incoming #{appointment_type} appointment with #{prof_name}. Find below details of the appointment: \n\n\n"
      
      if !appointment_type.nil? then str_email << "Appointment Type: #{appointment_type}\n" end
      if !confirmed_date.nil? then str_email << "Confirmed Appointment Date: #{confirmed_date}\n" end
      if !category.nil? then str_email << "Appointment Category: #{category}\n" end
      if !beneficiary_name.nil? then str_email << "Beneficiary's Name: #{beneficiary_name}\n" end
      if !beneficiary_age.nil? then str_email << "Beneficiary's Age: #{beneficiary_age}\n" end
      if !beneficiary_gender.nil? then str_email << "Beneficiary's Gender: #{beneficiary_gender}\n" end
      if !beneficiary_phone_number.nil? then str_email << "Beneficiary's Phone Number: #{beneficiary_phone_number}\n" end
      if !urgency.nil? then str_email << "Appointment's Urgency: #{urgency}\n" end
      if !allergies.nil? then str_email << "Allergies: #{allergies}\n" end
      if !prev_medical_history.nil? then str_email << "Previous Medical History: #{prev_medical_history}\n\n" end
      str_email << "Regards,\n#{Email_sub}\n"
      
      ############## Send email to patient #################
      puts send_email(patient_email, str_email, "#{appointment_type} Appointment Reminder")
            
   end
   
   if !patient_contact_number.nil? && paid_status == true then
     
      appointment_sms= "Dear #{patient_name}, you are reminded of your pending #{appointment_type} appointment with #{prof_name} on #{confirmed_date}."
      appointment_sms << " Regards,\n#{Email_sub}\n"
      
      puts "#{appointment_sms.to_s} AND patient_contact_number = #{patient_contact_number}"
      
      puts sendmsg(APP_NAME, patient_contact_number, appointment_sms.to_s, msg_type='T',src="API")#send sms to user
   end
   
end




