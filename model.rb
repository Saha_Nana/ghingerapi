class SubscriptionNewMain < ActiveRecord::Base
   self.table_name ="subscription_new_mains"

       def self.retrieve_s_id(user_id)

    select("subscription_new_mains.id,subscription_new_mains.f_name,subscription_new_mains.user_id").where(
    "subscription_new_mains.user_id = ?","#{user_id}").order('subscription_new_mains.id desc')[0]

  end

  def self.retrieve_subscription_history(user_id)

    select("subscription_new_mains.id,subscription_new_mains.f_name,subscription_new_mains.user_id,subscription_new_mains.last_name, subscription_new_mains.phone_number,subscription_new_mains.email,subscription_new_mains.status,subscription_new_mains.amount,subscription_new_mains.paid_status,subscription_new_mains.created_at,subscription_new_mains.sub_name, subscription_new_mains.gender").where(
    "subscription_new_mains.user_id = ?","#{user_id}" ).order('subscription_new_mains.id desc')

  end







    def self.subscription_history_details(sub_id)

    # select("subscription_new_mains.id,subscription_new_mains.f_name,subscription_new_mains.user_id,subscription_new_mains.last_name, subscription_new_mains.phone_number,subscription_new_mains.email,subscription_new_mains.status,subscription_new_mains.amount,subscription_new_mains.paid_status,subscription_new_mains.created_at,subscription_new_mains.sub_name, subscription_new_mains.gender, subscription_new_mains.marital_status,subscription_new_mains.religion,subscription_new_mains.address,subscription_new_mains.emergency_name,subscription_new_mains.emergency_phone,subscription_new_mains.emergency_address,subscription_new_mains.emergency_email,subscription_new_mains.remark").where(
    # "subscription_new_mains.id = ?","#{sub_id}" ).order('subscription_new_mains.id desc')

     joins("LEFT JOIN subscription_remarks ON subscription_new_mains.id = subscription_remarks.sub_id").select("subscription_new_mains.id,subscription_new_mains.f_name,subscription_new_mains.user_id,subscription_new_mains.last_name, subscription_new_mains.phone_number,subscription_new_mains.email,subscription_new_mains.status,subscription_new_mains.amount,subscription_new_mains.paid_status,subscription_new_mains.created_at,subscription_new_mains.sub_name, subscription_new_mains.gender, subscription_new_mains.marital_status,subscription_new_mains.religion,subscription_new_mains.address,subscription_new_mains.emergency_name,subscription_new_mains.emergency_phone,subscription_new_mains.emergency_address,subscription_new_mains.emergency_email,subscription_remarks.remark").where(
    "subscription_new_mains.id = ?","#{sub_id}" ).order('subscription_new_mains.id desc')

  end




    def self.retrieve_subscription_history_for_doc(doc_id)

    select("subscription_new_mains.id,subscription_new_mains.f_name,subscription_new_mains.user_id,subscription_new_mains.last_name, subscription_new_mains.phone_number,subscription_new_mains.email,subscription_new_mains.status,subscription_new_mains.amount,subscription_new_mains.paid_status,subscription_new_mains.created_at,subscription_new_mains.sub_name, subscription_new_mains.gender, subscription_new_mains.marital_status,subscription_new_mains.religion,subscription_new_mains.address,subscription_new_mains.emergency_name,subscription_new_mains.emergency_phone,subscription_new_mains.emergency_address,subscription_new_mains.emergency_email,subscription_new_mains.remark").where(
    "subscription_new_mains.doc_id = ?","#{doc_id}" ).order('subscription_new_mains.id desc')


    #   joins("LEFT JOIN subscription_remarks ON subscription_new_mains.id = subscription_remarks.sub_id").select("subscription_new_mains.id,subscription_new_mains.f_name,subscription_new_mains.user_id,subscription_new_mains.last_name, subscription_new_mains.phone_number,subscription_new_mains.email,subscription_new_mains.status,subscription_new_mains.amount,subscription_new_mains.paid_status,subscription_new_mains.created_at,subscription_new_mains.sub_name, subscription_new_mains.gender, subscription_new_mains.marital_status,subscription_new_mains.religion,subscription_new_mains.address,subscription_new_mains.emergency_name,subscription_new_mains.emergency_phone,subscription_new_mains.emergency_address,subscription_new_mains.emergency_email,subscription_remarks.remark").where(
    # "subscription_new_mains.doc_id = ?","#{doc_id}" ).order('subscription_new_mains.id desc')



    

  end

end


class SubsPayment < ActiveRecord::Base
  self.table_name ="subs_payments"

   def self.retrieve_payment_history(user_id)
    select("subs_payments.id,subs_payments.network,subs_payments.amount,subs_payments.transaction_id,subs_payments.created_at,subs_payments.trans_status,subs_payments.customer_number").where(
    "subs_payments.user_id = ?","#{user_id}" ).order('subs_payments.id desc')

  end
end


class PaymentState < ActiveRecord::Base
  self.table_name ="payment_states"
end

class SubscriptionRemark < ActiveRecord::Base
  self.table_name ="subscription_remarks"
end

class PersonInfo <ActiveRecord::Base

  self.table_name = 'person_infos'

    def self.retrieve_doctor_regis(id)

    select("person_infos.id,person_infos.surname,person_infos.other_names,person_infos.person_type_id").where(
    "person_infos.id = ?","#{id}" ).order('person_infos.id desc')

  end
end

class Role <ActiveRecord::Base
  self.table_name = 'roles'
end


class Feedback <ActiveRecord::Base
  self.table_name = 'feedbacks'
end

class ServiceFeeMaster <ActiveRecord::Base
  self.table_name = 'service_fee_masters'
end




class AppointmentType <ActiveRecord::Base
  self.table_name = 'appointment_types'
  
  def self.get_appointment_types()
    select("appointment_types.id,appointment_types.ref_id,appointment_types.title").where("appointment_types.active_status = 't'").order('appointment_types.id asc')
  end
  
  #this will get appointment types excluding the Personal Doctor Service
  def self.get_appointment_types1()
    select("appointment_types.id,appointment_types.ref_id,appointment_types.title").where("appointment_types.active_status = 't' AND appointment_types.id <> 1").order('appointment_types.id asc')
  end
  
end

class PersonContactInfo <ActiveRecord::Base
  self.table_name = 'person_contact_infos'
end
class PersonTypeMaster <ActiveRecord::Base
  self.table_name = 'person_type_masters'
end

class PasswordTable <ActiveRecord::Base
  self.table_name = "password_table"

end

class User <ActiveRecord::Base
  self.table_name = 'users'
  
  def self.retrieve_user_status(email)
    
    select("users.id,users.email, users.active_status").where("users.email = ?","#{email}").order("users.id desc")
    
  end
  
end

class Registration <ActiveRecord::Base
  self.table_name = "registration"
  
  def self.checkprof_info(email)
    
    select("registration.surburb_id,registration.hospital_name, registration.has_specialty,registration.specialty_id,registration.licence_number,registration.professional_group_id").where(
    "registration.email = ?","#{email}" ).order('registration.id desc')
    
      # check = Registration.where(id: id).update(surburb_id: surburb_id,hospital_name: hospital_name,has_specialty: has_specialty,specialty_id:specialty_name,specialty_duration:specialty_duration,licence_number:license_number,professional_group_id:medical_regulator,foreign_training:foreign_training,foreign_institution: foreign_medical_regulator,foreign_licence_number: foreign_license_number )

  end
  
  def self.retrieve_regis(email)

   joins("LEFT JOIN person_infos ON registration.id = person_infos.reg_id").select("person_infos.id,person_infos.person_type_id,reg_id,registration.other_names, registration.surname,registration.mobile_number,registration.password,registration.telco,registration.email,registration.email,registration.user_type,registration.specialty_id, registration.specialty_duration,registration.has_specialty,registration.foreign_training,registration.foreign_institution,registration.professional_group_id,registration.licence_number,registration.foreign_licence_number,registration.created_at").where( "registration.email = ?","#{email}").order('registration.created_at desc')

  end
  
  
  
  def self.retrieve_edit(reg_id)

   joins("LEFT JOIN person_infos ON registration.id = person_infos.reg_id").select("person_infos.id,person_infos.person_type_id,reg_id,registration.other_names, registration.surname,registration.mobile_number,registration.password,registration.telco,registration.email,registration.user_type,registration.specialty_id, registration.specialty_duration,registration.has_specialty,registration.foreign_training,registration.foreign_institution,registration.professional_group_id,registration.licence_number,registration.foreign_licence_number,registration.created_at").where( "registration.id = ?","#{reg_id}").order('registration.created_at desc')

  end

   def self.retrieve_regis1(email)

 select("registration.id,registration.other_names, registration.surname,registration.mobile_number,registration.password,registration.telco,registration.email,registration.user_type,registration.created_at").where(
    "registration.email = ?","#{email}" ).order('registration.id desc')

  end


     def self.retrieve_alt(id)
       
      select("registration.id,registration.other_names, registration.surname,registration.mobile_number,registration.password,registration.telco,registration.email,registration.user_type,registration.created_at").where(
      "registration.id = ?","#{id}" ).order('registration.id desc')

    end


  def self.retrieve_confirm(email)

     joins("LEFT JOIN person_infos ON registration.id = person_infos.reg_id LEFT JOIN  confirmed_personal_doctor_services ON confirmed_personal_doctor_services.patient_id = person_infos.id").select("person_infos.id,person_infos.surname,person_infos.other_names,person_infos.person_type_id,confirmed_personal_doctor_services.doctor_id,confirmed_personal_doctor_services.patient_id").where( "registration.email = ?","#{email}").order('registration.created_at desc')

  end


    def self.retrieve_doctor_regis(id)

     joins("LEFT JOIN person_infos ON registration.id = person_infos.reg_id").select("person_infos.id,person_infos.person_type_id,reg_id,registration.other_names, registration.surname,registration.mobile_number,registration.password,registration.telco,registration.email,registration.user_type,registration.created_at").where( "registration.id = ?","#{id}").order('registration.created_at desc')

  end
  
  def self.check_doc_professional_info(doc_id)
    
      reg = Registration.find(doc_id)
     # Registration.find_by_sql["select * from registration where id=:id and licence_number IS NOT NULL",{:id => doc_id}]
     
     
     if reg.specialty_id.nil? or reg.specialty_duration.nil? or professional_group_id.nil? or licence_number.nil?
       puts "#############################"
       puts "the specialty field is empty"
       puts "#############################"
       # prevent the person
       
     else
       puts "#############################"
       puts "the specialty field is not empty"
       puts "#############################"
       # allow the person
     end
    
  end

end

class ProviderMaster <ActiveRecord::Base
  self.table_name = "provider_masters"
  
  def self.get_service_providers()
    select("provider_masters.id,provider_masters.provider_name").where("provider_masters.active_status = 't'").order('provider_masters.id desc')
  end

end

class CountryMaster <ActiveRecord::Base
  self.table_name = "country_masters"
  
  def self.get_countries()
    select("country_masters.id,country_masters.country_name").where("country_masters.active_status = 't'").order('country_masters.country_name asc')
  end

end

class CityTownMaster <ActiveRecord::Base
  self.table_name = "city_town_masters"
  
  # def self.retrieve_cities(region_id)  
    # select("city_town_masters.id,city_town_masters.city_town_name").where("city_town_masters.active_status = 't' AND city_town_masters.region_id = ?",region_id).order('city_town_masters.city_town_name asc')
  # end
  def self.retrieve_cities()  
    select("city_town_masters.id,city_town_masters.city_town_name").where("city_town_masters.id IN (1,4)").order('city_town_masters.city_town_name asc')
  end
end
#"select * from info WHERE `id` IN ('1,2,3,4,5')"
class RegionMaster <ActiveRecord::Base
  self.table_name = "region_masters"
  def self.retrieve_location(location)
    location = "%#{location}%"

    joins("LEFT JOIN city_town_masters ON region_masters.id = city_town_masters.region_id LEFT JOIN suburb_masters ON city_town_masters.id = suburb_masters.city_town_id").select("region_masters.id,city_town_masters.region_id,city_town_masters.id,suburb_masters.city_town_id,suburb_name, city_town_name,
          region_state_name").where("LOWER(suburb_masters.suburb_name) LIKE  ?",location.to_lower)
  end
  
  
  def self.retrieve_region(country_id)
    
    select("region_masters.id,region_masters.region_state_name").where("region_masters.active_status = 't' AND region_masters.country_id = ?",country_id).order('region_masters.region_state_name asc')
  end
  
  

end

class SuburbMaster <ActiveRecord::Base
  self.table_name = "suburb_masters"
  
  def self.retrieve_suburbs(city_id)  
    select("suburb_masters.id,suburb_masters.suburb_name").where("suburb_masters.active_status = 't' AND suburb_masters.city_town_id = ?",city_id).order('suburb_masters.suburb_name asc')
  end
  
  
  def self.retrieve_suburbs_type_along(city_id,location)
    
    halt INVALID_PRAMS.to_json unless city_id
      
    location = "%#{location}%"
    select("suburb_masters.id,suburb_masters.suburb_name").where("suburb_masters.active_status = 't' AND suburb_masters.city_town_id = ? and suburb_masters.suburb_name ilike ?",city_id,location.downcase).order('suburb_masters.suburb_name asc')
  end
  
  
  def self.get_suburb_id(suburb_name)
    select("suburb_masters.id,suburb_masters.suburb_name").where("suburb_masters.suburb_name = ?",suburb_name)
  end


end

class ProviderMaster <ActiveRecord::Base
  self.table_name = "provider_masters"
  def self.retrieve_providers(location)
    location = "%#{location}%"

    joins("LEFT JOIN service_prov_extra_infos ON provider_masters.id = service_prov_extra_infos.service_prov_id LEFT JOIN suburb_masters ON service_prov_extra_infos.suburb_id = suburb_masters.id").select("provider_image,provider_name,service_prov_id,provider_masters.id,suburb_id,suburb_name").where(

    "LOWER(suburb_masters.suburb_name) LIKE  ?",location.downcase

    )
  end

end

class ServiceProvExtraInfo <ActiveRecord::Base
  self.table_name = "service_prov_extra_infos"

end

class ServiceMaster <ActiveRecord::Base
  self.table_name = "service_masters"

end

class SpecialtyMaster <ActiveRecord::Base
  self.table_name = "specialty_masters"
  def self.get_specialties()

    select("specialty_masters.id,specialty_masters.title").where("specialty_masters.active_status = 't'").order('specialty_masters.id desc')
  end

end

class ProfesionalGroups <ActiveRecord::Base
  self.table_name = "profesional_groups"
  
  def self.get_profesional_groups()
    select("profesional_groups.id,profesional_groups.group_name").where("profesional_groups.active_status = 't'").order('profesional_groups.id desc')
  end

end

class ProviderService <ActiveRecord::Base
  self.table_name = "provider_services"
  def self.search_services(provider_name)
    provider_name = "%#{provider_name}%"

    joins("LEFT JOIN service_masters ON provider_services.service_id = service_masters.id LEFT JOIN provider_masters ON provider_services.service_provider_id = provider_masters.id").select("provider_name,title,service_provider_id service_id").where(
    "provider_masters.provider_name LIKE  ?",provider_name

    )
  end

end

class PreAppointment <ActiveRecord::Base
  self.table_name = "pre_appointments"

  def self.appointment_history(requester_id)
    PreAppointment.joins("LEFT JOIN confirmed_appointments ON confirmed_appointments.pre_appointment_id = pre_appointments.id")
      .select("pre_appointments.id,pre_appointments.medication,pre_appointments.complaint_desc,pre_appointments.requester_id,pre_appointments.proposed_date,pre_appointments.allergies,pre_appointments.prev_medical_history,pre_appointments.confirm_status,pre_appointments.created_at,confirmed_appointments.attended_to")
      .where("pre_appointments.requester_id = ?","#{requester_id}" ).order('pre_appointments.confirm_status desc')
  end

  def self.medical_appointment_history(requester_id)

      

    PreAppointment.joins("LEFT JOIN suburb_masters ON suburb_masters.id = pre_appointments.suburb_id LEFT JOIN provider_masters ON provider_masters.id = pre_appointments.provider_id LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency LEFT JOIN confirmed_appointments ON confirmed_appointments.pre_appointment_id = pre_appointments.id LEFT JOIN patient_billing_infos ON patient_billing_infos.appointment_id = confirmed_appointments.id LEFT JOIN billing_info_bill_items ON billing_info_bill_items.billing_info_id = patient_billing_infos.id LEFT JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id")
      .select("DISTINCT ON (pre_appointments.id) pre_appointments.id,pre_appointments.medication,pre_appointments.complaint_desc,pre_appointments.requester_id,pre_appointments.proposed_date,pre_appointments.proposed_time,pre_appointments.allergies,pre_appointments.prev_medical_history,suburb_masters.suburb_name,provider_masters.provider_name,request_categories.category,pre_appointments.beneficiary_name,pre_appointments.beneficiary_age,pre_appointments.beneficiary_phone_number,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,billing_info_bill_items.billing_info_id AS billing_info_bill_items_info_id,confirmed_appointments.paid,appointment_types.title AS appointment_type_title,pre_appointments.created_at,confirmed_appointments.attended_to,confirmed_appointments.confirmed_date")
      .where("pre_appointments.requester_id = ? and pre_appointments.appointment_type_id = 'MA'","#{requester_id}" )
      .order('pre_appointments.id desc,pre_appointments.created_at desc')
      
  end


  def self.med_appointment_detail(appointment_id)

     PreAppointment.joins("LEFT JOIN suburb_masters ON suburb_masters.id = pre_appointments.suburb_id LEFT JOIN provider_masters ON provider_masters.id = pre_appointments.provider_id LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency LEFT JOIN confirmed_appointments ON confirmed_appointments.pre_appointment_id = pre_appointments.id")
      .select("pre_appointments.id,pre_appointments.medication,pre_appointments.complaint_desc,pre_appointments.requester_id,pre_appointments.proposed_date,pre_appointments.proposed_time,pre_appointments.allergies,pre_appointments.prev_medical_history,pre_appointments.suburb_id,suburb_masters.suburb_name,provider_masters.provider_name,request_categories.category,pre_appointments.beneficiary_name,pre_appointments.beneficiary_phone_number,pre_appointments.beneficiary_age,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,pre_appointments.created_at,confirmed_appointments.attended_to")
      .where("pre_appointments.id = ? and pre_appointments.appointment_type_id = 'MA'","#{appointment_id}" )
      .order('pre_appointments.confirm_status desc')
      # group('person_info.id,contacts.id,contact_groups.groups').
      # all.to_json

  end


  def self.video_consult_appointment_history(requester_id,appointment_type_id)
      
    # PreAppointment.joins("LEFT JOIN suburb_masters ON suburb_masters.id = pre_appointments.suburb_id LEFT JOIN provider_masters ON provider_masters.id = pre_appointments.provider_id LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency LEFT JOIN confirmed_appointments ON confirmed_appointments.pre_appointment_id = pre_appointments.id")
      # .select("pre_appointments.id,pre_appointments.medication,pre_appointments.complaint_desc,pre_appointments.requester_id,pre_appointments.proposed_date,pre_appointments.proposed_time,pre_appointments.allergies,pre_appointments.prev_medical_history,suburb_masters.suburb_name,provider_masters.provider_name,request_categories.category,pre_appointments.beneficiary_name,pre_appointments.beneficiary_age,pre_appointments.beneficiary_gender,pre_appointments.beneficiary_phone_number,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,pre_appointments.created_at,confirmed_appointments.attended_to,confirmed_appointments.id as confirmed_appointments_id")
      # .where("pre_appointments.requester_id = ? and pre_appointments.appointment_type_id = ?","#{requester_id}","#{appointment_type_id}" )
      # .order('pre_appointments.created_at desc')
      
    PreAppointment.joins("LEFT JOIN suburb_masters ON suburb_masters.id = pre_appointments.suburb_id LEFT JOIN provider_masters ON provider_masters.id = pre_appointments.provider_id LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency LEFT JOIN confirmed_appointments ON confirmed_appointments.pre_appointment_id = pre_appointments.id LEFT JOIN patient_billing_infos ON patient_billing_infos.appointment_id = confirmed_appointments.id LEFT JOIN billing_info_bill_items ON billing_info_bill_items.billing_info_id = patient_billing_infos.id LEFT JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id")
      .select("DISTINCT ON (pre_appointments.id) pre_appointments.id,pre_appointments.medication,pre_appointments.complaint_desc,pre_appointments.requester_id,pre_appointments.proposed_date,pre_appointments.proposed_time,pre_appointments.allergies,pre_appointments.prev_medical_history,suburb_masters.suburb_name,provider_masters.provider_name,request_categories.category,pre_appointments.beneficiary_name,pre_appointments.beneficiary_age,pre_appointments.beneficiary_phone_number,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,billing_info_bill_items.billing_info_id AS billing_info_bill_items_info_id,confirmed_appointments.paid,appointment_types.title AS appointment_type_title,pre_appointments.created_at,confirmed_appointments.attended_to,confirmed_appointments.confirmed_date")
      .where("pre_appointments.requester_id = ? and pre_appointments.appointment_type_id = ?","#{requester_id}","#{appointment_type_id}")
      .order('pre_appointments.id desc,pre_appointments.created_at desc')

  end

  def self.phone_consult_appointment_history(requester_id,appointment_type_id)
    
    
    PreAppointment.joins("LEFT JOIN suburb_masters ON suburb_masters.id = pre_appointments.suburb_id LEFT JOIN provider_masters ON provider_masters.id = pre_appointments.provider_id LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency LEFT JOIN confirmed_appointments ON confirmed_appointments.pre_appointment_id = pre_appointments.id LEFT JOIN patient_billing_infos ON patient_billing_infos.appointment_id = confirmed_appointments.id LEFT JOIN billing_info_bill_items ON billing_info_bill_items.billing_info_id = patient_billing_infos.id LEFT JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id")
      .select("DISTINCT ON (pre_appointments.id) pre_appointments.id,pre_appointments.medication,pre_appointments.complaint_desc,pre_appointments.requester_id,pre_appointments.proposed_date,pre_appointments.proposed_time,pre_appointments.allergies,pre_appointments.prev_medical_history,suburb_masters.suburb_name,provider_masters.provider_name,request_categories.category,pre_appointments.beneficiary_name,pre_appointments.beneficiary_age,pre_appointments.beneficiary_phone_number,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,billing_info_bill_items.billing_info_id AS billing_info_bill_items_info_id,confirmed_appointments.paid,appointment_types.title AS appointment_type_title,pre_appointments.created_at,confirmed_appointments.attended_to,confirmed_appointments.confirmed_date,patient_billing_infos.approval_status")
      .where("pre_appointments.requester_id = ? and pre_appointments.appointment_type_id = ?","#{requester_id}","#{appointment_type_id}")
      .order('pre_appointments.id desc,pre_appointments.created_at desc')

  end

  def self.phone_consult_appointment_history_detail(appointment_id,appointment_type_id)

    PreAppointment.joins("LEFT JOIN suburb_masters ON suburb_masters.id = pre_appointments.suburb_id LEFT JOIN provider_masters ON provider_masters.id = pre_appointments.provider_id LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency LEFT JOIN confirmed_appointments ON confirmed_appointments.pre_appointment_id = pre_appointments.id")
      .select("pre_appointments.id,pre_appointments.medication,pre_appointments.complaint_desc,pre_appointments.requester_id,pre_appointments.proposed_date,pre_appointments.proposed_time,pre_appointments.allergies,pre_appointments.prev_medical_history,suburb_masters.suburb_name,provider_masters.provider_name,request_categories.category,pre_appointments.beneficiary_name,pre_appointments.beneficiary_age,pre_appointments.beneficiary_phone_number,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,pre_appointments.created_at,confirmed_appointments.attended_to,confirmed_appointments.id as confirmed_appointments_id")
      .where("pre_appointments.id = ? and pre_appointments.appointment_type_id = ?","#{appointment_id}","#{appointment_type_id}" )
      .order('pre_appointments.confirm_status desc')
      
      # PreAppointment.joins("LEFT JOIN suburb_masters ON suburb_masters.id = pre_appointments.suburb_id LEFT JOIN provider_masters ON provider_masters.id = pre_appointments.provider_id LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency LEFT JOIN confirmed_appointments ON confirmed_appointments.pre_appointment_id = pre_appointments.id")
      # .select("pre_appointments.id,pre_appointments.medication,pre_appointments.complaint_desc,pre_appointments.requester_id,pre_appointments.proposed_date,pre_appointments.proposed_time,pre_appointments.allergies,pre_appointments.prev_medical_history,suburb_masters.suburb_name,provider_masters.provider_name,request_categories.category,pre_appointments.beneficiary_name,pre_appointments.beneficiary_gender,pre_appointments.beneficiary_age,pre_appointments.beneficiary_phone_number,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,pre_appointments.created_at,confirmed_appointments.attended_to, pre_appointments.test_list,confirmed_appointments.confirmed_date,confirmed_appointments.id as confirmed_appointments_id")
      # .where("pre_appointments.id = ? and pre_appointments.appointment_type_id = ?","#{appointment_id}","#{appointment_type_id}" )
      # .order('pre_appointments.created_at desc')
      
    # group('person_info.id,contacts.id,contact_groups.groups').
    # all.to_json

  end


  def self.video_consult_appointment_history_detail(appointment_id,appointment_type_id)

    PreAppointment.joins("LEFT JOIN suburb_masters ON suburb_masters.id = pre_appointments.suburb_id LEFT JOIN provider_masters ON provider_masters.id = pre_appointments.provider_id LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency LEFT JOIN confirmed_appointments ON confirmed_appointments.pre_appointment_id = pre_appointments.id")
      .select("pre_appointments.id,pre_appointments.medication,pre_appointments.complaint_desc,pre_appointments.requester_id,pre_appointments.proposed_date,pre_appointments.proposed_time,pre_appointments.allergies,pre_appointments.prev_medical_history,suburb_masters.suburb_name,provider_masters.provider_name,request_categories.category,pre_appointments.beneficiary_name,pre_appointments.beneficiary_age,pre_appointments.beneficiary_phone_number,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,pre_appointments.created_at,confirmed_appointments.attended_to,confirmed_appointments.id as confirmed_appointments_id")
      .where("pre_appointments.id = ? and pre_appointments.appointment_type_id = ?","#{appointment_id}","#{appointment_type_id}" )
      .order('pre_appointments.confirm_status desc')
    # group('person_info.id,contacts.id,contact_groups.groups').
    # all.to_json

  end

  def self.homecare_appointment_history(requester_id,appointment_type_id)

  

    
    PreAppointment.joins("LEFT JOIN suburb_masters ON suburb_masters.id = pre_appointments.suburb_id LEFT JOIN provider_masters ON provider_masters.id = pre_appointments.provider_id LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency LEFT JOIN confirmed_appointments ON confirmed_appointments.pre_appointment_id = pre_appointments.id LEFT JOIN patient_billing_infos ON patient_billing_infos.appointment_id = confirmed_appointments.id LEFT JOIN billing_info_bill_items ON billing_info_bill_items.billing_info_id = patient_billing_infos.id LEFT JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id")
      .select("DISTINCT ON (pre_appointments.id) pre_appointments.id,pre_appointments.medication,pre_appointments.complaint_desc,pre_appointments.requester_id,pre_appointments.proposed_date,pre_appointments.proposed_time,pre_appointments.allergies,pre_appointments.prev_medical_history,suburb_masters.suburb_name,provider_masters.provider_name,request_categories.category,pre_appointments.beneficiary_name,pre_appointments.beneficiary_age,pre_appointments.beneficiary_phone_number,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,billing_info_bill_items.billing_info_id AS billing_info_bill_items_info_id,confirmed_appointments.paid,appointment_types.title AS appointment_type_title,pre_appointments.created_at,confirmed_appointments.attended_to,confirmed_appointments.confirmed_date,patient_billing_infos.approval_status")
      .where("pre_appointments.requester_id = ? and pre_appointments.appointment_type_id = ?","#{requester_id}","#{appointment_type_id}")
      .order('pre_appointments.id desc,pre_appointments.created_at desc')

  end


  def self.homecare_appointment_history_detail(appointment_id,appointment_type_id)

    # PreAppointment.joins("LEFT JOIN suburb_masters ON suburb_masters.id = pre_appointments.suburb_id LEFT JOIN provider_masters ON provider_masters.id = pre_appointments.provider_id LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency LEFT JOIN confirmed_appointments ON confirmed_appointments.pre_appointment_id = pre_appointments.id")
      # .select("pre_appointments.id,pre_appointments.medication,pre_appointments.complaint_desc,pre_appointments.requester_id,pre_appointments.proposed_date,pre_appointments.proposed_time,pre_appointments.allergies,pre_appointments.prev_medical_history,suburb_masters.suburb_name,provider_masters.provider_name,request_categories.category,pre_appointments.beneficiary_name,pre_appointments.beneficiary_phone_number,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,pre_appointments.created_at,confirmed_appointments.attended_to,confirmed_appointments.id as confirmed_appointments_id")
      # .where("pre_appointments.id = ? and pre_appointments.appointment_type_id = ?","#{appointment_id}","#{appointment_type_id}" )
      # .order('pre_appointments.confirm_status desc')
      
    PreAppointment.joins("LEFT JOIN suburb_masters ON suburb_masters.id = pre_appointments.suburb_id LEFT JOIN provider_masters ON provider_masters.id = pre_appointments.provider_id LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency LEFT JOIN confirmed_appointments ON confirmed_appointments.pre_appointment_id = pre_appointments.id")
      .select("pre_appointments.id,pre_appointments.medication,pre_appointments.complaint_desc,pre_appointments.requester_id,pre_appointments.proposed_date,pre_appointments.proposed_time,pre_appointments.allergies,pre_appointments.prev_medical_history,suburb_masters.suburb_name,provider_masters.provider_name,request_categories.category,pre_appointments.beneficiary_name,pre_appointments.beneficiary_age,pre_appointments.beneficiary_phone_number,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,pre_appointments.created_at,confirmed_appointments.attended_to, pre_appointments.test_list,confirmed_appointments.confirmed_date,confirmed_appointments.id as confirmed_appointments_id")
      .where("pre_appointments.id = ? and pre_appointments.appointment_type_id = ?","#{appointment_id}","#{appointment_type_id}" )
      .order('pre_appointments.created_at desc')
      
    # group('person_info.id,contacts.id,contact_groups.groups').
    # all.to_json

  end

  def self.lab_appointment_history(requester_id) 

    # PreAppointment.joins("LEFT JOIN suburb_masters ON suburb_masters.id = pre_appointments.suburb_id LEFT JOIN provider_masters ON provider_masters.id = pre_appointments.provider_id LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency LEFT JOIN confirmed_appointments ON confirmed_appointments.pre_appointment_id = pre_appointments.id")
      # .select("pre_appointments.id,pre_appointments.medication,pre_appointments.complaint_desc,pre_appointments.requester_id,pre_appointments.proposed_date,pre_appointments.proposed_time,pre_appointments.allergies,pre_appointments.prev_medical_history,suburb_masters.suburb_name,provider_masters.provider_name,request_categories.category,pre_appointments.beneficiary_name,pre_appointments.beneficiary_gender,pre_appointments.beneficiary_age,pre_appointments.beneficiary_phone_number,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,pre_appointments.created_at,confirmed_appointments.attended_to, pre_appointments.test_list,confirmed_appointments.confirmed_date")
      # .where("pre_appointments.requester_id = ? and pre_appointments.appointment_type_id = 'LA'","#{requester_id}" )
      # .order('pre_appointments.created_at desc')   
    
    PreAppointment.joins("LEFT JOIN suburb_masters ON suburb_masters.id = pre_appointments.suburb_id LEFT JOIN provider_masters ON provider_masters.id = pre_appointments.provider_id LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency LEFT JOIN confirmed_appointments ON confirmed_appointments.pre_appointment_id = pre_appointments.id LEFT JOIN patient_billing_infos ON patient_billing_infos.appointment_id = confirmed_appointments.id LEFT JOIN billing_info_bill_items ON billing_info_bill_items.billing_info_id = patient_billing_infos.id LEFT JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id")
      .select("DISTINCT ON (pre_appointments.id) pre_appointments.id,pre_appointments.medication,pre_appointments.complaint_desc,pre_appointments.requester_id,pre_appointments.proposed_date,pre_appointments.proposed_time,pre_appointments.allergies,pre_appointments.prev_medical_history,suburb_masters.suburb_name,provider_masters.provider_name,request_categories.category,pre_appointments.beneficiary_name,pre_appointments.beneficiary_age,pre_appointments.beneficiary_phone_number,pre_appointments.req_urgency as req_urgency_ref,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.test_list,pre_appointments.src,billing_info_bill_items.billing_info_id AS billing_info_bill_items_info_id,confirmed_appointments.id as confirmed_appointment_id,confirmed_appointments.paid,appointment_types.title AS appointment_type_title,pre_appointments.created_at,confirmed_appointments.attended_to,confirmed_appointments.confirmed_date,pre_appointments.appointment_type_id,patient_billing_infos.approval_status")
      .where("pre_appointments.requester_id = ? and pre_appointments.appointment_type_id = 'LA'","#{requester_id}" )
      .order('pre_appointments.id desc,pre_appointments.created_at desc')

  end


  def self.lab_appointment_history_detail(appointment_id)

    PreAppointment.joins("LEFT JOIN suburb_masters ON suburb_masters.id = pre_appointments.suburb_id LEFT JOIN provider_masters ON provider_masters.id = pre_appointments.provider_id LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency LEFT JOIN confirmed_appointments ON confirmed_appointments.pre_appointment_id = pre_appointments.id")
      .select("pre_appointments.id,pre_appointments.medication,pre_appointments.complaint_desc,pre_appointments.requester_id,pre_appointments.proposed_date,pre_appointments.proposed_time,pre_appointments.allergies,pre_appointments.prev_medical_history,suburb_masters.suburb_name,provider_masters.provider_name,request_categories.category,pre_appointments.beneficiary_name,pre_appointments.beneficiary_age,pre_appointments.beneficiary_phone_number,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,pre_appointments.created_at,confirmed_appointments.attended_to,pre_appointments.test_list")
      .where("pre_appointments.id = ? and pre_appointments.appointment_type_id = 'LA'","#{appointment_id}" )
      .order('pre_appointments.confirm_status desc')
    # group('person_info.id,contacts.id,contact_groups.groups').
    # all.to_json

  end



  def self.medication_appointment_history(requester_id)

    PreAppointment.joins("LEFT JOIN suburb_masters ON suburb_masters.id = pre_appointments.suburb_id LEFT JOIN provider_masters ON provider_masters.id = pre_appointments.provider_id LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency LEFT JOIN confirmed_appointments ON confirmed_appointments.pre_appointment_id = pre_appointments.id LEFT JOIN patient_billing_infos ON patient_billing_infos.appointment_id = confirmed_appointments.id LEFT JOIN billing_info_bill_items ON billing_info_bill_items.billing_info_id = patient_billing_infos.id LEFT JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id ")
      .select("DISTINCT ON (pre_appointments.id) pre_appointments.id,pre_appointments.medication,pre_appointments.complaint_desc,pre_appointments.requester_id,pre_appointments.proposed_date,pre_appointments.proposed_time,pre_appointments.allergies,pre_appointments.prev_medical_history,suburb_masters.suburb_name,provider_masters.provider_name,request_categories.category,pre_appointments.beneficiary_name,pre_appointments.beneficiary_age,pre_appointments.beneficiary_phone_number,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,billing_info_bill_items.billing_info_id AS billing_info_bill_items_info_id,confirmed_appointments.paid,appointment_types.title AS appointment_type_title,pre_appointments.created_at,confirmed_appointments.attended_to,confirmed_appointments.confirmed_date,pre_appointments.appointment_type_id,patient_billing_infos.approval_status")
      .where("pre_appointments.requester_id = ? and pre_appointments.appointment_type_id = 'MD'","#{requester_id}" )
      .order('pre_appointments.id desc,pre_appointments.created_at desc')
   
    # group('person_info.id,contacts.id,contact_groups.groups').
    # all.to_json

  end

  def self.medication_appointment_history_details(appointment_id)

    PreAppointment.joins("LEFT JOIN suburb_masters ON suburb_masters.id = pre_appointments.suburb_id LEFT JOIN provider_masters ON provider_masters.id = pre_appointments.provider_id LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency LEFT JOIN confirmed_appointments ON confirmed_appointments.pre_appointment_id = pre_appointments.id")
      .select("pre_appointments.id,pre_appointments.medication,pre_appointments.complaint_desc,pre_appointments.requester_id,pre_appointments.proposed_date,pre_appointments.proposed_time,pre_appointments.allergies,pre_appointments.prev_medical_history,suburb_masters.suburb_name,provider_masters.provider_name,request_categories.category,pre_appointments.beneficiary_name,pre_appointments.beneficiary_age,pre_appointments.beneficiary_phone_number,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,pre_appointments.created_at,confirmed_appointments.attended_to")
      .where("pre_appointments.id = ? and pre_appointments.appointment_type_id = 'MD'","#{appointment_id}" )
      .order('pre_appointments.confirm_status desc')
    # group('person_info.id,contacts.id,contact_groups.groups').
    # all.to_json

  end

  def self.personal_doctor_prescription_history(requester_id,appointment_type_id)

    # PreAppointment.joins("LEFT JOIN suburb_masters ON suburb_masters.id = pre_appointments.suburb_id LEFT JOIN provider_masters ON provider_masters.id = pre_appointments.provider_id LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency LEFT JOIN confirmed_appointments ON confirmed_appointments.pre_appointment_id = pre_appointments.id")
      # .select("pre_appointments.id,pre_appointments.medication,pre_appointments.complaint_desc,pre_appointments.requester_id,pre_appointments.proposed_date,pre_appointments.proposed_time,pre_appointments.allergies,pre_appointments.prev_medical_history,suburb_masters.suburb_name,provider_masters.provider_name,request_categories.category,pre_appointments.beneficiary_name,pre_appointments.beneficiary_age,pre_appointments.beneficiary_phone_number,pre_appointments.beneficiary_gender,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,pre_appointments.created_at,confirmed_appointments.attended_to,pre_appointments.duration,confirmed_appointments.id as confirmed_appointments_id")
      # .where("pre_appointments.requester_id = ? and pre_appointments.appointment_type_id = ?","#{requester_id}","#{appointment_type_id}" )
      # .order('pre_appointments.confirm_status desc')
    
    PreAppointment.joins("LEFT JOIN suburb_masters ON suburb_masters.id = pre_appointments.suburb_id LEFT JOIN provider_masters ON provider_masters.id = pre_appointments.provider_id LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency LEFT JOIN confirmed_appointments ON confirmed_appointments.pre_appointment_id = pre_appointments.id LEFT JOIN patient_billing_infos ON patient_billing_infos.appointment_id = confirmed_appointments.id LEFT JOIN billing_info_bill_items ON billing_info_bill_items.billing_info_id = patient_billing_infos.id LEFT JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id")
      .select("DISTINCT ON (pre_appointments.id) pre_appointments.id,pre_appointments.medication,pre_appointments.complaint_desc,pre_appointments.requester_id,pre_appointments.proposed_date,pre_appointments.proposed_time,pre_appointments.allergies,pre_appointments.prev_medical_history,suburb_masters.suburb_name,provider_masters.provider_name,request_categories.category,pre_appointments.beneficiary_name,pre_appointments.beneficiary_age,pre_appointments.beneficiary_phone_number,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,billing_info_bill_items.billing_info_id AS billing_info_bill_items_info_id,confirmed_appointments.paid,appointment_types.title AS appointment_type_title,pre_appointments.created_at,confirmed_appointments.attended_to,confirmed_appointments.confirmed_date,patient_billing_infos.approval_status")
      .where("pre_appointments.requester_id = ? and pre_appointments.appointment_type_id = ?","#{requester_id}","#{appointment_type_id}")
      .order('pre_appointments.id desc,pre_appointments.created_at desc')

  end

  def self.personal_doctor_prescription_details(appointment_id,appointment_type_id)

    # PreAppointment.joins("LEFT JOIN suburb_masters ON suburb_masters.id = pre_appointments.suburb_id LEFT JOIN provider_masters ON provider_masters.id = pre_appointments.provider_id LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency LEFT JOIN confirmed_appointments ON confirmed_appointments.pre_appointment_id = pre_appointments.id")
      # .select("pre_appointments.id,pre_appointments.requester_id,pre_appointments.medication,pre_appointments.proposed_date,pre_appointments.proposed_time,pre_appointments.confirm_status,pre_appointments.src,pre_appointments.created_at,confirmed_appointments.attended_to")
      # .where("pre_appointments.id = ? and pre_appointments.appointment_type_id = ?","#{appointment_id}","#{appointment_type_id}" )
      # .order('pre_appointments.confirm_status desc')
    # group('person_info.id,contacts.id,contact_groups.groups').
    # all.to_json
    
    PreAppointment.joins("LEFT JOIN suburb_masters ON suburb_masters.id = pre_appointments.suburb_id LEFT JOIN provider_masters ON provider_masters.id = pre_appointments.provider_id LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency LEFT JOIN confirmed_appointments ON confirmed_appointments.pre_appointment_id = pre_appointments.id")
      .select("pre_appointments.id,pre_appointments.medication,pre_appointments.complaint_desc,pre_appointments.requester_id,pre_appointments.proposed_date,pre_appointments.proposed_time,pre_appointments.allergies,pre_appointments.prev_medical_history,suburb_masters.suburb_name,provider_masters.provider_name,request_categories.category,pre_appointments.beneficiary_name,pre_appointments.beneficiary_age,pre_appointments.beneficiary_phone_number,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,pre_appointments.created_at,confirmed_appointments.attended_to,pre_appointments.duration,confirmed_appointments.id as confirmed_appointments_id")
      .where("pre_appointments.id = ? and pre_appointments.appointment_type_id = ?","#{appointment_id}","#{appointment_type_id}" )
      .order('pre_appointments.confirm_status desc')

  end


   def self.personal_doc_appointment_history(requester_id)

    # select("pre_appointments.complaint_desc,pre_appointments.appointment_type_id,pre_appointments.requester_id,pre_appointments.allergies,pre_appointments.prev_medical_history,pre_appointments.suburb_id, pre_appointments.created_at")
    # .where("pre_appointments.requester_id = ? and pre_appointments.appointment_type_id ='PD'","#{requester_id}" ).order('pre_appointments.requester_id desc')
  
    PreAppointment.joins("LEFT JOIN suburb_masters ON suburb_masters.id = pre_appointments.suburb_id")
      .select("pre_appointments.complaint_desc,pre_appointments.appointment_type_id,pre_appointments.requester_id,pre_appointments.allergies,pre_appointments.prev_medical_history,pre_appointments.suburb_id,suburb_masters.suburb_name, pre_appointments.created_at")
      .where("pre_appointments.requester_id = ? and pre_appointments.appointment_type_id ='PD'","#{requester_id}" )
      .order('pre_appointments.requester_id desc')
      
   # person_infos.id = ? and confirmed_personal_doctor_services.appointment_type_id = 'PC'","#{id}"
  end

  def self.preappointment_statistics(requester_id)

    statistics1 = PreAppointment.find_by_sql ["SELECT 'med_appointment' AS pre_appointments, COUNT(1) FROM pre_appointments WHERE appointment_type_id='MA'  AND requester_id= :requester_id
            UNION
            SELECT 'medication' AS pre_appointments, COUNT(1) FROM pre_appointments WHERE appointment_type_id='MD'  AND requester_id= :requester_id
            UNION
            SELECT 'personal_doctor' AS pre_appointments, COUNT(1) FROM pre_appointments WHERE appointment_type_id='PD' AND requester_id= :requester_id
            UNION
            SELECT 'Lab_Appointment' AS pre_appointments, COUNT(1) FROM pre_appointments WHERE appointment_type_id='LA' AND requester_id= :requester_id
            UNION
            SELECT 'Phone_Consult' AS pre_appointments, COUNT(1) FROM pre_appointments WHERE appointment_type_id='PC' AND requester_id= :requester_id
            UNION
            SELECT 'Video_Consult' AS pre_appointments, COUNT(1) FROM pre_appointments WHERE appointment_type_id='VC' AND requester_id= :requester_id
            UNION
            SELECT 'Home_Care' AS pre_appointments, COUNT(1) FROM pre_appointments WHERE appointment_type_id='HC' AND requester_id= :requester_id
            UNION
            SELECT 'Digital_Prescription' AS pre_appointments, COUNT(1) FROM pre_appointments WHERE appointment_type_id='DP' AND requester_id= :requester_id
            UNION
            SELECT 'Personal_Doctor_Home_Care' AS pre_appointments, COUNT(1) FROM pre_appointments WHERE appointment_type_id='PDHC' AND requester_id= :requester_id
            UNION
            SELECT 'Personal_Doctor_Digital_Prescription' AS pre_appointments, COUNT(1) FROM pre_appointments WHERE appointment_type_id='PDDP' AND requester_id= :requester_id",{:requester_id => requester_id}];

  end
  
  
  

end

class LabDetailImage <ActiveRecord::Base
  self.table_name = "lab_detail_images"

end

class LabServices <ActiveRecord::Base
  self.table_name = "lab_services"
  def self.get_services(id)

    select("lab_services.id,lab_services.title,lab_services.price").order('lab_services.id desc')
  end

end

class DrugPrice <ActiveRecord::Base
  self.table_name = "drug_prices"
  

end

class DrugMaster <ActiveRecord::Base
  self.table_name = "drug_masters"
  def self.get_drugs(id)

   joins("LEFT JOIN drug_prices ON drug_masters.id =  drug_prices.drug_id").select("drug_masters.id,drug_prices.price,drug_name")

  end

end

class ConfirmedPersonalDoctorService <ActiveRecord::Base

  self.table_name ="confirmed_personal_doctor_services"
  
  belongs_to :confirmed_doctor, class_name: "PersonInfo",foreign_key: :doctor_id
  belongs_to :confirmed_patient, class_name: "PersonInfo",foreign_key: :patient_id
  
  def self.get_patient_doctor(user_id)
    
    ConfirmedPersonalDoctorService.select("confirmed_personal_doctor_services.*")
                                  .where("patient_id = ? AND appointment_type_id ='PD'","#{user_id}")
     # SELECT "confirmed_personal_doctor_services".* FROM "confirmed_personal_doctor_services" WHERE (patient_id =286 AND (appointment_type_id ='PD' ))
  end  
  
  
  # def self.fdsfa()
#     
    # # SELECT "pre_appointments".* FROM "pre_appointments" WHERE (requester_id =286 AND appointment_type_id ='PD' AND confirm_status ='f' )
  # end

 def self.doctor_patients(id)

  joins("LEFT JOIN person_infos ON confirmed_personal_doctor_services.doctor_id =  person_infos.id LEFT JOIN registration ON person_infos.reg_id = registration.id ").select("pre_appointment_id,patient_id,appointment_type_id,person_infos.surname,person_infos.other_names").
  where( "person_infos.id = ? and confirmed_personal_doctor_services.appointment_type_id = 'PC'","#{id}").order('person_infos.created_at desc')

  end

   def self.doctor_patients4(id)

  joins("LEFT JOIN person_infos ON confirmed_personal_doctor_services.doctor_id =  person_infos.id LEFT JOIN registration ON person_infos.reg_id = registration.id ").select("pre_appointment_id,patient_id,appointment_type_id,person_infos.surname,person_infos.other_names").where( "person_infos.id = ? and confirmed_personal_doctor_services.appointment_type_id = 'PD'","#{id}").order('person_infos.created_at desc')

  end

  def self.doctor_patients2(id)

  joins("LEFT JOIN person_infos ON confirmed_personal_doctor_services.doctor_id =  person_infos.id LEFT JOIN registration ON person_infos.reg_id = registration.id ").select("pre_appointment_id,patient_id,appointment_type_id,person_infos.surname,person_infos.other_names").where( "person_infos.id = ? and confirmed_personal_doctor_services.appointment_type_id = 'PDVC'","#{id}").order('person_infos.created_at desc')

  end


   def self.doctor_patients3(id)

  joins("LEFT JOIN person_infos ON confirmed_personal_doctor_services.doctor_id =  person_infos.id LEFT JOIN registration ON person_infos.reg_id = registration.id ").select("pre_appointment_id,patient_id,appointment_type_id,person_infos.surname,person_infos.other_names").where( "person_infos.id = ? and confirmed_personal_doctor_services.appointment_type_id = 'PDHC'","#{id}").order('person_infos.created_at desc')

  end

    def self.prescription_list(id,appointment_type_id)

  joins("LEFT JOIN person_infos ON confirmed_personal_doctor_services.doctor_id =  person_infos.id LEFT JOIN registration ON person_infos.reg_id = registration.id ").select("pre_appointment_id,patient_id,appointment_type_id,person_infos.surname,person_infos.other_names")
  .where( "person_infos.id = ? and confirmed_personal_doctor_services.appointment_type_id = ?","#{id}","#{appointment_type_id}").order('person_infos.created_at desc')

  end
end


class PatientMedRecord < ActiveRecord::Base
 self.table_name ="patient_med_records"

 def self.retrieve_order_details(patient_id,doctor_id)

    # select("patient_med_records.id,patient_med_records.patient_id,patient_med_records.doctor_id,patient_med_records.clinical_complaints,patient_med_records.clinical_examinations,patient_med_records.working_diagnosis,patient_med_records.investigation_rquired,patient_med_records.treatments,patient_med_records.follow_up_plan,patient_med_records.created_at ").where(
    # "patient_med_records.patient_id = ?","#{patient_id}" ).order('patient_med_records.patient_id desc')
#     
    # SELECT  "confirmed_appointments".id,confirmed_appointments.pre_appointment_id,confirmed_appointments.complaint_desc,confirmed_appointments.created_at,pre_appointments.appointment_type_id,appointment_types.title FROM "confirmed_appointments" INNER JOIN "patient_med_records" ON "patient_med_records"."confirmed_appt_id" = "confirmed_appointments"."id" LEFT JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id LEFT JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id WHERE (confirmed_appointments.patient_id ='247' and confirmed_appointments.doctor_id ='248' and confirmed_appointments.attended_to ='t') order by id desc;
#     
    ConfirmedAppointment.joins("INNER JOIN patient_med_records ON patient_med_records.confirmed_appt_id = confirmed_appointments.id LEFT JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id LEFT JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id")
          .select("confirmed_appointments.id,confirmed_appointments.pre_appointment_id,confirmed_appointments.complaint_desc,confirmed_appointments.created_at,pre_appointments.appointment_type_id,appointment_types.title,patient_med_records.clinical_complaints,patient_med_records.clinical_examinations,patient_med_records.working_diagnosis,patient_med_records.investigation_rquired,patient_med_records.treatments,patient_med_records.follow_up_plan")
          .where("confirmed_appointments.patient_id =? and confirmed_appointments.doctor_id =? and confirmed_appointments.attended_to ='t'","#{patient_id}","#{doctor_id}" )
          .order("confirmed_appointments.id desc")
      
  end
  
  def self.retrieve_order_details1(confirmed_appointment_id)
    
    PatientMedRecord.select("patient_med_records.*")
                    .where("patient_med_records.confirmed_appt_id = ?","#{confirmed_appointment_id}")
  end

end

class ConfirmedAppointment < ActiveRecord::Base
  self.table_name ="confirmed_appointments"
  
  def self.confirmed_appointments_by_patient(patient_id)

   
      
    ConfirmedAppointment.joins("INNER JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id INNER JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id INNER JOIN person_infos ON person_infos.id = confirmed_appointments.patient_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = person_infos.id LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat")
      .select("confirmed_appointments.id, confirmed_appointments.pre_appointment_id, confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,confirmed_appointments.patient_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,person_contact_infos.email,pre_appointments.medication,pre_appointments.duration,confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,true as accepted_status,confirmed_appointments.attended_to,pre_appointments.beneficiary_name,pre_appointments.beneficiary_age,pre_appointments.beneficiary_phone_number,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,request_categories.category,pre_appointments.allergies,pre_appointments.prev_medical_history,pre_appointments.proposed_date,pre_appointments.proposed_time")
      .where("confirmed_appointments.patient_id = ? and confirmed_appointments.attended_to=false AND pre_appointments.appointment_type_id IN ('PDVC','PDDP','PDHC','PC')","#{patient_id}" )
      .order("confirmed_appointments.id desc")
      
  end
  
  
   def self.confirmed_appointments_by_doctor(doc_id)

   
      
    ConfirmedAppointment.joins("INNER JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id INNER JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id INNER JOIN person_infos ON person_infos.id = confirmed_appointments.patient_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = person_infos.id LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat")
      .select("confirmed_appointments.id, confirmed_appointments.pre_appointment_id, confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,confirmed_appointments.patient_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,person_contact_infos.email,pre_appointments.medication,pre_appointments.duration,confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,true as accepted_status,confirmed_appointments.attended_to,pre_appointments.beneficiary_name,pre_appointments.appt_amnt,pre_appointments.beneficiary_age,pre_appointments.beneficiary_phone_number,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,request_categories.category,pre_appointments.allergies,pre_appointments.prev_medical_history,pre_appointments.proposed_date,pre_appointments.proposed_time")
      .where("confirmed_appointments.doctor_id = ? and confirmed_appointments.attended_to=true AND pre_appointments.appointment_type_id IN ('PDVC','PDDP','PDHC','PC')","#{doc_id}" )
      .order("confirmed_appointments.id desc")
      
  end
  
  def self.confirmed_appointments_by_patient_count(patient_id)

    ConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM confirmed_appointments
         LEFT JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id LEFT JOIN person_infos ON person_infos.id = pre_appointments.requester_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = person_infos.id LEFT JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id
         WHERE confirmed_appointments.patient_id = :patient_id and confirmed_appointments.attended_to=false AND pre_appointments.appointment_type_id IN ('PDVC','PDDP','PDHC','PC') ",{:patient_id => patient_id}];

      
  end
  
  
  def self.accepted_appointments_details_for_sms(appt_id)

    ConfirmedAppointment.joins("LEFT JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id LEFT JOIN person_infos ON person_infos.id = pre_appointments.requester_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = person_infos.id LEFT JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id")
      .select("confirmed_appointments.id,confirmed_appointments.pre_appointment_id,confirmed_appointments.confirmed_date,pre_appointments.appointment_type_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,person_contact_infos.email,confirmed_appointments.doctor_id,confirmed_appointments.patient_id,confirmed_appointments.attended_to")
      .where("confirmed_appointments.pre_appointment_id = ?","#{appt_id}" )
      .order("confirmed_appointments.id desc")
     
  end
  
 

end


class Referal < ActiveRecord::Base
  self.table_name ="referals"
  
  def self.retrieve_patients_referred(doc_id)
    # location = "%#{doc_id}%"
    select("referals.id,referals.surname,referals.other_names,referals.dob,referals.contact,referals.refered_by,referals.active_status,referals.created_at ")
    .where("referals.refered_by = ? and referals.active_status = true","#{doc_id}" ).order('referals.id desc')
  end
  
  
end

class PreConfirmedAppointment <ActiveRecord::Base
  
  self.table_name ="pre_confirmed_appointments"
  
  
  def self.medical_appointments_count(patient_id)
    puts "patient_id = #{patient_id}"

      # PreConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM pre_appointments
         # WHERE pre_appointments.requester_id = :patient_id and pre_appointments.confirm_status=true and pre_appointments.appointment_type_id IN ('MA') AND pre_appointments.counter_view IS NOT TRUE AND pre_appointments.active_status=true AND pre_appointments.del_status = false",{:patient_id => patient_id}];
#    
 PreConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM pre_appointments
         WHERE pre_appointments.requester_id = :patient_id and pre_appointments.confirm_status=true and pre_appointments.appointment_type_id IN ('MA') AND pre_appointments.active_status=true AND pre_appointments.del_status = false",{:patient_id => patient_id}];
     
  end
  
  def self.medication_appointments_count(patient_id)

      PreConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM pre_appointments
         WHERE pre_appointments.requester_id = :patient_id and pre_appointments.confirm_status=true and pre_appointments.appointment_type_id IN ('MD') AND pre_appointments.active_status=true AND pre_appointments.del_status = false",{:patient_id => patient_id}];
         
  end
  
  def self.labrequest_appointments_count(patient_id)

      PreConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM pre_appointments
         WHERE pre_appointments.requester_id = :patient_id and pre_appointments.confirm_status=true and pre_appointments.appointment_type_id IN ('LA') AND pre_appointments.active_status=true AND pre_appointments.del_status = false",{:patient_id => patient_id}];
         
  end
  
  def self.homecare_appointments_count(patient_id)

      PreConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM pre_appointments
         WHERE pre_appointments.requester_id = :patient_id and pre_appointments.confirm_status=true and pre_appointments.appointment_type_id IN ('HC') AND pre_appointments.active_status=true AND pre_appointments.del_status = false",{:patient_id => patient_id}];
         
  end
  
  def self.phoneconsult_appointments_count(patient_id)

      PreConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM pre_appointments
         WHERE pre_appointments.requester_id = :patient_id and pre_appointments.confirm_status=true and pre_appointments.appointment_type_id IN ('PC') AND pre_appointments.active_status=true AND pre_appointments.del_status = false",{:patient_id => patient_id}];
         
  end
  
  def self.videoconsult_appointments_count(patient_id)

      PreConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM pre_appointments
         WHERE pre_appointments.requester_id = :patient_id and pre_appointments.confirm_status=true and pre_appointments.appointment_type_id IN ('VC') AND pre_appointments.active_status=true AND pre_appointments.del_status = false",{:patient_id => patient_id}];
         
  end
  
  
  
  
  
  #PDS
  def self.pdhc_appointments_count(patient_id)

      PreConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM pre_appointments
         WHERE pre_appointments.requester_id = :patient_id and pre_appointments.confirm_status=true and pre_appointments.appointment_type_id IN ('PDHC') AND pre_appointments.active_status=true AND pre_appointments.del_status = false",{:patient_id => patient_id}];
         
  end
  
  def self.pdpc_appointments_count(patient_id)

      PreConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM pre_appointments
         WHERE pre_appointments.requester_id = :patient_id and pre_appointments.confirm_status=true and pre_appointments.appointment_type_id IN ('PC') AND pre_appointments.active_status=true AND pre_appointments.del_status = false",{:patient_id => patient_id}];
         
         
  end
  
  def self.pdvc_appointments_count(patient_id)

      PreConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM pre_appointments
         WHERE pre_appointments.requester_id = :patient_id and pre_appointments.confirm_status=true and pre_appointments.appointment_type_id IN ('PDVC') AND pre_appointments.active_status=true AND pre_appointments.del_status = false",{:patient_id => patient_id}];
         
  end
  
  def self.pddp_appointments_count(patient_id)

      PreConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM pre_appointments
         WHERE pre_appointments.requester_id = :patient_id and pre_appointments.confirm_status=true and pre_appointments.appointment_type_id IN ('PDDP') AND pre_appointments.active_status=true AND pre_appointments.del_status = false",{:patient_id => patient_id}];
         
  end
  
  
  def self.patient_pds_appointments_count(patient_id)

      PreConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM pre_appointments
         WHERE pre_appointments.requester_id = :patient_id and pre_appointments.confirm_status=true and pre_appointments.appointment_type_id IN ('PDHC','PC','PDVC','PDDP') AND pre_appointments.active_status=true AND pre_appointments.del_status = false",{:patient_id => patient_id}];
         
  end
  
  
  
  
  
  
  
  
  
  def self.new_general_appointments(doc_id,person_type)

    # PreConfirmedAppointment.joins("LEFT JOIN pre_appointments ON pre_appointments.id = pre_confirmed_appointments.pre_appointment_id LEFT JOIN person_infos ON person_infos.id = pre_appointments.requester_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = person_infos.id LEFT JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id")
      # .select("pre_confirmed_appointments.id,pre_appointments.id as pre_appointment_id,pre_appointments.requester_id,pre_appointments.proposed_date,pre_appointments.appointment_type_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,pre_confirmed_appointments.accepted_status,pre_confirmed_appointments.decline_status,pre_confirmed_appointments.created_at")
      # .where("pre_confirmed_appointments.doctor_id = ? and pre_confirmed_appointments.accepted_status=false and pre_confirmed_appointments.decline_status=false and pre_appointments.appointment_type_id IN ('VC','MD','HC') AND pre_appointments.active_status=true and pre_confirmed_appointments.closed=false AND pre_appointments.del_status = false","#{doc_id}" )
      # .order("pre_confirmed_appointments.id desc")
      
      if person_type 
        case person_type
        when "D"
          PreConfirmedAppointment.joins("LEFT JOIN pre_appointments ON pre_appointments.id = pre_confirmed_appointments.pre_appointment_id LEFT JOIN person_infos ON person_infos.id = pre_appointments.requester_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = person_infos.id LEFT JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency")
            .select("pre_confirmed_appointments.id,pre_appointments.requester_id,pre_confirmed_appointments.pre_appointment_id,pre_appointments.proposed_date,pre_appointments.appointment_type_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,pre_confirmed_appointments.accepted_status,pre_confirmed_appointments.decline_status,pre_confirmed_appointments.created_at,pre_appointments.medication,pre_appointments.duration,pre_appointments.beneficiary_name,pre_appointments.beneficiary_age,pre_appointments.beneficiary_phone_number,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,request_categories.category,pre_appointments.allergies,pre_appointments.prev_medical_history,pre_appointments.appt_amnt")
            .where("pre_confirmed_appointments.doctor_id = ? and pre_confirmed_appointments.accepted_status=false and pre_confirmed_appointments.decline_status=false  and pre_appointments.appointment_type_id IN ('VC','MD','HC') AND pre_appointments.active_status=true and pre_confirmed_appointments.closed = false AND pre_appointments.del_status = false","#{doc_id}" )
            .order("pre_confirmed_appointments.id desc")
        when "N"
          PreConfirmedAppointment.joins("LEFT JOIN pre_appointments ON pre_appointments.id = pre_confirmed_appointments.pre_appointment_id LEFT JOIN person_infos ON person_infos.id = pre_appointments.requester_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = person_infos.id LEFT JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency")
            .select("pre_confirmed_appointments.id,pre_appointments.requester_id,pre_confirmed_appointments.pre_appointment_id,pre_appointments.proposed_date,pre_appointments.appointment_type_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,pre_confirmed_appointments.accepted_status,pre_confirmed_appointments.decline_status,pre_confirmed_appointments.created_at,pre_appointments.medication,pre_appointments.duration,pre_appointments.beneficiary_name,pre_appointments.beneficiary_age,pre_appointments.beneficiary_phone_number,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,request_categories.category,pre_appointments.allergies,pre_appointments.prev_medical_history,pre_appointments.appt_amnt")
            .where("pre_confirmed_appointments.nurse_id = ? and pre_confirmed_appointments.accepted_status=false and pre_confirmed_appointments.decline_status=false  and pre_appointments.appointment_type_id IN ('HC') AND pre_appointments.active_status=true and pre_confirmed_appointments.closed = false AND pre_appointments.del_status = false","#{doc_id}" )
            .order("pre_confirmed_appointments.id desc")
        else
          return
        end

      end
      
      # PreConfirmedAppointment.joins("LEFT JOIN pre_appointments ON pre_appointments.id = pre_confirmed_appointments.pre_appointment_id LEFT JOIN person_infos ON person_infos.id = pre_appointments.requester_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = person_infos.id LEFT JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency")
      # .select("pre_confirmed_appointments.id,pre_appointments.requester_id,pre_confirmed_appointments.pre_appointment_id,pre_appointments.proposed_date,pre_appointments.appointment_type_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,pre_confirmed_appointments.accepted_status,pre_confirmed_appointments.decline_status,pre_confirmed_appointments.created_at,pre_appointments.medication,pre_appointments.duration,pre_appointments.beneficiary_name,pre_appointments.beneficiary_age,pre_appointments.beneficiary_gender,pre_appointments.beneficiary_phone_number,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,request_categories.category,pre_appointments.allergies,pre_appointments.prev_medical_history")
      # .where("pre_confirmed_appointments.doctor_id = ? and pre_confirmed_appointments.accepted_status=false and pre_confirmed_appointments.decline_status=false  and pre_appointments.appointment_type_id IN ('VC','MD','HC') AND pre_appointments.active_status=true and pre_confirmed_appointments.closed = false AND pre_appointments.del_status = false","#{doc_id}" )
      # .order("pre_confirmed_appointments.id desc")
     
  end
  
  def self.new_general_appointments_count(doc_id,person_type)
    
    puts "IN Model - new_general_appointments_count doc_id = #{doc_id} AND person_type = #{person_type}"

    if person_type 
      case person_type
      when "D"
        PreConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM pre_confirmed_appointments
         LEFT JOIN pre_appointments ON pre_appointments.id = pre_confirmed_appointments.pre_appointment_id
         WHERE pre_confirmed_appointments.doctor_id = :doctor_id and pre_confirmed_appointments.accepted_status=false and pre_confirmed_appointments.decline_status=false and pre_appointments.appointment_type_id IN ('VC','MD','HC') AND pre_appointments.active_status=true and pre_confirmed_appointments.closed=false AND pre_appointments.del_status = false",{:doctor_id => doc_id}];

      when "N"
        PreConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM pre_confirmed_appointments
         LEFT JOIN pre_appointments ON pre_appointments.id = pre_confirmed_appointments.pre_appointment_id
         WHERE pre_confirmed_appointments.nurse_id = :nurse_id and pre_confirmed_appointments.accepted_status=false and pre_confirmed_appointments.decline_status=false and pre_appointments.appointment_type_id IN ('HC') AND pre_appointments.active_status=true and pre_confirmed_appointments.closed=false AND pre_appointments.del_status = false",{:nurse_id => doc_id}];

      else
        return
      end
    end

  end
  
  
  def self.new_pds_appointments_count(doc_id)

      PreConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM pre_confirmed_appointments
         LEFT JOIN pre_appointments ON pre_appointments.id = pre_confirmed_appointments.pre_appointment_id
         WHERE pre_confirmed_appointments.doctor_id = :doctor_id and pre_confirmed_appointments.accepted_status=false and pre_confirmed_appointments.decline_status=false and pre_appointments.appointment_type_id IN ('PDVC','PDDP','PDHC','PC','PD') AND pre_appointments.active_status=true and pre_confirmed_appointments.closed=false AND pre_appointments.del_status = false",{:doctor_id => doc_id}];

  end
  
  def self.new_total_appointments_count(doc_id,person_type)
    puts "in MODEL - new_total_appointments_count: person_type = #{person_type} AND doc_id = #{doc_id}"
    if person_type 
      case person_type
      when "D"
        PreConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM pre_confirmed_appointments
         LEFT JOIN pre_appointments ON pre_appointments.id = pre_confirmed_appointments.pre_appointment_id
         WHERE pre_confirmed_appointments.doctor_id = :doctor_id and pre_confirmed_appointments.accepted_status=false and pre_confirmed_appointments.decline_status=false and pre_appointments.appointment_type_id IN ('PDVC','PDDP','PDHC','PD','PC','VC','MD','HC') AND pre_appointments.active_status=true and pre_confirmed_appointments.closed=false AND pre_appointments.del_status = false",{:doctor_id => doc_id}];

      when "N"
        PreConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM pre_confirmed_appointments
         LEFT JOIN pre_appointments ON pre_appointments.id = pre_confirmed_appointments.pre_appointment_id
         WHERE pre_confirmed_appointments.nurse_id = :nurse_id and pre_confirmed_appointments.accepted_status=false and pre_confirmed_appointments.decline_status=false and pre_appointments.appointment_type_id IN ('HC') AND pre_appointments.active_status=true and pre_confirmed_appointments.closed=false AND pre_appointments.del_status = false",{:nurse_id => doc_id}];

      else
        return
      end
    end

      
  end
  
  def self.general_doc_total_appointments_count(doc_id,person_type)
    
    if person_type 
      case person_type
      when "D"
        ConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM confirmed_appointments
         LEFT JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id
         WHERE confirmed_appointments.doctor_id = :doctor_id AND pre_appointments.appointment_type_id IN ('VC','MD','HC') and attended_to = false AND (confirmed_appointments.paid IS NOT NULL)",{:doctor_id => doc_id}];

      when "N"
        
        ConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM confirmed_appointments
         LEFT JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id
         WHERE confirmed_appointments.nurse_id = :nurse_id AND pre_appointments.appointment_type_id IN ('HC') and attended_to = false AND (confirmed_appointments.paid IS NOT NULL)",{:nurse_id => doc_id}];

      else
        return
      end
    end

      
# ConfirmedAppointment.joins("INNER JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id INNER JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id INNER JOIN person_infos ON person_infos.id = confirmed_appointments.patient_id")
      # .select("confirmed_appointments.id, confirmed_appointments.pre_appointment_id, confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,confirmed_appointments.patient_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names")
      # .where('confirmed_appointments.doctor_id =? AND (pre_appointments.appointment_type_id =?) ', doc_id, 'MD' )
      # .order("confirmed_appointments.id desc")
      
      
  end
  

  
  
   def self.general_doc_vc_appointments_count(doc_id)

      ConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM confirmed_appointments
         LEFT JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id
         WHERE confirmed_appointments.doctor_id = :doctor_id and pre_appointments.appointment_type_id IN ('VC') and attended_to = false and (confirmed_appointments.paid IS NOT NULL)",{:doctor_id => doc_id}];
  end
  
  def self.general_doc_md_appointments_count(doc_id)

      ConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM confirmed_appointments
         LEFT JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id
         WHERE confirmed_appointments.doctor_id = :doctor_id and pre_appointments.appointment_type_id IN ('MD') and attended_to = false",{:doctor_id => doc_id}];
  end
  
  def self.general_doc_hc_appointments_count(doc_id,person_type)

    if person_type 
      case person_type
      when "D"
        
        ConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM confirmed_appointments
         LEFT JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id
         WHERE confirmed_appointments.doctor_id = :doctor_id and pre_appointments.appointment_type_id IN ('HC') and attended_to = false AND (confirmed_appointments.paid IS NOT NULL)",{:doctor_id => doc_id}];

      when "N"
        
        ConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM confirmed_appointments
         LEFT JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id
         WHERE confirmed_appointments.doctor_id = :nurse_id and pre_appointments.appointment_type_id IN ('HC') and attended_to = false AND (confirmed_appointments.paid IS NOT NULL)",{:nurse_id => doc_id}];
       
      else
        return
      end
    end
    
  end
  
  def self.pds_doc_pdvc_appointments_count(doc_id)

      ConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM confirmed_appointments
         LEFT JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id
         WHERE confirmed_appointments.doctor_id = :doctor_id AND pre_appointments.appointment_type_id IN ('PDVC') and attended_to = false",{:doctor_id => doc_id}];
  end
  
  def self.pds_doc_pddp_appointments_count(doc_id)

      ConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM confirmed_appointments
         LEFT JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id
         WHERE confirmed_appointments.doctor_id = :doctor_id AND pre_appointments.appointment_type_id IN ('PDDP') and attended_to = false",{:doctor_id => doc_id}];
  end
  
  def self.pds_doc_pdhc_appointments_count(doc_id)

      ConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM confirmed_appointments
         LEFT JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id
         WHERE confirmed_appointments.doctor_id = :doctor_id AND pre_appointments.appointment_type_id IN ('PDHC') and attended_to = false",{:doctor_id => doc_id}];
  end
  
  def self.pds_doc_pc_appointments_count(doc_id)

      ConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM confirmed_appointments
         LEFT JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id
         WHERE confirmed_appointments.doctor_id = :doctor_id AND pre_appointments.appointment_type_id IN ('PC') and attended_to = false",{:doctor_id => doc_id}];
  end
  
  def self.pds_doc_total_appointments_count(doc_id)

      ConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM confirmed_appointments
         LEFT JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id
         WHERE confirmed_appointments.doctor_id = :doctor_id AND pre_appointments.appointment_type_id IN ('PDVC','PDDP','PDHC','PC') and attended_to = false",{:doctor_id => doc_id}];
  end
  
  
 # def self.total_all_appointments_count(doc_id)
# 
      # ConfirmedAppointment.find_by_sql ["SELECT COUNT(*) as counter FROM confirmed_appointments
         # LEFT JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id
         # WHERE confirmed_appointments.doctor_id = :doctor_id AND pre_appointments.appointment_type_id IN ('VC','MD','HC')",{:doctor_id => doc_id}];
# 
  # end
  
  
  def self.new_general_appointments_details(record_id)

    PreConfirmedAppointment.joins("LEFT JOIN pre_appointments ON pre_appointments.id = pre_confirmed_appointments.pre_appointment_id LEFT JOIN person_infos ON person_infos.id = pre_appointments.requester_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = person_infos.id LEFT JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id")
      .select("pre_confirmed_appointments.id,pre_appointments.requester_id,pre_appointments.id as pre_appointment_id,pre_appointments.proposed_date,pre_appointments.appointment_type_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,pre_confirmed_appointments.accepted_status,pre_confirmed_appointments.decline_status,pre_confirmed_appointments.created_at")
      .where("pre_confirmed_appointments.id = ? and pre_confirmed_appointments.accepted_status=false and pre_confirmed_appointments.decline_status=false and pre_appointments.appointment_type_id IN ('VC','MD','HC') AND pre_appointments.active_status=true and pre_confirmed_appointments.closed = false  AND pre_appointments.del_status = false","#{record_id}" )
      .order("pre_confirmed_appointments.id desc")
     
  end
  
  def self.new_personaldoctorserviceappointments(doc_id)

    PreConfirmedAppointment.joins("LEFT JOIN pre_appointments ON pre_appointments.id = pre_confirmed_appointments.pre_appointment_id LEFT JOIN person_infos ON person_infos.id = pre_appointments.requester_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = person_infos.id LEFT JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency")
      .select("pre_confirmed_appointments.id,pre_appointments.requester_id,pre_confirmed_appointments.pre_appointment_id,pre_appointments.proposed_date,pre_appointments.appointment_type_id,pre_appointments.appt_amnt,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,pre_confirmed_appointments.accepted_status,pre_confirmed_appointments.decline_status,pre_confirmed_appointments.created_at,pre_appointments.medication,pre_appointments.duration,pre_appointments.beneficiary_name,pre_appointments.beneficiary_age,pre_appointments.beneficiary_phone_number,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,request_categories.category,pre_appointments.allergies,pre_appointments.prev_medical_history")
      .where("pre_confirmed_appointments.doctor_id = ? and pre_confirmed_appointments.accepted_status=false and pre_confirmed_appointments.decline_status=false  and pre_appointments.appointment_type_id IN ('PDVC','PDDP','PDHC','PC','PD') AND pre_appointments.active_status=true and pre_confirmed_appointments.closed = false AND pre_appointments.del_status = false","#{doc_id}" )
      .order("pre_confirmed_appointments.id desc")
      
      
     
  end
  
  def self.new_personaldoctorserviceappointments_details(record_id)

    PreConfirmedAppointment.joins("LEFT JOIN pre_appointments ON pre_appointments.id = pre_confirmed_appointments.pre_appointment_id LEFT JOIN person_infos ON person_infos.id = pre_appointments.requester_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = person_infos.id LEFT JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id")
      .select("pre_confirmed_appointments.id,pre_appointments.requester_id,pre_confirmed_appointments.pre_appointment_id,pre_appointments.proposed_date,pre_appointments.appointment_type_id,pre_appointments.appt_amnt,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,pre_confirmed_appointments.accepted_status,pre_confirmed_appointments.decline_status,pre_confirmed_appointments.created_at")
      .where("pre_confirmed_appointments.id = ? and pre_confirmed_appointments.accepted_status=false and pre_confirmed_appointments.decline_status=false and pre_appointments.appointment_type_id IN ('PDVC','PDDP','PDHC','PC') AND pre_appointments.active_status=true and pre_confirmed_appointments.closed = false AND pre_appointments.del_status = false","#{record_id}" )
      .order("pre_confirmed_appointments.id desc")
     
  end
  
  def self.general_appointments_medication(doc_id)

    # PreConfirmedAppointment.joins("LEFT JOIN pre_appointments ON pre_appointments.id = pre_confirmed_appointments.pre_appointment_id LEFT JOIN person_infos ON person_infos.id = pre_appointments.requester_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = person_infos.id LEFT JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id")
      # .select("pre_confirmed_appointments.id,pre_appointments.requester_id,pre_appointments.proposed_date,pre_appointments.appointment_type_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,pre_appointments.medication,pre_appointments.duration,pre_confirmed_appointments.accepted_status,pre_confirmed_appointments.decline_status,pre_confirmed_appointments.created_at")
      # .where("pre_confirmed_appointments.doctor_id = ? and pre_confirmed_appointments.accepted_status=true and pre_confirmed_appointments.decline_status=false and pre_appointments.appointment_type_id IN ('MD') AND pre_appointments.active_status=true and pre_confirmed_appointments.closed = true AND pre_appointments.del_status = false","#{doc_id}" )
      # .order("pre_confirmed_appointments.id desc")
      
      ConfirmedAppointment.joins("INNER JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id INNER JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id INNER JOIN person_infos ON person_infos.id = confirmed_appointments.patient_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = person_infos.id LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat")
      .select("confirmed_appointments.id, confirmed_appointments.pre_appointment_id, confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,confirmed_appointments.patient_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,pre_appointments.medication,pre_appointments.duration,confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,true as accepted_status,confirmed_appointments.attended_to,pre_appointments.beneficiary_name,pre_appointments.beneficiary_age,pre_appointments.beneficiary_phone_number,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,request_categories.category,pre_appointments.allergies,pre_appointments.prev_medical_history,pre_appointments.proposed_date,pre_appointments.proposed_time")
      .where('confirmed_appointments.doctor_id =? AND (pre_appointments.appointment_type_id =?) AND confirmed_appointments.attended_to = false', doc_id, 'MD' )
      .order("confirmed_appointments.id desc")
      
        
  end
  
  def self.general_appointments_medication_details(record_id)

    # PreConfirmedAppointment.joins("LEFT JOIN pre_appointments ON pre_appointments.id = pre_confirmed_appointments.pre_appointment_id LEFT JOIN person_infos ON person_infos.id = pre_appointments.requester_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = person_infos.id LEFT JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id")
      # .select("pre_confirmed_appointments.id,pre_appointments.requester_id,pre_confirmed_appointments.pre_appointment_id,pre_appointments.proposed_date,pre_appointments.appointment_type_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,pre_appointments.medication,pre_appointments.duration,pre_confirmed_appointments.accepted_status,pre_confirmed_appointments.decline_status,pre_confirmed_appointments.created_at")
      # .where("pre_confirmed_appointments.id = ? and pre_confirmed_appointments.accepted_status=true and pre_confirmed_appointments.decline_status=false and pre_appointments.appointment_type_id IN ('MD') AND pre_appointments.active_status=true and pre_confirmed_appointments.closed = true AND pre_appointments.del_status = false","#{record_id}" )
      # .order("pre_confirmed_appointments.id desc")
#      

     ConfirmedAppointment.joins("INNER JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id INNER JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id INNER JOIN person_infos ON person_infos.id = confirmed_appointments.patient_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = confirmed_appointments.patient_id ")
      .select("confirmed_appointments.id, confirmed_appointments.pre_appointment_id, pre_appointments.medication,pre_appointments.duration,confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,confirmed_appointments.patient_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,true as accepted_status")
      .where('confirmed_appointments.id =? ', record_id)
  end
  
  def self.general_appointments_videoconsult(doc_id)

   # ConfirmedAppointment.joins("INNER JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id INNER JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id INNER JOIN person_infos ON person_infos.id = confirmed_appointments.patient_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = confirmed_appointments.patient_id")
      # .select("confirmed_appointments.id, confirmed_appointments.pre_appointment_id, confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,confirmed_appointments.patient_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,true as accepted_status,confirmed_appointments.attended_to")
      # .where('confirmed_appointments.doctor_id =? AND (pre_appointments.appointment_type_id =?) AND confirmed_appointments.attended_to = false', doc_id, 'VC' )
      # .order("confirmed_appointments.id desc")
      
  ConfirmedAppointment.joins("INNER JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id INNER JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id INNER JOIN person_infos ON person_infos.id = confirmed_appointments.patient_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = person_infos.id LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat")
  .select("confirmed_appointments.id, confirmed_appointments.pre_appointment_id, confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,confirmed_appointments.patient_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,pre_appointments.medication,pre_appointments.duration,confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,true as accepted_status,confirmed_appointments.attended_to,pre_appointments.beneficiary_name,pre_appointments.beneficiary_age,pre_appointments.beneficiary_phone_number,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,request_categories.category,pre_appointments.allergies,pre_appointments.prev_medical_history,pre_appointments.proposed_date,pre_appointments.proposed_time,confirmed_appointments.paid")
  .where('confirmed_appointments.doctor_id =? AND (pre_appointments.appointment_type_id =?) AND confirmed_appointments.attended_to = false', doc_id, 'VC' )
  .order("confirmed_appointments.id desc")
      
     
  end
  
  def self.general_appointments_videoconsult_details(record_id)

    # PreConfirmedAppointment.joins("LEFT JOIN pre_appointments ON pre_appointments.id = pre_confirmed_appointments.pre_appointment_id LEFT JOIN person_infos ON person_infos.id = pre_appointments.requester_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = person_infos.id LEFT JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id")
      # .select("pre_confirmed_appointments.id,pre_appointments.requester_id,pre_confirmed_appointments.pre_appointment_id,pre_appointments.proposed_date,pre_appointments.appointment_type_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,pre_appointments.medication,pre_appointments.duration,pre_confirmed_appointments.accepted_status,pre_confirmed_appointments.decline_status,pre_confirmed_appointments.created_at")
      # .where("pre_confirmed_appointments.id = ? and pre_confirmed_appointments.accepted_status=true and pre_confirmed_appointments.decline_status=false and pre_appointments.appointment_type_id IN ('VC') AND pre_appointments.active_status=true and pre_confirmed_appointments.closed = true AND pre_appointments.del_status = false","#{record_id}" )
      # .order("pre_confirmed_appointments.id desc")
      
   ConfirmedAppointment.joins("INNER JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id INNER JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id INNER JOIN person_infos ON person_infos.id = confirmed_appointments.patient_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = confirmed_appointments.patient_id ")
      .select("confirmed_appointments.id, confirmed_appointments.pre_appointment_id, confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,confirmed_appointments.patient_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,true as accepted_status")
      .where('confirmed_appointments.id =? ', record_id)
     
  end
  
  def self.general_appointments_homecare(doc_id,person_type)

   
      # doctor_home_appointments = 
      # ConfirmedAppointment.joins("INNER JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id INNER JOIN person_infos ON person_infos.id = pre_appointments.requester_id INNER JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id INNER JOIN pre_confirmed_appointments ON pre_appointments.id = pre_confirmed_appointments.pre_appointment_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = person_infos.id ")
      # .select("DISTINCT(confirmed_appointments.id) as confirmed_appointments_id,pre_appointments.id,pre_appointments.requester_id,pre_appointments.proposed_date,pre_appointments.appointment_type_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,pre_appointments.medication,pre_appointments.duration,pre_confirmed_appointments.accepted_status, pre_confirmed_appointments.decline_status,confirmed_appointments.attended_to,confirmed_appointments.confirmed_date as created_at,person_contact_infos.contact_number as contact")
      # .where('confirmed_appointments.doctor_id =? AND (pre_appointments.appointment_type_id =?) AND pre_confirmed_appointments.decline_status=false AND pre_appointments.active_status=true and pre_confirmed_appointments.closed = true AND pre_appointments.del_status = false AND pre_confirmed_appointments.accepted_status=true', doc_id, 'HC' )
#       
      
      # SELECT DISTINCT(confirmed_appointments.id) as confirmed_appointments_id,pre_appointments.id,pre_appointments.requester_id,pre_appointments.proposed_date,pre_appointments.appointment_type_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,pre_appointments.medication,pre_appointments.duration,pre_confirmed_appointments.accepted_status, pre_confirmed_appointments.decline_status,confirmed_appointments.attended_to,confirmed_appointments.confirmed_date as created_at,person_contact_infos.contact_number as contact 
      # INNER JOIN "pre_appointments" ON "pre_appointments"."id" = "confirmed_appointments"."pre_appointment_id" 
    # INNER JOIN person_infos ON person_infos.id = pre_appointments.requester_id INNER JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id INNER JOIN pre_confirmed_appointments ON pre_appointments.id = pre_confirmed_appointments.pre_appointment_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = person_infos.id 
      # WHERE (confirmed_appointments.doctor_id ='248' AND (pre_appointments.appointment_type_id ='HC') AND pre_appointments.active_status=true AND pre_appointments.del_status = false AND pre_confirmed_appointments.accepted_status=true);
     
     
     if person_type 
      case person_type
      when "D"

        ConfirmedAppointment.joins("INNER JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id INNER JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id INNER JOIN person_infos ON person_infos.id = confirmed_appointments.patient_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = person_infos.id LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat")
          .select("confirmed_appointments.id, confirmed_appointments.pre_appointment_id, confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,confirmed_appointments.patient_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,pre_appointments.medication,pre_appointments.duration,confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,true as accepted_status,confirmed_appointments.attended_to,pre_appointments.beneficiary_name,pre_appointments.beneficiary_age,pre_appointments.beneficiary_phone_number,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,request_categories.category,pre_appointments.allergies,pre_appointments.prev_medical_history,pre_appointments.proposed_date,pre_appointments.proposed_time,confirmed_appointments.paid")
          .where('confirmed_appointments.doctor_id =? AND (pre_appointments.appointment_type_id =?) AND confirmed_appointments.attended_to = false ', doc_id, 'HC' )
          .order("confirmed_appointments.id desc")
      
      when "N"

         ConfirmedAppointment.joins("INNER JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id INNER JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id INNER JOIN person_infos ON person_infos.id = confirmed_appointments.patient_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = person_infos.id LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat")
          .select("confirmed_appointments.id, confirmed_appointments.pre_appointment_id, confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,confirmed_appointments.patient_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,pre_appointments.medication,pre_appointments.duration,confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,true as accepted_status,confirmed_appointments.attended_to,pre_appointments.beneficiary_name,pre_appointments.beneficiary_age,pre_appointments.beneficiary_phone_number,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,request_categories.category,pre_appointments.allergies,pre_appointments.prev_medical_history,pre_appointments.proposed_date,pre_appointments.proposed_time,confirmed_appointments.paid")
          .where('confirmed_appointments.nurse_id =? AND (pre_appointments.appointment_type_id =?) AND confirmed_appointments.attended_to = false', doc_id, 'HC' )
          .order("confirmed_appointments.id desc")
      
      else
        return
      end
    end
    
    
    
      
     
# SELECT confirmed_appointments.id, confirmed_appointments.pre_appointment_id, confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,confirmed_appointments.patient_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names 
# FROM "confirmed_appointments" INNER JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id INNER JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id INNER JOIN person_infos ON person_infos.id = confirmed_appointments.patient_id  
# WHERE (confirmed_appointments.doctor_id =248 AND (pre_appointments.appointment_type_id ='HC' ) );     
  end
  
  def self.general_appointments_homecare_details(record_id)
   
      
      ConfirmedAppointment.joins("INNER JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id INNER JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id INNER JOIN person_infos ON person_infos.id = confirmed_appointments.patient_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = confirmed_appointments.patient_id ")
      .select("confirmed_appointments.id, confirmed_appointments.pre_appointment_id, confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,confirmed_appointments.patient_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,true as accepted_status")
      .where('confirmed_appointments.id =? ', record_id)
      
  
  end
  
  def self.doc_pds_appointments_prescription(doc_id)
      
      
      ConfirmedAppointment.joins("INNER JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id INNER JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id INNER JOIN person_infos ON person_infos.id = confirmed_appointments.patient_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = person_infos.id LEFT JOIN request_urgencies ON request_urgencies.ref_id =  pre_appointments.req_urgency LEFT JOIN request_categories ON request_categories.ref_id = pre_appointments.requester_cat")
      .select("confirmed_appointments.id, confirmed_appointments.pre_appointment_id, confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,confirmed_appointments.patient_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,pre_appointments.medication,pre_appointments.duration,confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,true as accepted_status,confirmed_appointments.attended_to,pre_appointments.beneficiary_name,pre_appointments.beneficiary_age,pre_appointments.beneficiary_phone_number,request_urgencies.urgency,pre_appointments.confirm_status,pre_appointments.src,request_categories.category,pre_appointments.allergies,pre_appointments.prev_medical_history,pre_appointments.proposed_date,pre_appointments.proposed_time,pre_appointments.appt_amnt,pre_appointments.requester_id ")
      .where('confirmed_appointments.doctor_id =? AND (pre_appointments.appointment_type_id =?)', doc_id, 'PDDP' )
      .order("confirmed_appointments.id desc")
      
     
  end
  
  def self.doc_pds_appointments_prescription_details(record_id)

  
     
     ConfirmedAppointment.joins("INNER JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id INNER JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id INNER JOIN person_infos ON person_infos.id = confirmed_appointments.patient_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = confirmed_appointments.patient_id ")
      .select("confirmed_appointments.id, confirmed_appointments.pre_appointment_id, confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,confirmed_appointments.patient_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,true as accepted_status,pre_appointments.appt_amnt,pre_appointments.requester_id ")
      .where('confirmed_appointments.id =? ', record_id)
      .order("confirmed_appointments.id desc")
      
  end
  
  
  def self.doc_pds_appointments_phoneconsult(doc_id)

      
      ConfirmedAppointment.joins("INNER JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id INNER JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id INNER JOIN person_infos ON person_infos.id = confirmed_appointments.patient_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = confirmed_appointments.patient_id ")
      .select("confirmed_appointments.id, confirmed_appointments.pre_appointment_id, confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,confirmed_appointments.patient_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,true as accepted_status,confirmed_appointments.attended_to,pre_appointments.appt_amnt,pre_appointments.requester_id")
      .where('confirmed_appointments.doctor_id =? AND (pre_appointments.appointment_type_id =?)', doc_id, 'PC' ).order("confirmed_appointments.id desc")
     
  end
  
  def self.doc_pds_appointments_phoneconsult_details(record_id)

  
     
     ConfirmedAppointment.joins("INNER JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id INNER JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id INNER JOIN person_infos ON person_infos.id = confirmed_appointments.patient_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = confirmed_appointments.patient_id ")
      .select("confirmed_appointments.id, confirmed_appointments.pre_appointment_id, confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,confirmed_appointments.patient_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,true as accepted_status,pre_appointments.appt_amnt,pre_appointments.requester_id")
      .where('confirmed_appointments.id =? ', record_id)
      .order("confirmed_appointments.id desc")
      
  end
  
  def self.doc_pds_appointments_videoconsult(doc_id)
      
      ConfirmedAppointment.joins("INNER JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id INNER JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id INNER JOIN person_infos ON person_infos.id = confirmed_appointments.patient_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = confirmed_appointments.patient_id")
      .select("confirmed_appointments.id, confirmed_appointments.pre_appointment_id, confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,confirmed_appointments.patient_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,true as accepted_status,confirmed_appointments.attended_to,pre_appointments.appt_amnt,pre_appointments.requester_id")
      .where('confirmed_appointments.doctor_id =? AND (pre_appointments.appointment_type_id =?)', doc_id, 'PDVC' ).order("confirmed_appointments.id desc")
     
  end
  
  def self.doc_pds_appointments_videoconsult_details(record_id)

    
      
      ConfirmedAppointment.joins("INNER JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id INNER JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id INNER JOIN person_infos ON person_infos.id = confirmed_appointments.patient_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = confirmed_appointments.patient_id ")
      .select("confirmed_appointments.id, confirmed_appointments.pre_appointment_id, confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,confirmed_appointments.patient_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,true as accepted_status,pre_appointments.appt_amnt,pre_appointments.requester_id")
      .where('confirmed_appointments.id =? ', record_id)
      .order("confirmed_appointments.id desc")
     
  end
  
  def self.doc_pds_appointments_homecare(doc_id)

      ConfirmedAppointment.joins("INNER JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id INNER JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id INNER JOIN person_infos ON person_infos.id = confirmed_appointments.patient_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = confirmed_appointments.patient_id")
      .select("confirmed_appointments.id, confirmed_appointments.pre_appointment_id, confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,confirmed_appointments.patient_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,true as accepted_status,confirmed_appointments.attended_to,pre_appointments.appt_amnt,pre_appointments.requester_id")
      .where('confirmed_appointments.doctor_id =? AND (pre_appointments.appointment_type_id =?)', doc_id, 'PDHC' ).order("confirmed_appointments.id desc")
     
  end
  
  def self.doc_pds_appointments_homecare_details(record_id)
     
    ConfirmedAppointment.joins("INNER JOIN pre_appointments ON pre_appointments.id = confirmed_appointments.pre_appointment_id INNER JOIN appointment_types ON appointment_types.ref_id = pre_appointments.appointment_type_id INNER JOIN person_infos ON person_infos.id = confirmed_appointments.patient_id LEFT JOIN person_contact_infos ON person_contact_infos.person_id = confirmed_appointments.patient_id ")
      .select("confirmed_appointments.id, confirmed_appointments.pre_appointment_id, confirmed_appointments.complaint_desc,confirmed_appointments.confirmed_date,confirmed_appointments.patient_id,appointment_types.title as appointment_type,person_infos.surname,person_infos.other_names,person_contact_infos.contact_number as contact,true as accepted_status,pre_appointments.appt_amnt,pre_appointments.requester_id")
      .where('confirmed_appointments.id =? ', record_id)
      .order("confirmed_appointments.id desc")
     
  end
  
  
  
  
end

class LabImage < ActiveRecord::Base
  self.table_name ="lab_images"
end

class LabDetail < ActiveRecord::Base
  self.table_name ="lab_details"
end

class DeclineComment < ActiveRecord::Base
  self.table_name ="decline_comments"
end

class EarningRate < ActiveRecord::Base
   self.table_name ="earning_rates"
end

class BillingInfoBillItems < ActiveRecord::Base
  self.table_name ="billing_info_bill_items"
  
  def self.bill_details(bill_info_id,confirmed_appointment_id,appointment_type_id,req_urgency_ref)
    
    if appointment_type_id == "LA"
      
       BillingInfoBillItems.joins("LEFT JOIN patient_billing_infos ON patient_billing_infos.id = billing_info_bill_items.billing_info_id LEFT JOIN lab_details ON lab_details.confirmed_appt_id = patient_billing_infos.appointment_id  LEFT JOIN confirmed_appointments ON confirmed_appointments.id = patient_billing_infos.appointment_id LEFT JOIN drug_prices ON drug_prices.drug_id = billing_info_bill_items.item_id LEFT JOIN lab_services ON lab_services.id = billing_info_bill_items.item_id")
        .select("DISTINCT ON (billing_info_bill_items.id) billing_info_bill_items.id,lab_details.id as lab_details_id, patient_billing_infos.id AS patient_billing_infos_id,patient_billing_infos.appointment_id,confirmed_appointments.paid,lab_services.title AS drug_name, billing_info_bill_items.item_id,billing_info_bill_items.item_price,billing_info_bill_items.quantity,billing_info_bill_items.comment,billing_info_bill_items.active_status,billing_info_bill_items.created_at")
        .where('billing_info_bill_items.billing_info_id =? ', bill_info_id)
        .order("billing_info_bill_items.id desc, patient_billing_infos.id desc")
        
    else
        
      BillingInfoBillItems.joins("LEFT JOIN patient_billing_infos ON patient_billing_infos.id = billing_info_bill_items.billing_info_id LEFT JOIN confirmed_appointments ON confirmed_appointments.id = patient_billing_infos.appointment_id LEFT JOIN drug_prices ON drug_prices.drug_id = billing_info_bill_items.item_id LEFT JOIN drug_masters ON drug_masters.id = billing_info_bill_items.item_id")
        .select("DISTINCT ON (billing_info_bill_items.id) billing_info_bill_items.id, patient_billing_infos.id AS patient_billing_infos_id,patient_billing_infos.appointment_id,confirmed_appointments.paid,drug_masters.drug_name, billing_info_bill_items.item_id,drug_prices.price AS unit_price,billing_info_bill_items.item_price,billing_info_bill_items.quantity,billing_info_bill_items.comment,billing_info_bill_items.active_status,billing_info_bill_items.created_at")
        .where('billing_info_bill_items.billing_info_id =? ', bill_info_id)
        .order("billing_info_bill_items.id desc, patient_billing_infos.id desc")
      
    end
    # bill_details(bill_info_id,confirmed_appointment_id,appointment_type_id,req_urgency_ref)
    
  end
   
end

class PatientBillingInfo < ActiveRecord::Base
  self.table_name ="patient_billing_infos"
  

   
end

class CallBackStatus < ActiveRecord::Base
  self.table_name ="callback_statuses"
end

# class PaymentReq < ActiveRecord::Base
  # self.table_name ="payment_reqs"
#   
  # def self.get_appointment_id(exttrid)
#     
    # PaymentReq.joins("LEFT JOIN patient_billing_infos ON patient_billing_infos.id = payment_reqs.billing_info_id LEFT JOIN confirmed_appointments ON confirmed_appointments.id = patient_billing_infos.appointment_id")
      # .select("confirmed_appointments.id,patient_billing_infos.id AS patient_billing_infos_id,patient_billing_infos.appointment_id,confirmed_appointments.paid")
      # .where('payment_reqs.exttrid =? ', exttrid)
      # .order("confirmed_appointments.id desc")
#       
     # end
#   
# end

class PaymentTransaction < ActiveRecord::Base
  self.table_name ="payment_transactions"
  
  def self.get_appointment_id(reference)
    
    PaymentTransaction.joins("LEFT JOIN patient_billing_infos ON patient_billing_infos.id = payment_transactions.billing_info_id LEFT JOIN confirmed_appointments ON confirmed_appointments.id = patient_billing_infos.appointment_id")
      .select("confirmed_appointments.id,patient_billing_infos.id AS patient_billing_infos_id,patient_billing_infos.appointment_id,confirmed_appointments.paid,payment_transactions.billing_info_id")
      .where('payment_transactions.reference =? ', reference)
      .order("confirmed_appointments.id desc")
      
     end
     
     
   def self.retrieve_payment_history(mobile_number)  
  
  puts "mobile number is #{mobile_number}"
  PaymentTransaction.joins("LEFT JOIN payment_callback_status ON payment_callback_status.reference = payment_transactions.reference")
      .select("payment_transactions.id,payment_transactions.appointment_title,payment_transactions.amount,payment_transactions.mobile_wallet_network,payment_transactions.mobile_wallet_number,
        payment_transactions.created_at,payment_transactions.reference,payment_callback_status.status as pay_status")
      .where("payment_transactions.mobile_number LIKE ?", "%#{mobile_number}%")
      .order("payment_transactions.id desc")
      


  end 
  
end


class PaymentCallbackStatus < ActiveRecord::Base
  self.table_name ="payment_callback_status"
  

  
end



# 

# class AppVersion <ActiveRecord::Base
  # self.table_name = "app_versions"
#   
  # def self.get_app_versions()
    # select("app_versions.id,app_versions.app_id,app_versions.app_version as version, app_versions.app_code as code, app_versions.platform").where("app_versions.act_status = 't'").order('app_versions.id desc')
  # end
#   
  # def self.get_app_version(platform)
#     
    # if AppVersion.all.order("id desc").where('app_versions.platform =? ', platform).exists?
      # _version = AppVersion.all.where('app_versions.platform =? ', platform).order("id desc").limit(1)[0]
      # the_resp = { resp_code: '000', version: _version.app_version, id: _version.app_id, code: _version.app_code, platform: _version.platform }.freeze
    # else
      # the_resp = { resp_code: '100', resp_desc:"No data found" }.freeze
    # end
#     
    # return the_resp.to_json
  # end
# 
# end




